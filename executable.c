/* This file is part of Codecraft, and IDE running under AmigaOS 3.2.1 and later
 *
 * Copyright 2022 Camilla Boemann
 *
 * Codecraft is free software: you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2 as published by the Free Software Foundation.
 *
 * Codecraft is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not,
 * see <https://www.gnu.org/licenses/>.
 */

/* include */
#include <exec/memory.h>
#include <exec/execbase.h>
#include <dos/dos.h>
#include <dos/dosextens.h>
#include <dos/dostags.h>
#include <string.h>

/* prototypes */
#include <proto/dos.h>
#include <proto/exec.h>

#include "external/wb2cli.h"

#include "Codecraft_rev.h"

/*****************************************************************************/

char *version = VERSTAG;

char *stacksize = "$STACK:8192";

/*****************************************************************************/


void startTextEdit(void);

void main(int argc, char **argv)
{
	struct WBStartup *wbstartup = NULL;
    
	SysBase = (struct ExecBase *)*((struct Library **)(4L));

	if (argc == 0)
		wbstartup = (struct WBStartup *)argv;
	if (DOSBase = (struct DosLibrary *)OpenLibrary("dos.library",39))
	{
		if (WB2CLI(wbstartup, 4000, DOSBase))
		{
			SetProgramName("Codecraft");
			
			Execute("dir >NIL:", NULL,NULL); // workaround for winuae bug
			                  // where softlinks to dirs doesn't work
			                  
			startTextEdit();
		}

		CloseLibrary(DOSBase);
	}
}

void startTextEdit()
{
	BPTR seglist = NewLoadSegTags("sys:tools/textedit", TAG_DONE);

	if (seglist)
	{
		CreateNewProcTags(
				NP_Seglist, seglist,
				NP_StackSize, 100000,
//				NP_Input, Input(),
//				NP_Output, Output(),
				NP_Name, "Codecraft",
				NP_Arguments, "\n",
				NP_Cli, TRUE, TAG_DONE);
	}
}
