/* This file is part of Codecraft, and IDE running under AmigaOS 3.2.1 and later
 *
 * Copyright 2022 Camilla Boemann
 *
 * Codecraft is free software: you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2 as published by the Free Software Foundation.
 *
 * Codecrafr is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not,
 * see <https://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <stdlib.h>
#include <stdarg.h>

#include <intuition/classusr.h>
#include <gadgets/radiobutton.h>
#include <gadgets/checkbox.h>
#include <gadgets/listbrowser.h>
#include <gadgets/layout.h>
#include <gadgets/clicktab.h>
#include <gadgets/texteditor.h>
#include <images/label.h>
#include <images/penmap.h>
#include <libraries/iffparse.h>
#include <libraries/gadtools.h>
#include <dos/dos.h>
#include <dos/exall.h>
#include <dos/dostags.h>

#define __NOLIBBASE__
#include <proto/exec.h>

#define __CLIB_PRAGMA_LIBCALL
#include <clib/alib_protos.h>

#include <proto/intuition.h>
#include <proto/graphics.h>
#include <proto/utility.h>
#include <proto/asl.h>
#include <proto/texteditor.h>
#include <proto/locale.h>
#include <proto/radiobutton.h>
#include <proto/checkbox.h>
#include <proto/listbrowser.h>
#include <proto/label.h>
#include <proto/penmap.h>
#include <proto/layout.h>
#include <proto/iffparse.h>
#include <proto/gadtools.h>
#include <proto/dos.h>

#include "breakpoints.h"
#include "codecraft.h"
#include "debugger.h"
#include "wbreplytask.h"

#define CATCOMP_NUMBERS;
#include "strings.h"
const UBYTE *GetStr(LONG stringNum);

#include <tools/textedit/extension.h>

extern struct Library *SysBase;
extern struct Library *DOSBase;
extern struct Library *TextFieldBase;
extern struct Library *LocaleBase;
extern struct Library *UtilityBase;
extern struct Library *IntuitionBase;
extern struct Library *GfxBase;
extern struct Locale *Locale;
extern struct Library *LayoutBase;
extern struct Library *RadioButtonBase;
extern struct Library *CheckBoxBase;
extern struct Library *ChooserBase;
extern struct Library *LabelBase;
extern struct Library *PenMapBase;
extern struct Library *ListBrowserBase;
extern struct Library *IFFParseBase;
extern struct Catalog *Catalog;
extern struct Library *AslBase;
extern struct Library *GadToolsBase;

#define MAXPATHLEN 256

static const int bufsize = 1024;

// returns pointer to last node inserted or NULL if nothing was inserted
static struct Node *fill(struct Codecraft *ads, UWORD generation, BPTR dirLock, struct Node *latestDirNode)
{
    BOOL more;
    struct Node *doFirst = NULL;
    struct Node *lastFileNode = NULL;
    BPTR oldLock;
    struct ExAllData *buffer = (struct ExAllData *)AllocMem(bufsize, MEMF_CLEAR);
    struct ExAllControl *eac;
	UBYTE namebuf[256];
	struct LinkedNotifyRequest *notify;

    if (!buffer)
        return NULL;

    eac = (struct ExAllControl *)AllocDosObject(DOS_EXALLCONTROL, NULL);

    if (!eac)
    {
        FreeMem(buffer, bufsize);
        return NULL;
    }

    eac->eac_LastKey = 0;

    oldLock = CurrentDir(dirLock);

    do {
        struct ExAllData *ead;
        int err;
        more = ExAll(dirLock, buffer, bufsize, ED_DATE, eac);
        err = IoErr();
        if ((!more) && (err != ERROR_NO_MORE_ENTRIES)) {
            break;
        }
        if (eac->eac_Entries == 0) {
            /* ExAll failed normally with no entries */
            continue;        /* ("more" is *usually* zero) */
        }
        ead = buffer;
        do {
            BOOL isDrawer = (ead->ed_Type >= 0 && ead->ed_Type != ST_SOFTLINK);
            if (isDrawer)
            {
	            if (ead->ed_Name[0] != '.')
				{
	                BPTR subLock = Lock(ead->ed_Name, SHARED_LOCK);
	                struct Node *newPre = fill(ads, generation+1, subLock, latestDirNode);
	
	                if (newPre)
	                {
	                    if (!doFirst)
	                        doFirst = latestDirNode;
	                    latestDirNode = newPre;
	                }
	
	                UnLock(subLock);
				}
			}
            else if (MatchPatternNoCase(ads->matchingPat, ead->ed_Name))
            {
                struct Node *fileNode;

                if (!doFirst)
                    doFirst = latestDirNode;

                fileNode = AllocListBrowserNode(
                        2,
                        LBNA_Flags, generation > 0 ? LBFLG_HIDDEN : 0,
                        LBNA_Generation, generation+1,
                        LBNA_Column, 0,
                        LBNCA_CopyText, TRUE,
                        LBNCA_Text, ead->ed_Name,
                        LBNCA_MaxChars, 107,
                        LBNCA_Editable, TRUE,
                        LBNCA_Justification, LCJ_LEFT,
                        TAG_DONE
                );

                if(fileNode)
                {
                    Insert(&ads->projectTreeList, fileNode, lastFileNode ? lastFileNode : latestDirNode);
                    lastFileNode = fileNode;
                }
            }

            ead = ead->ed_Next;
        } while (ead);

    } while (more);

	NameFromLock(dirLock, namebuf, sizeof(namebuf));
	notify = (struct LinkedNotifyRequest *)AllocMem(sizeof(struct LinkedNotifyRequest), MEMF_CLEAR|MEMF_PUBLIC);
	if (notify)
	{
		notify->nr.nr_Name = namebuf;
		notify->nr.nr_UserData = (ULONG)ads;
		notify->nr.nr_Flags = NRF_SEND_SIGNAL;
		notify->nr.nr_stuff.nr_Signal.nr_Task = FindTask(NULL);
		notify->nr.nr_stuff.nr_Signal.nr_SignalNum = ads->notifySignal;
		
		AddTail((struct List *)&ads->notifyRequestList, (struct Node *)notify);
		StartNotify(&notify->nr);
	}
	
    if (doFirst)
    {
		struct Node *drawernode;
		drawernode = AllocListBrowserNode(
                2,
                LBNA_Flags, 
		                (generation ? LBFLG_HASCHILDREN:LBFLG_CUSTOMPENS)
		                |(generation > 1 ? LBFLG_HIDDEN : 0),
                LBNA_Generation, generation ? generation : 0,
                LBNA_Column, 0,
                LBNCA_CopyText, TRUE,
                LBNCA_Text, FilePart(namebuf),
                LBNCA_MaxChars, 107,
                LBNCA_FGPen,1,
                LBNCA_BGPen,2,
                LBNCA_Editable, TRUE,
                LBNCA_Justification, LCJ_LEFT,
                TAG_DONE
        );
        if(drawernode)
            Insert(&ads->projectTreeList, drawernode, doFirst);
    }

    CurrentDir(oldLock);
    FreeDosObject(DOS_EXALLCONTROL, eac);
    FreeMem(buffer, bufsize);

    return doFirst ? (lastFileNode ? lastFileNode : latestDirNode) : NULL;
}

void expandNode(struct Node *node, STRPTR path, LONG visible)
{
	LONG firstGeneration = -1;
	STRPTR sptr = path;
	LONG lenToEnd = 0;
	
	while (*sptr && *sptr != '\n' && *sptr++ != '/')
		lenToEnd++;
	if (*sptr == '\n')
		*sptr = 0;
		
	if (lenToEnd == 0)
		return;
		
	while (node->ln_Succ)
	{
		STRPTR nodeName;
		ULONG flags;
		ULONG generation;
		
		GetListBrowserNodeAttrs(node,
			LBNA_Generation, &generation,
			LBNA_Flags, &flags,
			LBNA_Column, 0,
			LBNCA_Text, (ULONG *)&nodeName,
			NULL
		);

		if (firstGeneration == -1)
			firstGeneration = generation;
			
		if (generation == firstGeneration
			&& flags & LBFLG_HASCHILDREN
		    && Strnicmp(nodeName, path, lenToEnd) == 0)
		{
			if (path[lenToEnd] == 0)
			{
				if (visible)
					ShowListBrowserNodeChildren(node, 1);
				else
					SetListBrowserNodeAttrs(node,
						LBNA_Flags, flags | LBFLG_SHOWCHILDREN,
						NULL
					);
			}
			else
			{
				if (! (flags & LBFLG_SHOWCHILDREN))
					visible = FALSE;
				expandNode(node->ln_Succ, path + lenToEnd + 1, visible);
			}
			
			return;
		}
		node = node->ln_Succ;
	}
}

BOOL saveProjectTreeIFF(BPTR file, struct Codecraft *ads)
{
	struct IFFHandle *iff;
	BOOL          result;

	result = FALSE;
	if (iff = AllocIFF())
	{
		iff->iff_Stream = file;

		InitIFFasDOS(iff);

		if (!OpenIFF(iff, IFFF_WRITE))
		{
			if (!PushChunk(iff, MAKE_ID('C', 'C', 'P', 'T'), ID_FORM, IFFSIZE_UNKNOWN))
			{
				if (PushChunk(iff, 0, MAKE_ID('H', 'E', 'A', 'D'), PROJECTHEADSIZE) == 0)
				{
					struct Configuration *cfg;

					WriteChunkBytes(iff, &ads->project, PROJECTHEADSIZE);
					PopChunk(iff);
					result = TRUE;

					cfg = (struct Configuration *)ads->project.cfgList.lh_Head;
					while (cfg->node.ln_Succ)
					{
						ULONG size = sizeof(struct Configuration) - sizeof(struct Node);
						if (PushChunk(iff, 0, MAKE_ID('C', 'O', 'N', 'F'), size) == 0)
						{
							WriteChunkBytes(iff, cfg->name, size);
							PopChunk(iff);
						}
						else
							result = FALSE;

						cfg = (struct Configuration *)cfg->node.ln_Succ;
					}
				}
			}
			CloseIFF(iff);
		}

		FreeIFF(iff);
	}

	return result;
}

void saveProjectTree(struct Codecraft *ads)
{
	BPTR oldLock;
	BPTR file;
	UBYTE filename[128];

	saveSession();

	oldLock = CurrentDir(ads->projectDirLock);

	SNPrintf(filename, sizeof(filename), "%s.projecttree", ads->project.name);
	if (file = Open(filename, MODE_NEWFILE))
	{
		saveProjectTreeIFF(file, ads);
		Close(file);
		SetProtection(filename, FIBF_EXECUTE);
		updateRecentMenu(ads);
	}

	CurrentDir(oldLock);
}

void loadProjectTreeIFF(BPTR file, struct Codecraft *ads)
{
	struct IFFHandle   *iff;
	struct ContextNode *cn;
	ULONG error;
	struct Configuration *cfg;
	LONG i;

	if (iff = AllocIFF())
	{
		iff->iff_Stream = file;

		InitIFFasDOS(iff);
		
		if (!OpenIFF(iff, IFFF_READ))
		{
			while (TRUE)
			{
				error = ParseIFF(iff, IFFPARSE_STEP);
				if (error == IFFERR_EOC)
					continue;
				if (error)
					break;

				cn = CurrentChunk(iff);

				if (cn->cn_Type == MAKE_ID('C', 'C', 'P', 'T'))
				{
					if (cn->cn_ID == MAKE_ID('H', 'E', 'A', 'D'))
					{
						ReadChunkBytes(iff, &ads->project, 132);
						if (ads->project.flags & CCPROJ_FLAG_NEWFMT)
						{
							ReadChunkBytes(iff, &ads->project.currentCfgIndex,
									 PROJECTHEADSIZE - 132);
							ads->project.cfg = 0;
							NewList(&ads->project.cfgList);
						}
						else
						{
							struct oldformat
							{
								// flags and name already loaded
								UBYTE testUpToDateCmd[128];
								UBYTE exeCmd[128];
								UBYTE arguments[256];
								UBYTE workDir[MAXPATHLEN];
								UBYTE includePattern[128];
								UBYTE buildDir[MAXPATHLEN];
								UBYTE buildCmd[128];
								UBYTE cleanCmd[128];
								} old;

							ads->project.flags &= ~1; // clear old wb flag
							ads->project.flags |= CCPROJ_FLAG_NEWFMT;
							
							ReadChunkBytes(iff, &old, sizeof(struct oldformat));
							
							NewList(&ads->project.cfgList);
							ads->project.currentCfgIndex = 0;

							cfg = (struct Configuration *)AllocVec(sizeof(struct Configuration), MEMF_CLEAR);
							
							if (cfg)
							{
								Strncpy(cfg->name, "Default", 128);
								cfg->flags = (ads->project.flags & 0x1) ? CFG_FLAG_WB : 0;
								Strncpy(cfg->includePattern, old.includePattern, 128);
								Strncpy(cfg->testUpToDateCmd, old.testUpToDateCmd, 128);
								Strncpy(cfg->exeCmd, old.exeCmd, 128);
								Strncpy(cfg->arguments, old.arguments, 256);
								Strncpy(cfg->workDir, old.workDir, MAXPATHLEN);
								Strncpy(cfg->buildDir, old.buildDir, MAXPATHLEN);
								Strncpy(cfg->buildCmd, old.buildCmd, 128);
								Strncpy(cfg->cleanCmd, old.cleanCmd, 128);
								
								AddTail(&ads->project.cfgList, (struct Node *)cfg);
							}
						}
					}
					else if (cn->cn_ID == MAKE_ID('C', 'O', 'N', 'F'))
					{
						cfg = (struct Configuration *)AllocVec(sizeof(struct Configuration), MEMF_CLEAR);
						
						if (cfg)
						{
							ReadChunkBytes(iff, cfg->name, sizeof(struct Configuration) - sizeof(struct Node));
							AddTail(&ads->project.cfgList, (struct Node *)cfg);
						}
					}
				}
			}
			CloseIFF(iff);
		}
		FreeIFF(iff);
	}

	cfg = (struct Configuration *)ads->project.cfgList.lh_Head;
	i = 0;
	while (cfg->node.ln_Succ)
	{
		ads->project.cfg = cfg;
		if (i == ads->project.currentCfgIndex)
			break;
		i++;
		cfg = (struct Configuration *)cfg->node.ln_Succ;
	}
}

void closeProjectTree(struct Codecraft *ads)
{
	struct CodecraftDoc *adsDoc = (struct CodecraftDoc *)ads->documentList.mlh_Head;
	struct Node *node;
	
	if (ads->projectDirLock)
	{
		saveProjectTree(ads);
		UnLock(ads->projectDirLock);
		ads->projectDirLock = NULL;
	}

	fillGui(ads); // this will remove
	
	clearSearchInFilesPath();

	while (adsDoc->node.mln_Succ)
	{
		struct CodecraftDoc *nextDoc = (struct CodecraftDoc *)adsDoc->node.mln_Succ;
		ads->tei->executeRexxCommand(adsDoc->document, "CLOSE");
		adsDoc = nextDoc;
	}

	while(adsDoc = (struct CodecraftDoc *)RemTail((struct List *)&ads->breakpointDocList))
		disposeCodecraftDoc(adsDoc);

	/* clear any project configuration */
	while (node = RemHead(&ads->project.cfgList))
		FreeVec(node);
		
	ads->tei->setWindowFormatStrings("Codecraft", "Codecraft");

	ItemAddress(ads->menu, FULLMENUNUM(0, 6, 0xFF))->Flags &= ~ITEMENABLED;
	ItemAddress(ads->menu, FULLMENUNUM(1, 0, 0))->Flags &= ~ITEMENABLED;
	ItemAddress(ads->menu, FULLMENUNUM(1, 1, 0))->Flags &= ~ITEMENABLED;
	ItemAddress(ads->menu, FULLMENUNUM(1, 2, 0))->Flags &= ~ITEMENABLED;
	ItemAddress(ads->menu, FULLMENUNUM(2, 0, 0))->Flags &= ~ITEMENABLED;
	ItemAddress(ads->menu, FULLMENUNUM(2, 1, 0))->Flags &= ~ITEMENABLED;
	ItemAddress(ads->menu, FULLMENUNUM(0, 3, 0))->Flags &= ~ITEMENABLED;
	ItemAddress(ads->menu, FULLMENUNUM(0, 5, 0))->Flags &= ~ITEMENABLED;
}

void setWindowTitles(struct Codecraft *ads)
{
	SNPrintf(ads->windowtitle, sizeof(ads->windowtitle), "%s - Codecraft", ads->project.name);

	if (ads->debugger->debuggerMode != NOTACTIVE)
		Strncat(ads->windowtitle,"   [DEBUGGING]", sizeof(ads->windowtitle));
	ads->tei->setWindowFormatStrings(ads->windowtitle, ads->windowtitle);
}

void fillGui(struct Codecraft *ads)
{
	struct Node *node;
	struct LinkedNotifyRequest *notify;
	TEXT buffer[MAXPATHLEN];
	BPTR file;
	
	while (notify = (struct LinkedNotifyRequest *)RemHead((struct List *)&ads->notifyRequestList))
	{
		EndNotify(&notify->nr);
		FreeMem(notify, sizeof(struct LinkedNotifyRequest));
	}
		
	SetGadgetAttrs(	(struct Gadget *) ads->makefileListGadget, NULL, NULL,
		LISTBROWSER_Labels, (ULONG) NULL,
		TAG_DONE
		);

	file = Open("T:codecraftexpanded", MODE_NEWFILE);
	if (file)
	{
		writeExpandedNodes(ads, file, buffer);
		Close(file);
	}
		
	/* clear any old tree that was loaded */
	while (node = RemHead(&ads->projectTreeList))
		FreeListBrowserNode(node);

	if (ads->projectDirLock)
	{
		ParsePatternNoCase(ads->project.cfg->includePattern, ads->matchingPat, sizeof(ads->matchingPat));
	
		/* build new tree */
		fill(ads, 0, ads->projectDirLock, ads->projectTreeList.lh_TailPred);
		file = Open("T:codecraftexpanded", MODE_OLDFILE);
		if (file)
		{
			expandNodes(ads, file, buffer);
			Close(file);
		}

		SNPrintf(ads->windowtitle, sizeof(ads->windowtitle), "%s - Codecraft", ads->project.name);
		ads->tei->setWindowFormatStrings(ads->windowtitle, ads->windowtitle);

		ItemAddress(ads->menu, FULLMENUNUM(0, 6, 0xFF))->Flags |= ITEMENABLED;
		ItemAddress(ads->menu, FULLMENUNUM(1, 0, 0))->Flags |= ITEMENABLED;
		ItemAddress(ads->menu, FULLMENUNUM(1, 1, 0))->Flags |= ITEMENABLED;
		ItemAddress(ads->menu, FULLMENUNUM(1, 2, 0))->Flags |= ITEMENABLED;
		ItemAddress(ads->menu, FULLMENUNUM(2, 0, 0))->Flags |= ITEMENABLED;
		ItemAddress(ads->menu, FULLMENUNUM(2, 1, 0))->Flags |= ITEMENABLED;
		ItemAddress(ads->menu, FULLMENUNUM(0, 3, 0))->Flags |= ITEMENABLED;
		ItemAddress(ads->menu, FULLMENUNUM(0, 5, 0))->Flags |= ITEMENABLED;
	}
	
	SetGadgetAttrs(	(struct Gadget *) ads->makefileListGadget, ads->window, NULL,
		LISTBROWSER_Labels, (ULONG) &ads->projectTreeList,
		LISTBROWSER_Selected, 0,
		TAG_DONE
		);
}

void openProjectTree(struct Codecraft *ads, STRPTR optionalFullPath)
{
	BPTR file = NULL;
	BPTR dirLock = NULL;
	BPTR oldLock;
	struct TagItem tg[12];

    tg[0].ti_Tag = ASL_Hail;
    tg[0].ti_Data = (ULONG) GetStr(MSG_OPENTREE_TITLE);
    tg[1].ti_Tag = ASL_FuncFlags;
    tg[1].ti_Data = 0;
    tg[2].ti_Tag = ASL_OKText;
    tg[2].ti_Data = (ULONG) GetStr(MSG_OPENTREE_OPEN_GAD);
    tg[3].ti_Tag = ASL_Window;
    tg[3].ti_Data = (ULONG) ads->window;
    tg[4].ti_Tag = ASL_LeftEdge;
    tg[4].ti_Data = (ULONG) ads->window->LeftEdge + 12;
    tg[5].ti_Tag = ASL_TopEdge;
    tg[5].ti_Data = (ULONG) ads->window->TopEdge + 12;
    tg[6].ti_Tag = ASL_Pattern;
	tg[6].ti_Data = (ULONG) "#?.projecttree";
    tg[7].ti_Tag = ASLFR_DoSaveMode;
    tg[7].ti_Data = FALSE;
    tg[8].ti_Tag = ASLFR_SleepWindow;
    tg[8].ti_Data = TRUE;
	tg[9].ti_Tag = ASLFR_DoPatterns;
	tg[9].ti_Data = (ULONG)"";
	tg[10].ti_Tag = ASLFR_InitialFile;
	tg[10].ti_Data = (ULONG)"";
	tg[11].ti_Tag = TAG_DONE;

	if (ads->debugger->debuggerMode != NOTACTIVE)
	{
		askUser(ads->window, MSG_OK_GAD, MSG_DBGRUNS_CANTDO, NULL);
		return;
	}
	
    /* Request the file name */
	if (optionalFullPath)
	{
		UBYTE c;
		STRPTR pos = PathPart(optionalFullPath);
		c = *pos;
		*pos = 0;
		dirLock = Lock(optionalFullPath, SHARED_LOCK);
		*pos = c;

		if (dirLock)
		{
			oldLock = CurrentDir(dirLock);
			file = Open(FilePart(optionalFullPath), MODE_OLDFILE);
			CurrentDir(oldLock);
		}

		if (!file)
			askUser(ads->window, MSG_OK_GAD, MSG_NOPROJECTTREE, optionalFullPath);
	}
	else if (AslRequest (ads->FileRequester, tg))
    {
		dirLock = Lock(ads->FileRequester->rf_Dir, SHARED_LOCK);

		if (dirLock)
		{
			oldLock = CurrentDir(dirLock);

			file = Open(ads->FileRequester->rf_File, MODE_OLDFILE);
			CurrentDir(oldLock);
		}
    }

	if (file)
	{
		TEXT buf[MAXPATHLEN];
		STRPTR suffixPos;
		
		closeProjectTree(ads);

		ads->projectDirLock = dirLock;

		loadProjectTreeIFF(file, ads);
		updateConfigurationMenu(ads);
		
		NameFromFH(file, buf, sizeof(buf));
		suffixPos = buf + strlen(buf) - 12;
		if (Stricmp(".projecttree", suffixPos) == 0)
			*suffixPos = 0; // strip the suffix
		Strncpy(ads->project.name, FilePart(buf), sizeof(ads->project.name));

		fillGui(ads);

		loadSession();

		Close(file);
		
		ads->tei->installWelcomeGadget((Object *)1, "Get started");
	}
	else if (dirLock)
		UnLock(dirLock);
}


void startAsyncRead(struct Codecraft *ads)
{
	struct FileHandle *fh;
    struct DosPacket *packet;

	if (!ads->pipeFh)
	{
		ads->messageBuffer[0] = 0;
		ads->pipeFh = Open("PIPE:codecraftbuild", MODE_OLDFILE);
	}

	fh = (struct FileHandle *)BADDR(ads->pipeFh);

    if (fh && fh->fh_Type)
    {
        if (packet = (struct DosPacket *)AllocDosObject (DOS_STDPKT,TAG_END))
        {
            packet->dp_Port = ads->asyncFilePort;

            packet->dp_Type = ACTION_READ;
            packet->dp_Arg1 = fh->fh_Arg1;
			packet->dp_Arg2 = (LONG)ads->pipeBuffer;
			packet->dp_Arg3 = (LONG)sizeof(ads->pipeBuffer)-1;
            PutMsg (fh->fh_Type,packet->dp_Link);
        }
    }
}

void insertBuildMessage(struct Codecraft *ads, STRPTR msg)
{
	struct Node *node = AllocListBrowserNode(
		1,
		LBNA_Column, 0,
		LBNCA_CopyText, TRUE,
		LBNCA_WordWrap, TRUE,
		LBNCA_Text, msg,
		LBNCA_Editable, FALSE,
		LBNCA_Justification, LCJ_LEFT,
		TAG_DONE
	);
	
	if (node)
		AddTail(&ads->msgNodeList, node);
}

void parseBuildMessages(struct Codecraft *ads)
{
    struct Message *asyncMsg;
	struct DosPacket *packet;
	LONG rc;
	UBYTE *buf;
	int i;
	int start;
	BOOL newline;

	SetGadgetAttrs(	(struct Gadget *) ads->buildBrowser, ads->window, NULL,
		LISTBROWSER_Labels, (ULONG) NULL,
		TAG_DONE
		);

    while (asyncMsg = GetMsg(ads->asyncFilePort))
    {
        packet = (struct DosPacket *) asyncMsg->mn_Node.ln_Name;
        rc = packet->dp_Res1;
        buf = (char *)packet->dp_Arg2;
        buf [rc] = 0;
		i = 0;
		start = 0;
        FreeDosObject (DOS_STDPKT, packet);

		while (buf[i] != 0)
        {
			while (buf[i] != 0 && buf[i] != '\n')
             ++i;

			newline = (buf[i] == '\n');
			buf[i] = 0;
			Strncat(ads->messageBuffer, buf + start, sizeof(ads->messageBuffer));

			if (newline)
			{
				insertBuildMessage(ads, ads->messageBuffer);

                ads->messageBuffer[0] = 0;
                ++i;
            }

            start = i;
        }
    }
	if (rc)
		startAsyncRead(ads);
	else
	{
		Close(ads->pipeFh);
		ads->pipeFh = NULL;
		ads->buildInProgress = FALSE;
		UpdateMenuStates(ads, TRUE, TRUE, TRUE);
		
		if (ads->FollowupAfterBuild == 1)
		{
			ads->FollowupAfterBuild = 0;
			buildProjectTree(ads, ads->project.cfg->buildCmd, FALSE);

		}
		else if (ads->FollowupAfterBuild == 2)
		{
			ads->FollowupAfterBuild = 0;
			insertBuildMessage(ads, "...done");
			runProject(ads, FALSE);
		}
		else if (ads->FollowupAfterBuild == 3)
		{
			ads->FollowupAfterBuild = 0;
			insertBuildMessage(ads, "...done");
			runProject(ads, TRUE);
		}
		else
		{
			insertBuildMessage(ads, "...done");
			fillGui(ads);
		}
	}

	SetGadgetAttrs(	(struct Gadget *) ads->buildBrowser, ads->window, NULL,
		LISTBROWSER_Labels, (ULONG) &ads->msgNodeList,
		LISTBROWSER_Selected, 0,
		LISTBROWSER_MakeVisible, ads->msgNodeList.lh_TailPred,
		TAG_DONE
		);
}

struct CodecraftDoc *revealLocation(struct Codecraft *ads, STRPTR path, ULONG blockNum)
{
	UBYTE buffer[MAXPATHLEN];

	SNPrintf(buffer, MAXPATHLEN, "OPEN FILENAME \"%s\"", path);
	ads->tei->executeRexxCommand(NULL, buffer);

	ads->tei->executeRexxCommand(NULL, "MARK OFF");

	SNPrintf(buffer, MAXPATHLEN, "GOTOLINE %ld", blockNum);
	ads->tei->executeRexxCommand(NULL, buffer);
	return ads->currentAdsDocument;
}

void findEFilename(struct Codecraft *ads, struct Node *outputNode, UBYTE *buffer)
{
	while (outputNode)
	{
		STRPTR text;

		GetListBrowserNodeAttrs(outputNode,
			LBNCA_Text, (ULONG *)&text,
			TAG_DONE);

		if (strstr(text,"E\240Compiler"))
			return;

		if (Strnicmp(text,"-> ",3) == 0)
		{
			Strncpy(buffer, text+3, MAXPATHLEN);
			return;
		}

		outputNode = outputNode->ln_Pred;
	}
}

STRPTR getBuildLineText(struct Codecraft *ads, struct Node *outputNode)
{
	STRPTR text;

	if (outputNode->ln_Succ == NULL)
		return NULL;
		
	GetListBrowserNodeAttrs(outputNode,
		LBNCA_Text, (ULONG *)&text,
		TAG_DONE);

	return text;
}

ULONG parseBuildLine(struct Codecraft *ads, STRPTR text, struct Node *outputNode)
{
	STRPTR c = text;
	STRPTR stringstart = text;
	STRPTR part[7];
	int i = 0;
	UBYTE buffer[MAXPATHLEN];
	BPTR oldLock;
	ULONG blockNum;
	
	while (*c && i < 7)
	{
		while (*c && *c!= ' ')
			c++;

		part[i] = stringstart;
		i++;
		stringstart = ++c;
	}
	
	if (i < 3)
		return FALSE;

	if (Strnicmp(part[0], "WARNING:", 8)==0)
	{
		// E style warnings eg..
		//WARNING: suspicious use of "=" in void expression(s). (line 33)
		c = strstr(text,"(line ");
		if (!c)
			return FALSE;
		blockNum = atoi(c+6)-1;
		findEFilename(ads, outputNode, buffer);
	}
	else if (Strnicmp(part[1], "ERROR: ", 7)==0 || Strnicmp(part[2], "ERROR: ", 7)==0)
	{
		// E style errors
		c = getBuildLineText(ads, outputNode->ln_Succ);
		if (c && !Strnicmp(c,"LINE ",5))
		{
			blockNum = atoi(c+5)-1;
			findEFilename(ads, outputNode, buffer);
		}
	}
	else if (Strnicmp(part[2], "Warning", 7)==0 || Strnicmp(part[2], "Error", 5)==0)
	{
		// SAS/C style errors
		if (atoi(part[1]) == 0)
			return FALSE;
			
		Strncpy(buffer,part[0], part[1] - part[0]);
		buffer[part[1] - part[0] - 1] = 0;
		blockNum = atoi(part[1])-1;
	}
	else if (Strnicmp(part[0], "warning", 7)==0 || Strnicmp(part[0], "error", 5)==0)
	{
		// VBCC style errors
		if (atoi(part[4]) == 0)
			return FALSE;
		if (*part[6] != '"')
			return FALSE;
			
		Strncpy(buffer,part[6], MAXPATHLEN);
		buffer[strlen(buffer) - 1] = 0;
		blockNum = atoi(part[4])-1;
	}
	else
		return FALSE;
			
	oldLock = CurrentDir(ads->projectDirLock);
	revealLocation(ads, buffer, blockNum);
	CurrentDir(oldLock);
	return TRUE;
}

void gotoPrevMessage(struct Codecraft *ads)
{
	struct Node *node;
	STRPTR text;
	ULONG nodeIndex;

    GetAttr(LISTBROWSER_SelectedNode, ads->buildBrowser, (ULONG *)&node);
    GetAttr(LISTBROWSER_Selected, ads->buildBrowser, (ULONG *)&nodeIndex);

	if (!node || !node->ln_Pred)
		return;
	node = node->ln_Pred;
	nodeIndex--;
		
   	while (node->ln_Pred)
	{
		GetListBrowserNodeAttrs(node,
			LBNCA_Text, (ULONG *)&text,
			TAG_DONE);

		if (parseBuildLine(ads, text, node))
		{
			SetGadgetAttrs((struct Gadget *)ads->buildBrowser, ads->window, NULL,
				LISTBROWSER_SelectedNode, node,
				LISTBROWSER_MakeVisible, nodeIndex,
				TAG_END);
			
			return;
		}
		
		node = node->ln_Pred;
		nodeIndex--;
	}
}

void gotoNextMessage(struct Codecraft *ads)
{
	struct Node *node;
	STRPTR text;
	ULONG nodeIndex;

    GetAttr(LISTBROWSER_SelectedNode, ads->buildBrowser, (ULONG *)&node);
    GetAttr(LISTBROWSER_Selected, ads->buildBrowser, (ULONG *)&nodeIndex);

	if (!node || !node->ln_Succ)
		return;
	node = node->ln_Succ;
	nodeIndex++;

   	while (node->ln_Succ)
	{
		GetListBrowserNodeAttrs(node,
			LBNCA_Text, (ULONG *)&text,
			TAG_DONE);

		if (parseBuildLine(ads, text, node))
		{
			SetGadgetAttrs((struct Gadget *)ads->buildBrowser, ads->window, NULL,
				LISTBROWSER_SelectedNode, node,
				LISTBROWSER_MakeVisible, nodeIndex,
				TAG_END);
			
			return;
		}
		
		node = node->ln_Succ;
		nodeIndex++;
	}
}    								

//returns lock to progdir or NULL
static BPTR checkExistsExe(STRPTR file)
{
	BPTR lock;
	BPTR parent = NULL;
	
	if (*file == 0)
		return NULL;
	
	lock = Lock(file, SHARED_LOCK);
	if (lock)
	{
		struct FileInfoBlock fib;

		Examine(lock, &fib);

		if (fib.fib_DirEntryType < 0 && (fib.fib_Protection & FIBF_EXECUTE) == 0)
			parent = ParentDir(lock);

		UnLock(lock);
	}
	return parent;
}

struct Process *runAsIfFromCLI(struct Codecraft *ads, BPTR homeLock, BPTR seglist, ...)
{
	va_list taglist;
	struct Process *p;
	
	va_start(taglist, seglist);
	
	p = CreateNewProcTags(
			NP_Seglist, seglist,
			NP_FreeSeglist, TRUE,
			NP_StackSize, 100000,
			NP_Output,0,
			NP_CloseOutput,FALSE,
			NP_HomeDir, homeLock,
			NP_Arguments, ads->project.cfg->arguments,
			NP_Cli, TRUE,
			TAG_MORE, taglist,
			TAG_DONE);
			
	va_end(taglist);
	
	return p;
}

struct Process *runAsIfFromWb(struct Codecraft *ads, BPTR homeLock, BPTR seglist, ...)
{
	struct Process *p = NULL;
	int i;
	int numArgs = 0;
	ULONG size = sizeof(struct WbRun) + sizeof(struct WBArg) * numArgs;
	struct WbRun *wr;

	if (wr = (struct WbRun *)AllocMem(size, MEMF_PUBLIC | MEMF_CLEAR))
	{
		wr->WBArg[0].wa_Lock = NULL;
		wr->WBArg[0].wa_Name = ads->project.cfg->exeCmd;
		
		for (i = 0; i <= numArgs; i++)
		{
			BPTR flock;
			BPTR plock;

			if (flock = Lock(wr->WBArg[i].wa_Name, SHARED_LOCK))
			{
				plock = ParentDir(flock);
				UnLock(flock);
			}
			
			if (plock == NULL)
				break;
			
			wr->WBArg[i].wa_Lock = plock;
			wr->WBArg[i].wa_Name = FilePart(wr->WBArg[i].wa_Name);
			
		}
		
		if (i == numArgs + 1)
		{
			va_list taglist;
			
			va_start(taglist, seglist);
			if (p = CreateNewProcTags(
					NP_Seglist, seglist,
					NP_FreeSeglist, TRUE,
					NP_StackSize, 100000,
					NP_Output,0,
					NP_CloseOutput,FALSE,
					NP_HomeDir, homeLock,
					TAG_MORE, taglist,
					TAG_DONE))
			{
				wr->WBStartup.sm_Message.mn_ReplyPort = FindPort(WBREPLYPORTNAME);
				wr->WBStartup.sm_Message.mn_Length  = sizeof(struct WBStartup);
				wr->WBStartup.sm_Process = &p->pr_MsgPort;
				wr->WBStartup.sm_Segment = seglist;
				wr->WBStartup.sm_NumArgs = numArgs + 1;
				wr->WBStartup.sm_ToolWindow = NULL;
				wr->WBStartup.sm_ArgList = wr->WBArg;
				
				wr->Size = size;
			
				PutMsg(&p->pr_MsgPort, &wr->WBStartup.sm_Message);
				
				va_end(taglist);
				return p;
			}
			va_end(taglist);
		}
		
		for (--i; i >= 0; --i)
			UnLock(wr->WBArg[i] .wa_Lock);
				
		FreeMem(wr, size);
	}
	return p;
}

LONG testUpToDate(struct Codecraft *ads)
{
	BPTR testWhereLock;
	BPTR buildDirLock = NULL;
	UBYTE fullCmd[160];
	ULONG retval;

	if (ads->project.cfg->testUpToDateCmd[0] == 0)
		return TRUE;
		
	testWhereLock = ads->projectDirLock;

	if (*ads->project.cfg->buildDir)
	{
		buildDirLock = Lock(ads->project.cfg->buildDir, SHARED_LOCK);
		testWhereLock = buildDirLock;
	}

	SNPrintf(fullCmd, sizeof(fullCmd), "%s", ads->project.cfg->testUpToDateCmd);
	retval = SystemTags(fullCmd,
		NP_CurrentDir, DupLock(testWhereLock),
		TAG_DONE);
 
	if (buildDirLock)
		UnLock(buildDirLock);

	return retval == 0;
}

void runProject(struct Codecraft *ads, BOOL withDebug)
{
	BPTR oldLock;
	BPTR workLock = NULL;
	BPTR homeLock;
	TEXT exepath[MAXPATHLEN];

	saveAll(ads);

	if (! testUpToDate(ads))
	{
		switch (askUser(ads->window, MSG_NOTUPTODATE_GAD, MSG_NOTUPTODATE, NULL))
		{
			case 2:
				buildProjectTree(ads, ads->project.cfg->buildCmd, TRUE);
				ads->FollowupAfterBuild = withDebug ? 3 : 2;
				return;
			case 1:
				break;
			case 0:
				return;
		}
	}
		
	oldLock = CurrentDir(ads->projectDirLock);

	//try to build full path to executable
	NameFromLock(ads->projectDirLock, exepath, MAXPATHLEN);
	if (*ads->project.cfg->buildDir)
		AddPart(exepath, ads->project.cfg->buildDir, MAXPATHLEN);
	AddPart(exepath, ads->project.cfg->exeCmd, MAXPATHLEN);
 		
	if (*ads->project.cfg->workDir)
	{
		workLock = Lock(ads->project.cfg->workDir, SHARED_LOCK);
		CurrentDir(workLock);
	}

	homeLock = checkExistsExe(exepath);
	if (homeLock)
	{
		if (withDebug)
			StartDebug(ads->debugger, homeLock, exepath);
		else
		{
			BPTR seglist;
			
		    seglist = NewLoadSeg(exepath, TAG_DONE);

			if (seglist)
			{
				if (ads->project.cfg->flags & CFG_FLAG_WB)
					runAsIfFromWb(ads, homeLock, seglist, TAG_DONE);
				else
					runAsIfFromCLI(ads, homeLock, seglist, TAG_DONE);
			}
		}
	}
	else	
		askUser(ads->window, MSG_OK_GAD, MSG_NOTHINGTORUN, exepath);

	CurrentDir(oldLock);
	UnLock(workLock);
}

void buildProjectTree(struct Codecraft *ads, STRPTR cmd, ULONG restartOutput)
{
	BPTR buildWhereLock;
	BPTR buildDirLock = NULL;
	struct Node *node;
	UBYTE fullCmd[160];

	saveAll(ads);

	SetGadgetAttrs(	(struct Gadget *) ads->buildBrowser, ads->window, NULL,
		LISTBROWSER_Labels, (ULONG) NULL,
		TAG_DONE
		);
	if (restartOutput)
	{
		while (node = RemTail(&ads->msgNodeList))
			FreeListBrowserNode(node);

		insertBuildMessage(ads, "starting ...");
	}
	
	SetGadgetAttrs(	(struct Gadget *) ads->buildBrowser, ads->window, NULL,
		LISTBROWSER_Labels, (ULONG) &ads->msgNodeList,
		LISTBROWSER_Selected, 0,
		TAG_DONE
		);

	SetGadgetAttrs(	(struct Gadget *) ads->utilityClickTab, ads->window, NULL,
		CLICKTAB_Current, 0,
		TAG_DONE
		);
	
	startAsyncRead(ads);
	ads->buildInProgress = TRUE;
	UpdateMenuStates(ads, FALSE, FALSE, FALSE);

	if (*cmd == 0)
	{
	
		Execute("echo No build command set in paramters >PIPE:codecraftbuild",
			Open("NIL:", MODE_OLDFILE), 0);
		return;
	}
	
	buildWhereLock = ads->projectDirLock;
	if (*ads->project.cfg->buildDir)
	{
		buildDirLock = Lock(ads->project.cfg->buildDir, SHARED_LOCK);
		buildWhereLock = buildDirLock;
	}

	SNPrintf(fullCmd, sizeof(fullCmd), "%s *>< >PIPE:codecraftbuild &", cmd);
	SystemTags(fullCmd,
		NP_CurrentDir, DupLock(buildWhereLock),
		TAG_DONE);

	if (buildDirLock)
		UnLock(buildDirLock);
}
