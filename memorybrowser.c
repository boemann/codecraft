/* This file is part of Codecraft, an IDE running under AmigaOS 3.2.1 and later
 *
 * Copyright 2022 Camilla Boemann
 *
 * Codecraft is free software: you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2 as published by the Free Software Foundation.
 *
 * Codecrafr is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with CodeCraft. If not,
 * see <https://www.gnu.org/licenses/>.
 */
#include <string.h>
#include <stdlib.h>
#include <intuition/classusr.h>
#include <gadgets/chooser.h>
#include <gadgets/button.h>
#include <gadgets/string.h>
#include <gadgets/listbrowser.h>

#include <proto/exec.h>

#define __CLIB_PRAGMA_LIBCALL
#include <clib/alib_protos.h>

#include <proto/intuition.h>
#include <proto/utility.h>
#include <proto/button.h>
#include <proto/string.h>
#include <proto/chooser.h>
#include <proto/listbrowser.h>
#include <proto/layout.h>

#include <graphics/gfxbase.h>
#include <proto/graphics.h>

#define CATCOMP_NUMBERS;
#include "strings.h"
#include "debugger.h"
#include "codecraft.h"

const UBYTE *GetStr(LONG stringNum);
extern struct TextAttr fixedFont;
struct List memBrowserTypes;

void clearMemoryBrowser(struct Codecraft *ads)
{
	SetGadgetAttrs(	(struct Gadget *) ads->memoryBrowser, ads->window, NULL,
		LISTBROWSER_Labels, (ULONG) NULL,
		TAG_DONE
		);

	FreeListBrowserList(&ads->memoryBrowserList);

	SetGadgetAttrs(	(struct Gadget *) ads->memoryBrowser, ads->window, NULL,
		LISTBROWSER_Labels, (ULONG) &ads->memoryBrowserList,
		TAG_DONE
		);
}

int printable(UBYTE c)
{
	return (c <31 || c >127) ? '.' : c;
}

struct Node *GetMemNode(struct Codecraft *ads, LONG num)
{
	struct Node *node = ads->memoryBrowserList.lh_Head;
	struct Node *nextnode;
	LONG cnt=0;

	while (nextnode = node->ln_Succ)
	{
		if (cnt==num) return node;
		node = nextnode;
		cnt++;
	}
	return NULL;
}

void AddMem(struct Codecraft *ads, ULONG row, UBYTE *addr, ULONG type, BOOL showUpdates)
{
	struct Node *node;
	char memText1[10];
	char memText2[20];
	char memText3[10];
	char memText4[10];
	char memText5[10];
	STRPTR oldMemText2, oldMemText3, oldMemText4, oldMemText5; 
	ULONG i;

	if (TypeOfMem(addr) && TypeOfMem((addr+15)))
	{
		if (!type)
		{
			SNPrintf(memText1, sizeof(memText1), "%08lx:",addr);
			SNPrintf(memText2, sizeof(memText2), "%02lx%02lx%02lx%02lx", *addr, *(addr+1), *(addr+2), *(addr+3));
			SNPrintf(memText3, sizeof(memText3), "%02lx%02lx%02lx%02lx", *(addr+4), *(addr+5), *(addr+6), *(addr+7));
			SNPrintf(memText4, sizeof(memText4), "%02lx%02lx%02lx%02lx", *(addr+8), *(addr+9), *(addr+10), *(addr+11));
			SNPrintf(memText5, sizeof(memText5), "%02lx%02lx%02lx%02lx", *(addr+12), *(addr+13), *(addr+14), *(addr+15));
		}
		else
		{
			SNPrintf(memText1,sizeof(memText1),"%08lx:",addr);
			Strncpy(memText2,"########",sizeof(memText2));
			Strncpy(memText3,"########",sizeof(memText3));
			for (i=0; i<8; i++)
				*(memText2+i) = printable(*(addr+i));
			for (i=0; i<8; i++)
				*(memText3+i) = printable(*(addr+i+8));
			Strncpy(memText4,"",sizeof(memText4));
			Strncpy(memText5,"",sizeof(memText5));
		}
	}
	else
	{
		SNPrintf(memText1, sizeof(memText1), "%08lx:",addr);
		Strncpy(memText2, "********",sizeof(memText2));
		Strncpy(memText3, "********",sizeof(memText3));
		Strncpy(memText4, "********",sizeof(memText4));
		Strncpy(memText5, "********",sizeof(memText5));
	}

	node = GetMemNode(ads, row);
	
	if (node == NULL)
	{	
		node = AllocListBrowserNode(
			5,
			LBNA_Column, 0,
			LBNCA_CopyText, TRUE,
			LBNCA_Text, memText1,
			LBNA_Column, 1,
			LBNCA_CopyText, TRUE,
			LBNCA_Text, memText2,
			LBNA_Column, 2,
			LBNCA_CopyText, TRUE,
			LBNCA_Text, memText3,
			LBNA_Column, 3,
			LBNCA_CopyText, TRUE,
			LBNCA_Text, memText4,
			LBNA_Column, 4,
			LBNCA_CopyText, TRUE,
			LBNCA_Text, memText5,
			TAG_DONE);

		AddTail(&ads->memoryBrowserList, node);
	}
	else
	{
		GetListBrowserNodeAttrs(node,
			LBNA_Column, 1,
			LBNCA_Text, &oldMemText2,
			LBNA_Column, 2,
			LBNCA_Text, &oldMemText3,
			LBNA_Column, 3,
			LBNCA_Text, &oldMemText4,
			LBNA_Column, 4,
			LBNCA_Text, &oldMemText5,
			TAG_DONE);
			
		SetListBrowserNodeAttrs(node,
			LBNA_Flags, LBFLG_CUSTOMPENS,
			LBNA_Column, 0,
			LBNCA_CopyText, TRUE,
			LBNCA_FGPen, 1, 
			LBNCA_Text, memText1,
			LBNA_Column, 1,
			LBNCA_CopyText, TRUE,
			LBNCA_FGPen, showUpdates && strcmp(oldMemText2,memText2) ? 2 : 1,
			LBNCA_Text, memText2,
			LBNA_Column, 2,
			LBNCA_CopyText, TRUE,
			LBNCA_FGPen, showUpdates && strcmp(oldMemText3,memText3) ? 2 : 1,
			LBNCA_Text, memText3,
			LBNA_Column, 3,
			LBNCA_CopyText, TRUE,
			LBNCA_FGPen, showUpdates && strcmp(oldMemText4,memText4) ? 2 : 1,
			LBNCA_Text, memText4,
			LBNA_Column, 4,
			LBNCA_CopyText, TRUE,
			LBNCA_FGPen, showUpdates && strcmp(oldMemText5,memText5) ? 2 : 1,
			LBNCA_Text, memText5,
			TAG_DONE);
		
	}
}

void UpdateMemoryBrowser(struct Codecraft *ads, BOOL showUpdates)
{
	ULONG i;
	UBYTE * memoryBrowserAddr;
	char *addrText;
	ULONG type;

	GetAttr(CHOOSER_Selected, ads->memoryBrowserTypeChooser, (ULONG *)&type);

	GetAttr(STRINGA_TextVal, ads->memoryBrowserAddrStr, (ULONG *)&addrText);
	if (strlen(addrText))
	{
		memoryBrowserAddr = (UBYTE *)strtoul(addrText,NULL,16);
		for (i=0; i<8; i++)
		{
			AddMem(ads,i,memoryBrowserAddr+(i<<4),type, showUpdates);
		}
	}

	SetGadgetAttrs(	(struct Gadget *) ads->memoryBrowser, ads->window, NULL,
	LISTBROWSER_Labels, (ULONG) &ads->memoryBrowserList,
	TAG_DONE
	);
}

void browseMemoryFromAddress(struct Codecraft *ads, UBYTE *addr)
{
	char addrText[16];

	SNPrintf( addrText, 16, "0x%08lx", addr);
		SetGadgetAttrs(	(struct Gadget *) ads->memoryBrowserAddrStr, ads->window, NULL,
		STRINGA_TextVal, addrText,
		TAG_DONE
		);
	
	UpdateMemoryBrowser(ads, FALSE);

}

void browseMemory(struct Codecraft *ads)
{
	UBYTE *addrText;

	GetAttr(STRINGA_TextVal, ads->memoryBrowserAddrStr, (ULONG *)&addrText);
	browseMemoryFromAddress(ads,(UBYTE *)strtoul(addrText,NULL,16));
}

void browseMemoryDown(struct Codecraft *ads)
{
	UBYTE *addrText1;
	char addrText2[16];
	ULONG addr;

  GetAttr(STRINGA_TextVal, ads->memoryBrowserAddrStr, (ULONG *)&addrText1);
	if (strlen(addrText1))
	{
		addr = strtoul(addrText1,NULL,16);
	
		SNPrintf( addrText2, 16, "0x%08lx", addr+16);
			SetGadgetAttrs(	(struct Gadget *) ads->memoryBrowserAddrStr, ads->window, NULL,
			STRINGA_TextVal, addrText2,
			TAG_DONE
			);

		UpdateMemoryBrowser(ads, FALSE);
	}
}

void browseMemoryUp(struct Codecraft *ads)
{
	UBYTE *addrText1;
	char addrText2[16];
	UBYTE *addr;

  GetAttr(STRINGA_TextVal, ads->memoryBrowserAddrStr, (ULONG *)&addrText1);
	if (strlen(addrText1))
	{
		addr = (UBYTE *)strtoul(addrText1,NULL,16);
	
		SNPrintf( addrText2, 16, "0x%08lx", addr-16);
			SetGadgetAttrs(	(struct Gadget *) ads->memoryBrowserAddrStr, ads->window, NULL,
			STRINGA_TextVal, addrText2,
			TAG_DONE
			);

		UpdateMemoryBrowser(ads, FALSE);
	}
}

void createMemoryBrowser(struct Codecraft *ads)
{
	struct ColumnInfo *ci;
	struct Node *node;
	struct GfxBase *gfxb;
	ULONG fontWidth;
	
	NewList(&ads->memoryBrowserList);

	//get user preferred monospaced font
	gfxb = (struct GfxBase*)GfxBase;
	fontWidth = gfxb->DefaultFont->tf_XSize;

	ci = AllocLBColumnInfo(5,
				LBCIA_Column, 0,
					LBCIA_Title, "",
					LBCIA_Width, fontWidth*10,
				LBCIA_Column, 1,
					LBCIA_Title, "",
					LBCIA_Width, fontWidth*9,
				LBCIA_Column, 2,
					LBCIA_Title, "",
					LBCIA_Width, fontWidth*9,
				LBCIA_Column, 3,
					LBCIA_Title, "",
					LBCIA_Width, fontWidth*9,
				LBCIA_Column, 4,
					LBCIA_Title, "",
					LBCIA_Width, fontWidth*9,
				TAG_DONE);

	NewList(&memBrowserTypes);
	node = AllocChooserNode(CNA_Text, GetStr(MSG_BROWSE_MEMORY_HEX), TAG_DONE);
	if (node) AddTail(&memBrowserTypes, node);
	node = AllocChooserNode(CNA_Text, GetStr(MSG_BROWSE_MEMORY_ASCII), TAG_DONE);
	if (node) AddTail(&memBrowserTypes, node);
	
		ads->memoryBrowserLayout = 
      NewObject( LAYOUT_GetClass(), NULL, 
        LAYOUT_Orientation, LAYOUT_ORIENT_VERT,
        LAYOUT_AddChild, NewObject( LAYOUT_GetClass(), NULL, 
          LAYOUT_Orientation, LAYOUT_ORIENT_HORIZ,
          LAYOUT_FixedHoriz, FALSE,
          LAYOUT_FixedVert, FALSE,
          LAYOUT_AddChild, NewObject( NULL, "button.gadget",
            GA_ID, GLOBALGID_MEMORYBROWSERGOTO,
            GA_Text, GetStr(MSG_BROWSE_MEMORY_GOTO_ADDR),
            GA_RelVerify, TRUE,
            GA_TabCycle, TRUE,
          TAG_END),
          CHILD_WeightMinimum, TRUE,
          LAYOUT_AddChild, ads->memoryBrowserAddrStr = NewObject( STRING_GetClass(), NULL, 
						GA_ID, GLOBALGID_MEMORYBROWSERADDR,
            GA_RelVerify, TRUE,
            STRINGA_TextVal, "",
            STRINGA_MaxChars, 80,
          TAG_END),
          CHILD_MinWidth, 100,
					LAYOUT_AddChild, NewObject( NULL, "button.gadget",
            GA_ID, GLOBALGID_MEMORYBROWSERUP,
            GA_Text, "",
            GA_RelVerify, TRUE,
            BUTTON_AutoButton, BAG_UPARROW,
          TAG_END),
          CHILD_WeightMinimum, TRUE,
          LAYOUT_AddChild, NewObject( NULL, "button.gadget",
            GA_ID, GLOBALGID_MEMORYBROWSERDOWN,
            GA_Text, "",
            GA_RelVerify, TRUE,
            GA_TabCycle, TRUE,
            BUTTON_AutoButton, BAG_DNARROW,
          TAG_END),
          CHILD_WeightMinimum, TRUE,
  				LAYOUT_AddChild, ads->memoryBrowserTypeChooser = NewObject(CHOOSER_GetClass(), NULL, 
						GA_ID, GLOBALGID_MEMORYBROWSERTYPE,
						GA_RelVerify, TRUE,
						GA_TabCycle, TRUE,
						CHOOSER_PopUp, TRUE,
						CHOOSER_Selected, 0,
						CHOOSER_Labels, &memBrowserTypes,
						TAG_END),
					TAG_END),
        LAYOUT_AddChild, ads->memoryBrowser = NewObject( LISTBROWSER_GetClass(), NULL, 
          GA_ID, GLOBALGID_MEMORYBROWSER,
          GA_RelVerify, TRUE,
          GA_TextAttr, &fixedFont,
					LISTBROWSER_ColumnInfo, (ULONG)ci,
					LISTBROWSER_MultiSelect, FALSE,
          LISTBROWSER_ShowSelected, FALSE,
          LISTBROWSER_Separators, FALSE,
        TAG_DONE),
      TAG_DONE);

	SetGadgetAttrs((struct Gadget *)ads->utilityPageGroup, ads->window, NULL,
				PAGE_Add, ads->memoryBrowserLayout,
				TAG_END);
}

void freeMemoryBrowser(void)
{
	struct Node *node;
	struct Node *nextnode;

	if (&memBrowserTypes)
	{
		node = memBrowserTypes.lh_Head;
		while(nextnode = node->ln_Succ)
		{
			FreeChooserNode(node);
			node = nextnode;
		}
	}
}