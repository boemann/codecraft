#define VERSION		1
#define REVISION	15
#define DATE		"13.7.2024"
#define VERS		"Codecraft 1.15"
#define VSTRING		"Codecraft 1.15 (13.7.2024)\r\n"
#define VERSTAG		"\0$VER: Codecraft 1.15 (13.7.2024)"
