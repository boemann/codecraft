/* This file is part of Codecraft, an IDE running under AmigaOS 3.2.1 and later
 *
 * Copyright 2022 Camilla Boemann
 *
 * Codecraft is free software: you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2 as published by the Free Software Foundation.
 *
 * Codecrafr is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not,
 * see <https://www.gnu.org/licenses/>.
 */

#include <string.h>

#include <gadgets/texteditor.h>
#include <intuition/classusr.h>

#define __NOLIBBASE__
#include <proto/exec.h>

#define __CLIB_PRAGMA_LIBCALL
#include <clib/alib_protos.h>

#include <proto/intuition.h>
#include <proto/utility.h>
#include <proto/texteditor.h>
#include <proto/listbrowser.h>
#include <proto/locale.h>
#include <proto/dos.h>

#include <tools/textedit/extension.h>
#include "codecraft.h"
#include "breakpoints.h"
#include "debugger.h"

extern struct Library *SysBase;
extern struct Library *DOSBase;
extern struct Library *ListBrowserBase;
extern struct Library *IntuitionBase;
extern struct Library *LocaleBase;
extern struct Library *UtilityBase;
extern struct Locale *Locale;
extern struct Catalog *Catalog;

struct BreakPoint *BreakPointsFindLessOrEqual(struct CodecraftDoc *adsDoc, struct BreakPoint *currentBreakPoint, ULONG blockNum)
{
    if (!currentBreakPoint)
		currentBreakPoint = (struct BreakPoint *)&adsDoc->breakPointList;

    /* At start we are sure to we have something less or equal
     * It could however be a pointer to the list itself
     */
    while (currentBreakPoint->node.mln_Succ) /* this test is just to be absolutely safe */
    {
        struct BreakPoint *next = (struct BreakPoint *)currentBreakPoint->node.mln_Succ;

        /* This is the real test to ensure we stop before going too far */
        if ((next->node.mln_Succ == NULL) || next->blockNum > blockNum)
            return currentBreakPoint;

        currentBreakPoint = next;
    }

    return currentBreakPoint;
}

void updateBreakPointNode(struct BreakPoint *breakPoint)
{
	UBYTE buf[64];

	SNPrintf(buf, 64, "%s,   line %ld", FilePart(breakPoint->adsDoc->fullPath), breakPoint->blockNum + 1);

	SetListBrowserNodeAttrs(breakPoint->browserNode,
		LBNA_Column, 2,
		LBNCA_CopyText, TRUE,
		LBNCA_Text, buf,
		TAG_DONE);
}

void UpdateBreakPointBrowser(struct Codecraft *ads)
{
	struct Node *browserNode = ads->breakpointBrowserList.lh_Head;
	
	SetGadgetAttrs(	(struct Gadget *) ads->breakpointBrowser, ads->window, NULL,
		LISTBROWSER_Labels, ~0,
		TAG_DONE
		);

	while (browserNode->ln_Succ)
	{
		struct BreakPoint *breakPoint;
		
		GetListBrowserNodeAttrs(browserNode,
			LBNA_UserData, &breakPoint,
			TAG_DONE);
			
		updateBreakPointNode(breakPoint);
		
		browserNode = browserNode->ln_Succ;
	}

	SetGadgetAttrs(	(struct Gadget *) ads->breakpointBrowser, ads->window, NULL,
		LISTBROWSER_Labels, (ULONG) &ads->breakpointBrowserList,
		TAG_DONE
		);
}

void UpdateBreakPointMenuItems(struct Codecraft *ads)
{
	if (ads->breakpointBrowserList.lh_Head->ln_Succ == 0)
	{
		ItemAddress(ads->menu, FULLMENUNUM(2, 11, 0))->Flags &= ~ITEMENABLED;
		ItemAddress(ads->menu, FULLMENUNUM(2, 12, 0))->Flags &= ~ITEMENABLED;
		ItemAddress(ads->menu, FULLMENUNUM(2, 13, 0))->Flags &= ~ITEMENABLED;
	}
	else
	{
		ItemAddress(ads->menu, FULLMENUNUM(2, 11, 0))->Flags |= ITEMENABLED;
		ItemAddress(ads->menu, FULLMENUNUM(2, 12, 0))->Flags |= ITEMENABLED;
		ItemAddress(ads->menu, FULLMENUNUM(2, 13, 0))->Flags |= ITEMENABLED;
	}
}

void BreakPointsOnCharsDeleted(struct ChangeListener *listener, ULONG startBlockNum, ULONG startPosInBlock,
                             ULONG endBlockNum, ULONG endPosInBlock)
{
    struct CodecraftDoc *adsDoc = ((struct ChangeListenerCodecraft *)listener)->adsDoc;
    struct BreakPoint *breakPoint;
	struct Codecraft *ads = adsDoc->ads;
	ULONG delta = endBlockNum - startBlockNum;

    if (!delta)
        return;

    breakPoint = BreakPointsFindLessOrEqual(adsDoc, NULL, startBlockNum);

	if (breakPoint == (struct BreakPoint *)&adsDoc->breakPointList ||
						breakPoint->blockNum < startBlockNum ||
						(breakPoint->blockNum == startBlockNum && startPosInBlock))
		breakPoint = (struct BreakPoint *)breakPoint->node.mln_Succ;

	SetGadgetAttrs(	(struct Gadget *) ads->breakpointBrowser, ads->window, NULL,
		LISTBROWSER_Labels, (ULONG) NULL,
		TAG_DONE
		);

    while (breakPoint->node.mln_Succ)
    {
        struct BreakPoint *currentBreakPoint = breakPoint;

        breakPoint = (struct BreakPoint *)breakPoint->node.mln_Succ;

        if (currentBreakPoint->blockNum >= endBlockNum)
		{
            currentBreakPoint->blockNum -= delta;
			updateBreakPointNode(currentBreakPoint);
		}
		else
        {
            Remove((struct Node *)currentBreakPoint);
			Remove(currentBreakPoint->browserNode);
			FreeListBrowserNode(currentBreakPoint->browserNode);
			UninstallBreakPoint(currentBreakPoint);
			FreeBreakPoint(currentBreakPoint);
        }
    }
	SetGadgetAttrs(	(struct Gadget *) ads->breakpointBrowser, ads->window, NULL,
		LISTBROWSER_Labels, (ULONG) &ads->breakpointBrowserList,
		TAG_DONE
		);
		
	UpdateBreakPointMenuItems(ads);
}

void BreakPointsOnCharsInserted(struct ChangeListener *listener, ULONG startBlockNum, ULONG startPosInBlock,
                    ULONG endBlockNum, ULONG endPosInBlock)
{
    struct CodecraftDoc *adsDoc = ((struct ChangeListenerCodecraft *)listener)->adsDoc;
    struct BreakPoint *breakPoint;
	struct Codecraft *ads = adsDoc->ads;
	ULONG delta = endBlockNum - startBlockNum;

    if (!delta)
        return;

    breakPoint = BreakPointsFindLessOrEqual(adsDoc, NULL, startBlockNum);

	if (breakPoint == (struct BreakPoint *)&adsDoc->breakPointList ||
						breakPoint->blockNum < startBlockNum ||
						(breakPoint->blockNum == startBlockNum && startPosInBlock))
		breakPoint = (struct BreakPoint *)breakPoint->node.mln_Succ;


	SetGadgetAttrs(	(struct Gadget *) ads->breakpointBrowser, ads->window, NULL,
		LISTBROWSER_Labels, (ULONG) NULL,
		TAG_DONE
		);

    while (breakPoint->node.mln_Succ)
    {
		breakPoint->blockNum += delta;
		updateBreakPointNode(breakPoint);
		breakPoint = (struct BreakPoint *)breakPoint->node.mln_Succ;
    }

	SetGadgetAttrs(	(struct Gadget *) ads->breakpointBrowser, ads->window, NULL,
		LISTBROWSER_Labels, (ULONG) &ads->breakpointBrowserList,
		TAG_DONE
		);
}

void DeleteBreakPoint(struct BreakPoint *breakPoint)
{
	Remove((struct Node *)breakPoint);
	Remove(breakPoint->browserNode);
	FreeListBrowserNode(breakPoint->browserNode);
	UninstallBreakPoint(breakPoint);
	FreeBreakPoint(breakPoint);
}

void BreakPointsToggleAtBlockNum(struct CodecraftDoc *adsDoc, ULONG blockNum)
{
    struct BreakPoint *prevBreakPoint = BreakPointsFindLessOrEqual(adsDoc, NULL, blockNum);
	struct BreakPoint *breakPoint = NULL;
	struct Codecraft *ads = adsDoc->ads;

	SetGadgetAttrs(	(struct Gadget *) ads->breakpointBrowser, ads->window, NULL,
		LISTBROWSER_Labels, (ULONG) NULL,
		TAG_DONE
		);

	if (prevBreakPoint != (struct BreakPoint *)&adsDoc->breakPointList && prevBreakPoint->blockNum == blockNum)
    {
	    DeleteBreakPoint(prevBreakPoint);
    }
    else
        if (breakPoint = AllocVec(sizeof(struct BreakPoint), MEMF_ANY))
        {
			breakPoint->blockNum = blockNum;
			breakPoint->adsDoc = adsDoc;
			breakPoint->disabled = FALSE;
			breakPoint->autocontinue = FALSE;
			breakPoint->address = NULL;

            Insert((struct List *)&adsDoc->breakPointList, (struct Node *)breakPoint, (struct Node *)prevBreakPoint);

			if ( !InstallBreakPoint(breakPoint))
			{
				Remove((struct Node *)breakPoint);
				FreeBreakPoint(breakPoint);
				SetGadgetAttrs(	(struct Gadget *) ads->breakpointBrowser, ads->window, NULL,
					LISTBROWSER_Labels, (ULONG) &ads->breakpointBrowserList,
					TAG_DONE
					);
				return;
			}

			breakPoint->browserNode = AllocListBrowserNode(
				3,
				LBNA_Column, 0,
				LBNA_CheckBox, TRUE,
				LBNA_Checked, TRUE,
				LBNA_UserData, (ULONG)breakPoint,
				LBNA_Column, 1,
				LBNCA_Justification, LCJ_CENTER,
				LBNCA_Image, ads->Images[0],
				LBNA_Column, 2,
				LBNCA_Editable, FALSE,
				LBNCA_Justification, LCJ_LEFT,
				TAG_DONE);

			updateBreakPointNode(breakPoint);

			if (breakPoint->browserNode)
				if (prevBreakPoint != (struct BreakPoint *)&adsDoc->breakPointList)
					Insert(&ads->breakpointBrowserList, breakPoint->browserNode, prevBreakPoint->browserNode);
				else
				{
					struct Node *preNode = (struct Node *)&ads->breakpointBrowserList;
					struct Node *examineNode = preNode->ln_Succ;
					STRPTR docName;
					STRPTR examineName;

					GetListBrowserNodeAttrs(breakPoint->browserNode,
						LBNA_Column, 2,
						LBNCA_Text, &docName,
						TAG_DONE);

					// Let's do an ordered insertion, so let's find the place to insert
					while (examineNode->ln_Succ)
					{
						/* If the next Node has a filename lexically after our document
						 * then we are done and preNode will be just before
						 */
						GetListBrowserNodeAttrs(examineNode,
							LBNA_Column, 2,
							LBNCA_Text, &examineName,
							TAG_DONE);

						if (Stricmp(examineName, docName) >= 0)
							break;

						preNode = examineNode;
						examineNode = examineNode->ln_Succ;
					}

					Insert(&ads->breakpointBrowserList, breakPoint->browserNode, preNode);
				}
		}

	SetGadgetAttrs(	(struct Gadget *) ads->breakpointBrowser, ads->window, NULL,
		LISTBROWSER_Labels, (ULONG) &ads->breakpointBrowserList,
		TAG_DONE
		);

	UpdateBreakPointMenuItems(ads);
}

void revealBreakPoint(struct BreakPoint *breakPoint)
{
	struct CodecraftDoc *adsDoc = breakPoint->adsDoc;
	struct Codecraft *ads = adsDoc->ads;
	revealLocation(ads, adsDoc->fullPath, breakPoint->blockNum);
}

void resetBreakPointImages(struct Codecraft *ads)
{
	struct Node *browserNode = (struct Node *)ads->breakpointBrowserList.lh_Head;

	SetGadgetAttrs(	(struct Gadget *) ads->breakpointBrowser, ads->window, NULL,
		LISTBROWSER_Labels, ~0,
		TAG_DONE
		);

	while (browserNode->ln_Succ)
	{
		ULONG disabled;
		struct BreakPoint *breakPoint;
	
		GetListBrowserNodeAttrs(browserNode,
			LBNA_UserData, &breakPoint,
			TAG_DONE);

		disabled = breakPoint->disabled;
		
		SetListBrowserNodeAttrs(browserNode,
			LBNA_Checked, !disabled,
			LBNA_Column, 1,
			LBNCA_Image, ads->Images[0 | (disabled?BPIMG_DISABLED:0)],
			TAG_DONE);
			
		browserNode = browserNode->ln_Succ;
	}

	SetGadgetAttrs(	(struct Gadget *) ads->breakpointBrowser, ads->window, NULL,
	LISTBROWSER_Labels, (ULONG) &ads->breakpointBrowserList,
	TAG_DONE
	);
}

void setDisabledBreakPoint(struct Codecraft *ads, struct Node *browserNode, BOOL disabled)
{
	struct BreakPoint *breakPoint;

	GetListBrowserNodeAttrs(browserNode,
		LBNA_UserData, &breakPoint,
		TAG_DONE);

	if (breakPoint->disabled == disabled)
		return;

	breakPoint->disabled = disabled;

	if (disabled)
		UninstallBreakPoint(breakPoint);
	else
		InstallBreakPoint(breakPoint);

	SetGadgetAttrs(	(struct Gadget *) ads->breakpointBrowser, ads->window, NULL,
		LISTBROWSER_Labels, ~0,
		TAG_DONE
		);

	SetListBrowserNodeAttrs(browserNode,
		LBNA_Checked, !disabled,
		LBNA_Column, 1,
		LBNCA_Image, ads->Images[0 | (disabled?BPIMG_DISABLED:0)],
		TAG_DONE);

	SetGadgetAttrs(	(struct Gadget *) ads->breakpointBrowser, ads->window, NULL,
	LISTBROWSER_Labels, (ULONG) &ads->breakpointBrowserList,
	TAG_DONE
	);
}

void setDisableAllBreakPoints(struct Codecraft *ads, BOOL disable)
{
	struct Node *browserNode = (struct Node *)ads->breakpointBrowserList.lh_Head;

	while (browserNode->ln_Succ)
	{
		setDisabledBreakPoint(ads, browserNode, disable);
		browserNode = browserNode->ln_Succ;
	}
}

void deleteAllBreakPoints(struct Codecraft *ads)
{
	struct Node *browserNode = (struct Node *)ads->breakpointBrowserList.lh_Head;
	struct BreakPoint *breakPoint;

	SetGadgetAttrs(	(struct Gadget *) ads->breakpointBrowser, ads->window, NULL,
		LISTBROWSER_Labels, NULL,
		TAG_DONE
		);

	while (browserNode = RemTail(&ads->breakpointBrowserList))
	{
		GetListBrowserNodeAttrs(browserNode,
			LBNA_UserData, &breakPoint,
			TAG_DONE);
		Remove((struct Node *)breakPoint);
		FreeListBrowserNode(breakPoint->browserNode);
		UninstallBreakPoint(breakPoint);
		FreeBreakPoint(breakPoint);
	}

	SetGadgetAttrs(	(struct Gadget *) ads->breakpointBrowser, ads->window, NULL,
		LISTBROWSER_Labels, (ULONG) &ads->breakpointBrowserList,
		TAG_DONE
		);

	UpdateBreakPointMenuItems(ads);
}
