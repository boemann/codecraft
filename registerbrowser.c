/* This file is part of Codecraft, an IDE running under AmigaOS 3.2.1 and later
 *
 * Copyright 2022 Camilla Boemann
 *
 * Codecraft is free software: you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2 as published by the Free Software Foundation.
 *
 * Codecrafr is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with CodeCraft. If not,
 * see <https://www.gnu.org/licenses/>.
 */
#include <string.h>
#include <intuition/classusr.h>
#include <gadgets/listbrowser.h>

#include <proto/exec.h>

#define __CLIB_PRAGMA_LIBCALL
#include <clib/alib_protos.h>

#include <proto/intuition.h>
#include <proto/utility.h>
#include <proto/listbrowser.h>
#include <proto/layout.h>

#include <graphics/gfxbase.h>
#include <proto/graphics.h>

#include "debugger.h"
#include "codecraft.h"

extern struct DebuggerRegisters *brokenRegs;
extern UWORD *brokenAddress;
extern UWORD brokenSR;

extern struct TextAttr fixedFont;

void clearRegisterBrowser(struct Codecraft *ads)
{
	SetGadgetAttrs(	(struct Gadget *) ads->registerBrowser, ads->window, NULL,
		LISTBROWSER_Labels, (ULONG) NULL,
		TAG_DONE
		);

	FreeListBrowserList(&ads->registerBrowserList);

	SetGadgetAttrs(	(struct Gadget *) ads->registerBrowser, ads->window, NULL,
		LISTBROWSER_Labels, (ULONG) &ads->registerBrowserList,
		TAG_DONE
		);
}

struct Node *GetRegNode(struct Codecraft *ads, LONG num)
{
	struct Node *node = ads->registerBrowserList.lh_Head;
	struct Node *nextnode;
	LONG cnt=0;

	while (nextnode = node->ln_Succ)
	{
		if (cnt==num) return node;
		node = nextnode;
		cnt++;
	}
	return NULL;
}

void AddRow(struct Codecraft *ads, ULONG row, char *regText1, char *regText2, char *regText3)
{
	struct Node *node;
	STRPTR oldRegText1, oldRegText2, oldRegText3; 

	node = GetRegNode(ads, row);
	
	if (node == NULL)
	{
		node = AllocListBrowserNode(
			3,
			LBNA_Column, 0,
			LBNCA_CopyText, TRUE,
			LBNCA_Text, regText1,
			LBNA_Column, 1,
			LBNCA_CopyText, TRUE,
			LBNCA_Text, regText2,
			LBNA_Column, 2,
			LBNCA_CopyText, TRUE,
			LBNCA_Text, regText3,
			TAG_DONE);
		AddTail(&ads->registerBrowserList, node);
	}
	else
	{
		GetListBrowserNodeAttrs(node,
			LBNA_Column, 0,
			LBNCA_Text, &oldRegText1,
			LBNA_Column, 1,
			LBNCA_Text, &oldRegText2,
			LBNA_Column, 2,
			LBNCA_Text, &oldRegText3,
			TAG_DONE);
			
		SetListBrowserNodeAttrs(node,
			LBNA_Flags, LBFLG_CUSTOMPENS,
			LBNA_Column, 0,
			LBNCA_CopyText, TRUE,
			LBNCA_FGPen, strcmp(oldRegText1,regText1) ? 2 : 1,
			LBNCA_Text, regText1,
			LBNA_Column, 1,
			LBNCA_CopyText, TRUE,
			LBNCA_FGPen, strcmp(oldRegText2,regText2) ? 2 : 1,
			LBNCA_Text, regText2,
			LBNA_Column, 2,
			LBNCA_CopyText, TRUE,
			LBNCA_FGPen, strcmp(oldRegText3,regText3) ? 2 : 1,
			LBNCA_Text, regText3,
			TAG_DONE);
	}
}

void AddReg(struct Codecraft *ads, ULONG row, char *dregStr, ULONG dval, char *aregStr, ULONG aval)
{
	char regText1[64];
	char regText2[64];
	char regText3[64];

	SNPrintf(regText1, 64, "%s: 0x%08lx",dregStr,dval);
	SNPrintf(regText2, 64, "%s: 0x%08lx",aregStr,aval);
	
	if (TypeOfMem((UBYTE*)aval) && TypeOfMem((UBYTE*)(aval+7)))
	{
		SNPrintf(regText3, 64, "%02lx%02lx%02lx%02lx %02lx%02lx%02lx%02lx", *(UBYTE *)aval, *(UBYTE *)(aval+1), *(UBYTE *)(aval+2), *(UBYTE *)(aval+3), *(UBYTE *)(aval+4), *(UBYTE *)(aval+5), *(UBYTE *)(aval+6), *(UBYTE *)(aval+7));
	}
	else
	{
		SNPrintf(regText3, 64, "illegal address");
	}
	AddRow(ads, row, regText1, regText2, regText3);
}

void AddRegSR(struct Codecraft *ads, ULONG row, char *srRegStr, UWORD srval, char *aregStr, ULONG aval)
{
	char regText1[64];
	char regText2[64];
	char regText3[64];

	SNPrintf(regText1, 64, "%s: 0x%04lx",srRegStr,srval);
	SNPrintf(regText2, 64, "%s: 0x%08lx",aregStr,aval);
	
	if (TypeOfMem((UBYTE*)aval) && TypeOfMem((UBYTE*)(aval+7)))
	{
		SNPrintf(regText3, 64, "%02lx%02lx%02lx%02lx %02lx%02lx%02lx%02lx", *(UBYTE *)aval, *(UBYTE *)(aval+1), *(UBYTE *)(aval+2), *(UBYTE *)(aval+3), *(UBYTE *)(aval+4), *(UBYTE *)(aval+5), *(UBYTE *)(aval+6), *(UBYTE *)(aval+7));
	}
	else
	{
		SNPrintf(regText3, 64, "illegal address");
	}
	AddRow(ads, row, regText1, regText2, regText3);
	
	strcpy(regText1,"SR:        ");
	if (srval & 0x8000) regText1[4]='T';
	if (srval & 0x2000) regText1[5]='S';
	if (srval & 16) regText1[6]='X';
	if (srval & 8) regText1[7]='N';
	if (srval & 4) regText1[8]='Z';
	if (srval & 2) regText1[9]='V';
	if (srval & 1) regText1[10]='C';
	AddRow(ads, row+1,regText1,"", "");
}

void UpdateRegisterBrowser(struct Codecraft *ads)
{
	ULONG sp;
	
	sp = (ULONG)(brokenRegs+1)-4;
	AddReg(ads,0, "D0",brokenRegs->d0,"A0",brokenRegs->a0);
	AddReg(ads,1, "D1",brokenRegs->d1,"A1",brokenRegs->a1);
	AddReg(ads,2, "D2",brokenRegs->d2,"A2",brokenRegs->a2);
	AddReg(ads,3, "D3",brokenRegs->d3,"A3",brokenRegs->a3);
	AddReg(ads,4, "D4",brokenRegs->d4,"A4",brokenRegs->a4);
	AddReg(ads,5, "D5",brokenRegs->d5,"A5",brokenRegs->a5);
	AddReg(ads,6, "D6",brokenRegs->d6,"A6",brokenRegs->a6);
	AddReg(ads,7, "D7",brokenRegs->d7,"A7",sp);
	AddRegSR(ads,8, "SR",brokenSR,"PC",(ULONG)brokenAddress);

	SetGadgetAttrs(	(struct Gadget *) ads->registerBrowser, ads->window, NULL,
	LISTBROWSER_Labels, (ULONG) &ads->registerBrowserList,
	TAG_DONE
	);
}

void createRegisterBrowser(struct Codecraft *ads)
{
	struct ColumnInfo *ci;
	struct GfxBase *gfxb;
	ULONG fontWidth;

	NewList(&ads->registerBrowserList);

	//get user preferred monospaced font
	gfxb = (struct GfxBase*)GfxBase;
	fontWidth = gfxb->DefaultFont->tf_XSize;

	ci = AllocLBColumnInfo(3,
				LBCIA_Column, 0,
					LBCIA_Title, "",
					LBCIA_Width, fontWidth*16,
				LBCIA_Column, 1,
					LBCIA_Title, "",
					LBCIA_Width, fontWidth*16,
				LBCIA_Column, 2,
					LBCIA_Title, "",
					LBCIA_Width, fontWidth*25,
				TAG_DONE);

	ads->registerBrowser = NewObject(LISTBROWSER_GetClass(), NULL,
				GA_ID, GLOBALGID_REGISTERBROWSER,
				GA_RelVerify, TRUE,
				LISTBROWSER_ShowSelected, FALSE,
				LISTBROWSER_MultiSelect, FALSE,
				LISTBROWSER_Separators, FALSE,
				GA_TextAttr, &fixedFont,
				LISTBROWSER_ColumnInfo, (ULONG)ci,
				TAG_DONE);

	SetGadgetAttrs((struct Gadget *)ads->utilityPageGroup, ads->window, NULL,
				PAGE_Add, ads->registerBrowser,
				TAG_END);
}
