// An ID starts with a 1 byte tha can have these flag values
#define SRC6_ID_MORE			0x02	// It is followed by another ID
#define SRC6_ID_TAG				0x04	// It refers to another tag (length is 17 bytes)
#define SRC6_ID_AGGREGATE		0x08	// It is a struct (aggregate) followed by 1 TAG and
										// several members (MEM)
#define SRC6_ID_POINTER			0x10	// It is a pointer
#define SRC6_ID_CONTINUATION	0x20	// It is followed by a CTX
#define SRC6_ID_TDEF			0x40	// It refers to another tdef (length is 17 bytes)


// A MEM starts with a 1 byte that can have these flag values
#define SRC6_MEM_TDEF			0x20	// It refers to another tdef (length is 17 bytes)
#define SRC6_MEM_SUBTAG			0x02	// It refers to a tag that follows after
#define SRC6_MEM_TAG			0x01	// It refers to another tag (length is 17 bytes)
#define SRC6_MEM_POINTER		0x04	// It is a pointer
#define SRC6_MEM_BITFIELD		0x40	// It is a bitfield (size and offset present so length is 15 bytes)
#define SRC6_MEM_CONTINUATION	0x08	// It is followed by a CTX
#define SRC6_MEM_MORE			0x10	// It is followed by another MEM

// A TAG starts with a 1 byte that can have these flag values
#define SRC6_TAG_PARTOFTDEF		0x38	// tag as part of tdef
#define SRC6_TAG_SYMBOLIC		0x04	// symbolic tag (taglocation is present, and length +4)
#define SRC6_TAG_MORE			0x02	// It is followed by another TAG

#define SRC6_TAGATTR_UNION		0x00000001	// Tag is a union, It is followed by another TAG
#define SRC6_TAGATTR_ENUM		0x00000002	// Tag is a enum, followed by IDs rather than MEMbers
#define SRC6_TAGATTR_STRUCT		0x10000000	// Seems it means a plain struct

// An ID or MEM has a ULONG attribute field that can have these values

// Storage is mutually exclusive
#define SRC6_ATTR_SCOPE_GLOBAL		0x01000000	// global or global static variable
#define SRC6_ATTR_SCOPE_EXTERN		0x02000000	// extern variable
#define SRC6_ATTR_SCOPE_LOCAL		0x04000000	// local variable
#define SRC6_ATTR_SCOPE_ARGUMENT	0x08000000	// function argument

// Storage is mutually exclusive
#define SRC6_ATTR_STORAGE_FAR		0x00100000	// FAR data
#define SRC6_ATTR_STORAGE_NEAR		0x00200000	// NEAR data
#define SRC6_ATTR_STORAGE_CHIP		0x00800000	// CHIP data

// These define the basic type
#define SRC6_ATTR_TYPE_MASK			0x0000000F	// Mask to get the values below
#define SRC6_ATTR_TYPE_UNSIGNEDMASK	0x00000008	// Mask to get the values below
#define SRC6_ATTR_TYPE_SCHAR		0x00000000	// signed char
#define SRC6_ATTR_TYPE_SSHORT		0x00000001	// signed short
#define SRC6_ATTR_TYPE_SLONG		0x00000002	// signed long
#define SRC6_ATTR_TYPE_SLONGLONG	0x00000003	// signed long long (?)
#define SRC6_ATTR_TYPE_FLOAT		0x00000004	// float
#define SRC6_ATTR_TYPE_DOUBLE		0x00000005	// double
#define SRC6_ATTR_TYPE_LONGDOUBLE	0x00000006	// long double (?)
#define SRC6_ATTR_TYPE_VOID			0x00000007	// void
#define SRC6_ATTR_TYPE_UCHAR		0x00000008	// unsigned char
#define SRC6_ATTR_TYPE_USHORT		0x00000009	// unsigned short
#define SRC6_ATTR_TYPE_ULONG		0x0000000A	// unsigned long
#define SRC6_ATTR_TYPE_ULONGLONG	0x0000000B	// unsigned long long (?)

// The remaining are modifiers to be or'ed in:
#define SRC6_ATTR_INSCOPE			0x10000000	// Defined in scope (ie in this source file,
											// this function or this block)
#define SRC6_ATTR_REG				0x20000000	// Register. Offset is 0-7 Dregs, 8-15 Aregs
											// 16-23 Fregs
#define SRC6_ATTR_UNINITIALIZED		0x80000000	// Un-initialized
#define SRC6_ATTR_TYPEDEF			0x00040000	// TypeDef
#define SRC6_ATTR_TYPEDEF2			0x00002000	// also set when typedef
#define SRC6_ATTR_AUTOGEN			0x00000400	// Auto generated by compiler (eg to init another var)
#define SRC6_ATTR_VOLATILE			0x00000200	// Declared using volatile keyword
#define SRC6_ATTR_ALIGNED			0x00000020	// Aligned mem location
#define SRC6_ATTR_TAGMEMBER			0x00000080	// MEMber (not a var declaration)
