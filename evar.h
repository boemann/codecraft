/* This file is part of Codecraft, and IDE running under AmigaOS 3.2.1 and later
 *
 * Copyright 2022 Camilla Boemann
 *
 * Codecraft is free software: you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2 as published by the Free Software Foundation.
 *
 * Codecrafr is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not,
 * see <https://www.gnu.org/licenses/>.
 */

#define EVAR_BLOCK_END 0
#define EVAR_BLOCK_PARAM 1
#define EVAR_BLOCK_LOCAL 2
#define EVAR_BLOCK_GLOBAL 3
#define EVAR_BLOCK_PROC 4
#define EVAR_BLOCK_SELF 5
#define EVAR_BLOCK_VAREXTRA 6
#define EVAR_BLOCK_PROCEXTRA 7
#define EVAR_BLOCK_OBJECT 8

#define EVAR_VARTYPE_CHAR 1
#define EVAR_VARTYPE_BYTE 2
#define EVAR_VARTYPE_INT 3
#define EVAR_VARTYPE_WORD 4
#define EVAR_VARTYPE_LONG 5
#define EVAR_VARTYPE_OBJECT 6
#define EVAR_VARTYPE_PTR_TO_CHAR 0x21
#define EVAR_VARTYPE_PTR_TO_BYTE 0x22
#define EVAR_VARTYPE_PTR_TO_INT 0x23
#define EVAR_VARTYPE_PTR_TO_WORD 0x24
#define EVAR_VARTYPE_PTR_TO_LONG 0x25
#define EVAR_VARTYPE_PTR_TO_OBJECT 0x26
#define EVAR_VARTYPE_PTR_TO_PTR 0x27
#define EVAR_VARTYPE_ARRAY_OF_CHAR 0x41
#define EVAR_VARTYPE_ARRAY_OF_BYTE 0x42
#define EVAR_VARTYPE_ARRAY_OF_INT 0x43
#define EVAR_VARTYPE_ARRAY_OF_WORD 0x44
#define EVAR_VARTYPE_ARRAY_OF_LONG 0x45
#define EVAR_VARTYPE_ARRAY_OF_OBJECT 0x46
#define EVAR_VARTYPE_ARRAY_OF_PTR 0x47
#define EVAR_VARTYPE_ARRAY_OF_PTR_TO_CHAR 0x61
#define EVAR_VARTYPE_ARRAY_OF_PTR_TO_BYTE 0x62
#define EVAR_VARTYPE_ARRAY_OF_PTR_TO_INT 0x63
#define EVAR_VARTYPE_ARRAY_OF_PTR_TO_WORD 0x64
#define EVAR_VARTYPE_ARRAY_OF_PTR_TO_LONG 0x65
#define EVAR_VARTYPE_ARRAY_OF_PTR_TO_OBJECT 0x66
#define EVAR_VARTYPE_ARRAY_OF_PTR_TO_PTR 0x67
#define EVAR_VARTYPE_UNKNOWN 0xffff
