/* This file is part of Codecraft, an IDE running under AmigaOS 3.2.1 and later
 *
 * Copyright 2022 Camilla Boemann
 *
 * Codecraft is free software: you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2 as published by the Free Software Foundation.
 *
 * Codecrafr is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not,
 * see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>

#include <dos/dostags.h>
#include <dos/doshunks.h>

#include <tools/textedit/extension.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/utility.h>
#include <proto/intuition.h>
#include <proto/alib.h>

#include "codecraft.h"
#include "debugger.h"
#include "src6.h"

enum DecendMode {
	ONLYPARSE = 0,		  // parse so location is advanced
						  // but never actually add anything
	DECL,				  // Add the tag, but not members
						  // as members are added by expanding
						  // However it is important to decend
						  // and parse the members to get the correct
						  // following location.
						  // This is also important for substructs
	POSTPONEDDECL,		  // For calling ParseID when we know the ID
						  // is a typedef and not a decl.
	DECLBUTNOTTAGITSELF   // Used when user expands but in ParseTag
						  // it is transformed into DECL, as soon
						  // as we have skipped adding the tag
						  // itself
};

struct DebugInfoParserSRC6
{
	struct DebugInfoParser dip;

	ULONG initializedNearOffset;
	ULONG initializedFarOffset;
	ULONG uninitializedNearOffset;
	ULONG uninitializedFarOffset;
	ULONG chipOffset;
	ULONG initializedNearHunk;
	ULONG initializedFarHunk;
	ULONG uninitializedNearHunk;
	ULONG uninitializedFarHunk;
	ULONG chipHunk;
	UBYTE *dataMem[8];
	
	UBYTE *buffer;
	STRPTR stringTable;
	UBYTE *locationZero;
	ULONG GIHLocation;
	ULONG NumFuncs;
	ULONG ExtLocation;
	ULONG StaticLocation;
	ULONG FMAPLocation;
	ULONG IsAReg;
	ULONG handledFirstTypedef;
};

ULONG ParseMember(struct DebugInfoParserSRC6 *dip, ULONG location,
	ULONG level, enum DecendMode decl, UBYTE *memPtr);
ULONG ParseID(struct DebugInfoParserSRC6 *dip, ULONG location,
	ULONG level,
	enum DecendMode decl, UBYTE *memPtr);

extern struct DebuggerRegisters *brokenRegs;

static void ClearEntryInfo(struct DebugInfoParserSRC6 *dip)
{
	struct VariableBrowserInfo *vbi = &dip->dip.vbi;
	
	*vbi->nameString = *vbi->typeString = *vbi->valueString
		= *vbi->ptrString = *vbi->funcPtrString
		= *vbi->encodedTypeString = 0;
	dip->handledFirstTypedef = FALSE;
	dip->IsAReg = FALSE;
}
	
void RegisterData(struct DebugInfoParserSRC6 *dip, ULONG hunkNum, UBYTE *memPtr)
{
	while (dip->dip.node.ln_Succ)
	{
		dip->dataMem[hunkNum] = memPtr;
		dip = (struct DebugInfoParserSRC6 *)dip->dip.node.ln_Succ;
	}
}

void RegisterBSS(struct DebugInfoParserSRC6 *dip, ULONG hunkNum, UBYTE *memPtr)
{
	while (dip->dip.node.ln_Succ)
	{
		dip->dataMem[hunkNum] = memPtr;
		dip = (struct DebugInfoParserSRC6 *)dip->dip.node.ln_Succ;
	}
}

void ParseOffsets(struct DebugInfoParserSRC6 *dip, UBYTE *buf)
{
	ULONG *ptr = (ULONG *)buf;
	
	dip->initializedNearHunk = (*ptr) >>24;
	dip->initializedNearOffset = (*ptr++) & 0xFFFFFF;
	dip->initializedFarHunk = (*ptr) >>24;
	dip->initializedFarOffset = (*ptr++) & 0xFFFFFF;
	dip->uninitializedNearHunk = (*ptr) >>24;
	dip->uninitializedNearOffset = (*ptr++) & 0xFFFFFF;
	dip->uninitializedFarHunk = (*ptr) >>24;
	dip->uninitializedFarOffset = (*ptr++) & 0xFFFFFF;
	dip->chipHunk = (*ptr) >>24;
	dip->chipOffset = (*ptr) & 0xFFFFFF;
}

void ParseHeader(struct DebugInfoParserSRC6 *dip)
{
	UBYTE *buf = dip->buffer;
	ULONG stringTableSize;
	ULONG stringsLocation;
	ULONG afterStringsLocation;
	ULONG aligner[4] = {0, 3, 2, 1};

	dip->locationZero = buf + 12 + *((ULONG*)(buf+8));

	dip->GIHLocation = *((ULONG*)dip->locationZero);
	
	Strncpy(dip->dip.filename, buf+12, sizeof(dip->dip.filename));
	
	stringsLocation = *((ULONG*)(dip->locationZero+4));
	dip->stringTable = dip->locationZero + stringsLocation;
	
	stringTableSize = *((ULONG*)(dip->locationZero+8));

	afterStringsLocation = stringsLocation + stringTableSize;
	
	// it seems tablesize is always long aligned
	ParseOffsets(dip, dip->stringTable + stringTableSize
	+ aligner[afterStringsLocation&3]);
}

void ParseGIH(struct DebugInfoParserSRC6 *dip)
{
	UBYTE *buf = dip->locationZero;
	ULONG location = dip->GIHLocation;
	ULONG flags = *(buf + location);

	location++;
	
	dip->NumFuncs = 0;
	dip->ExtLocation = 0;
	dip->StaticLocation = 0;
	
//	kprintf("%4ld [%lx] GIH -\n",dip->GIHLocation, *buf);
	if (flags & 0x01)
	{
		dip->NumFuncs = *((UWORD*)(buf+location));
		location += 2;
	}
	if (flags & 0x02)
	{
		location += 4;
	}
	if (flags & 0x04)
	{
		location += 4;
	}
	if (flags & 0x08)
	{
		dip->StaticLocation = *((ULONG*)(buf+location));
		location += 4;
	}
	if (flags & 0x10)
	{
		dip->ExtLocation = *((ULONG*)(buf+location));
		location += 4;
	}
	
	dip->FMAPLocation = location;
}

ULONG sizeOfBasicType(ULONG typeid)
{
	ULONG size[] =
	{
		sizeof(signed char),
		sizeof(signed short),
		sizeof(signed long),
		4,//sizeof(signed long long),
		sizeof(float),
		sizeof(double),
		sizeof(long double),
		sizeof(void),
		sizeof(unsigned char),
		sizeof(unsigned short),
		sizeof(unsigned long),
		4//sizeof(unsigned long long)
	};
	return size[(typeid & 0xF)];
}

void printBasicType(struct DebugInfoParserSRC6 *dip, ULONG attrs)
{
	struct VariableBrowserInfo *vbi = &dip->dip.vbi;
	STRPTR types[] =
	{
		"signed char ",
		"signed short ",
		"signed long ",
		"signed long long ",
		"float ",
		"double ",
		"long double ",
		"void ",
		"unsigned char ",
		"unsigned short ",
		"unsigned long ",
		"unsigned long long "
	};
	Strncat(vbi->typeString, types[(attrs & SRC6_ATTR_TYPE_MASK)], MAXTYPELEN);
}

// Returns TRUE if  basic value or a pointer to basic vlaue
// Will also return FALSE if function pointer
ULONG isValueOrPtrTo(STRPTR encodedTypeString)
{
	if (encodedTypeString[9] == 0)
		return TRUE;
		
	if (encodedTypeString[9] == '*' && encodedTypeString[10] == 0)
	    return TRUE;
	    
	return FALSE;	
}

ULONG isStrPtr(STRPTR encodedTypeString)
{
	if (encodedTypeString[9] == 0)
		return FALSE;
		
	if (encodedTypeString[9] == '*' && encodedTypeString[10] == 0)
		return TRUE;

	if (encodedTypeString[9] == '[')// && encodedTypeString[11] == 0)
		return TRUE;
	
	return FALSE;
}

void writeStrFmtStr(STRPTR format, STRPTR s)
{
	int cnt = 0;

	while (*(s++) && cnt < 31)
		cnt++;
		
	if (cnt > 30)
		strcpy(format, "%s<addr> 0x%08lx (\"%.30s\"...)");
	else
		strcpy(format, "%s<addr> 0x%08lx (\"%.30s\")");
}

void printBasicValue(struct DebugInfoParserSRC6 *dip, ULONG typeid
	, UBYTE *memPtr, UBYTE bitFieldPos, UBYTE bitFieldWidth)
{
	struct VariableBrowserInfo *vbi = &dip->dip.vbi;
	ULONG value = 0;
	ULONG bitWidth;
	TEXT tmpBuf[40];
	STRPTR format = tmpBuf;
	STRPTR ptrString = vbi->ptrString;
	LONG illegal = FALSE;

	if ((typeid & SRC6_ATTR_TYPE_MASK) == SRC6_ATTR_TYPE_VOID || ! isValueOrPtrTo(vbi->encodedTypeString))
	{
		if (vbi->encodedTypeString[10] == 'F' )
			SNPrintf(vbi->valueString, sizeof(vbi->valueString),"<func> 0x%08lx", memPtr);
		else
		{
			if ((typeid & 0x7) == 0 && isStrPtr(vbi->encodedTypeString))
				SNPrintf(vbi->valueString, sizeof(vbi->valueString),"<addr> 0x%08lx (\"%.30s\"...)", memPtr, memPtr);
			else
				SNPrintf(vbi->valueString, sizeof(vbi->valueString),"<addr> 0x%08lx", memPtr);
		}
		return;
	}
		 
	if (*ptrString)
	{
		SNPrintf(vbi->valueString, sizeof(vbi->valueString),"<addr> 0x%08lx (", memPtr);
	}
	
	if (TypeOfMem(memPtr))
	{
		switch (typeid & SRC6_ATTR_TYPE_MASK)
		{
			case SRC6_ATTR_TYPE_SCHAR:
				if (isStrPtr(vbi->encodedTypeString))
				{
					value = *(ULONG *)memPtr;
					writeStrFmtStr(format, (STRPTR)value);
				}
				else
				{
					if (dip->IsAReg)
						value = *(BYTE *)(memPtr+3);
					else
						value = *(BYTE *)memPtr;
							
					if (isprint(value))
						format = "%s%ld ('%lc')";
					else
						format = "%s%ld (0x%02lx)";
				}
				bitWidth = 8;
				break;
				
			case SRC6_ATTR_TYPE_SSHORT:
				if (dip->IsAReg)
					memPtr += 2;
				value = *(WORD *)memPtr;
				format = "%s%ld (0x%04lx)";
				bitWidth = 16;
				break;
				
			case SRC6_ATTR_TYPE_SLONG:
				value = *(LONG *)memPtr;
				format = "%s%ld (0x%08lx)";
				bitWidth = 32;
	
				break;
				
			case SRC6_ATTR_TYPE_SLONGLONG:
				value = *(LONG *)memPtr;
				format = "%s%ld (0x%08lx)";
				bitWidth = 32;
				break;
				
			case SRC6_ATTR_TYPE_FLOAT:
				format = "%s%float not supported";
				break;
				
			case SRC6_ATTR_TYPE_DOUBLE:
				format = "%sdouble not supported";
				break;
				
			case SRC6_ATTR_TYPE_LONGDOUBLE:
				format = "%sdouble not supported";
				break;
				
			case SRC6_ATTR_TYPE_VOID:
				value = 0;
				format = "%s";
				break;
				
			case SRC6_ATTR_TYPE_UCHAR:
				if (isStrPtr(vbi->encodedTypeString))
				{
					value = *(ULONG *)memPtr;
					writeStrFmtStr(format, (STRPTR)value);
				}
				else
				{
					if (dip->IsAReg)
						value = *(UBYTE *)(memPtr+3);
					else
						value = *(UBYTE *)memPtr;
							
					if (isprint(value))
						format = "%s%lu ('%lc')";
					else
						format = "%s%lu (0x%02lx)";
				}
				bitWidth = 8;
				break;
				
			case SRC6_ATTR_TYPE_USHORT:
				if (dip->IsAReg)
					memPtr += 2;
				value = *(UWORD *)memPtr;
				format = "%s%lu (0x%04lx)";
				bitWidth = 16;
				break;
				
			case SRC6_ATTR_TYPE_ULONG:
				value = *(ULONG *)memPtr;
				format = "%s%lu (0x%08lx)";
				bitWidth = 32;
				break;
				
			case SRC6_ATTR_TYPE_ULONGLONG:
				value = *(ULONG *)memPtr;
				format = "%s%lu (0x%08lx)";
				bitWidth = 32;
				break;
				
		}
		if (bitFieldWidth)
		{
			if (typeid & SRC6_ATTR_TYPE_UNSIGNEDMASK)
			{
				value >>= (bitWidth - bitFieldPos - bitFieldWidth);
				value &= (1<<bitFieldWidth)-1;
			}
			else
			{
				//signed
				LONG svalue = (LONG)value;
				svalue <<= (32 - bitWidth + bitFieldPos);
				svalue >>= (32 - bitFieldWidth);
				value = (ULONG)svalue;
			}
		}	
	}
	else
	{
		if (memPtr == (void *)~0)
			format = "optimized away";
		else
		{
			format = "";
			illegal = TRUE;
		}
	}
		
	SNPrintf(vbi->valueString, sizeof(vbi->valueString),format,
			 vbi->valueString, value, value);

	if (illegal)
		SNPrintf(vbi->valueString, sizeof(vbi->valueString),"<illegal addr> 0x%08lx", memPtr);
	else
		if (*ptrString )
			Strncat(vbi->valueString, ")", MAXTYPELEN);
}

void ExpandArray(struct DebugInfoParserSRC6 *dip, STRPTR encodedType,
		ULONG level, UBYTE *memPtr,
		ULONG elementSize, ULONG skipPtrSteps)
{
	struct VariableBrowserInfo *vbi = &dip->dip.vbi;
	ULONG arrayLength = 0;
	ULONG i;
	STRPTR endp;
	ULONG pos = 9;
	ULONG location = (ULONG)strtoul(encodedType+1, NULL, 16);

	// we have continuation
	if (encodedType[pos] == '*')
	{
		skipPtrSteps++;
//				memPtr = *(UBYTE **)memPtr;
		pos++;
	}
	
	if (encodedType[pos] == '[')
	{
		arrayLength = (ULONG)strtoul(encodedType+pos+1, &endp, 10);
		skipPtrSteps++;
	}
	else if (encodedType[pos] == '*')
	{
		arrayLength = 1;
		skipPtrSteps++;
	}
	
	// See if our elements are pointers
 	while (encodedType[++pos])
		if (encodedType[pos] == '*')
		{
			elementSize = 4;
			break;
		}
		
	for (i = 0; i < arrayLength; i++)
	{
		vbi->skipPtrSteps = skipPtrSteps;
		vbi->totalSkipPtrSteps = skipPtrSteps;

		SNPrintf(vbi->nameString, MAXTYPELEN, "[%ld]", i);
		if (*encodedType == 'M')
			ParseMember(dip, location, level, DECL, memPtr);
		else
			ParseID(dip, location, level, DECL, memPtr);
		memPtr += elementSize;
	}
}

ULONG PrintEnumValue(struct DebugInfoParserSRC6 *dip, ULONG location, ULONG val)
{
	struct VariableBrowserInfo *vbi = &dip->dip.vbi;
	ULONG done = FALSE;
	ULONG found = FALSE;
		
	while (! done)
	{
		if (!found && *(ULONG *)(dip->locationZero + location + 9) == val)
		{
			Strncpy(vbi->valueString, dip->stringTable + *(ULONG *)(dip->locationZero + location + 1), MAXTYPELEN);
			found = TRUE;
		}
		done = (dip->locationZero[location] & 0x02) == 0;
		location += 13;
	}
				
	SNPrintf(vbi->valueString, sizeof(vbi->valueString),"%s (%ld, 0x%08lx)", vbi->valueString, val, val);

	return location;
}

ULONG ParseTag(struct DebugInfoParserSRC6 *dip, ULONG location,
	ULONG level, enum DecendMode decl, UBYTE *memPtr)
{
	struct VariableBrowserInfo *vbi = &dip->dip.vbi;
	UBYTE *buf = dip->locationZero + location;
	UBYTE flags = *buf;

//	kprintf("%4ld [%lx] TAG - ",location, *buf);

	if (flags & SRC6_TAG_PARTOFTDEF)
	{
		STRPTR name = dip->stringTable + *(ULONG *)(buf + 1);
		ULONG tagattrib = *(ULONG *)(buf + ((flags & SRC6_TAG_SYMBOLIC) ? 9 : 5));
		ULONG size = *(ULONG *)(buf + ((flags & SRC6_TAG_SYMBOLIC) ? 13 : 9));
		ULONG done = FALSE;
		ULONG expand = FALSE;
		ULONG isNotFunctionPtr = (vbi->encodedTypeString[9]
			 != '*' || vbi->encodedTypeString[10] != 'F');
		ULONG simpleTag = (vbi->encodedTypeString[9]
			 == 0 || vbi->encodedTypeString[10] == 0);
		ULONG isEnum = (tagattrib & SRC6_TAGATTR_ENUM);		

		if (tagattrib & SRC6_TAGATTR_UNION)
			Strncat(vbi->typeString, "union ", MAXTYPELEN);
		else if (tagattrib & SRC6_TAGATTR_ENUM)
			Strncat(vbi->typeString, "enum ", MAXTYPELEN);
		else
			Strncat(vbi->typeString, "struct ", MAXTYPELEN);
		
		if (*name)
			Strncat(vbi->typeString, name, MAXTYPELEN);
		else
			Strncat(vbi->typeString, "<unnamed>", MAXTYPELEN);
		
		Strncat(vbi->typeString, " ", MAXTYPELEN);
		Strncat(vbi->typeString, vbi->ptrString, MAXTYPELEN);
		Strncat(vbi->typeString, vbi->funcPtrString, MAXTYPELEN);

		if (isValueOrPtrTo(vbi->encodedTypeString))
		{
			SNPrintf(vbi->encodedTypeString, MAXTYPELEN, "S%08lx", location);
		}

		vbi->elementSize *= size;
		
		location += 13 + ((flags & SRC6_TAG_SYMBOLIC) ? 4 : 0);

		if (isNotFunctionPtr)
		{
			if (isEnum)
				location = PrintEnumValue(dip, location, *(LONG *)memPtr);
			else
				SNPrintf(vbi->valueString, sizeof(vbi->valueString),"<addr> 0x%08lx", memPtr);
		}
		else
			SNPrintf(vbi->valueString, sizeof(vbi->valueString),"<func> 0x%08lx", memPtr);

		if (decl != ONLYPARSE)
		{
			if (decl != DECLBUTNOTTAGITSELF)
				expand = createOrUpdateEntry(level, &dip->dip.vbi,
					isNotFunctionPtr && !isEnum, memPtr);
			else
			{
				decl = DECL;
				expand = TRUE;
			}
		}

		if (simpleTag)
		{
			ClearEntryInfo(dip);

			if (!expand)
				decl = ONLYPARSE;
				
			if (!isEnum)
			{
				// since not an enum we will parse members
				while (! done)
				{
					done = (dip->locationZero[location] & 0x10) == 0;
					location = ParseMember(dip, location, level+1, decl , memPtr);
				}
			}
		}
		else if (expand)
		{
			TEXT encodedType[sizeof(vbi->encodedTypeString)];
			
			Strncpy(encodedType, vbi->encodedTypeString, sizeof(encodedType));
			ClearEntryInfo(dip);
			ExpandArray(dip, encodedType, level+1, memPtr,
				 vbi->elementSize, vbi->skipPtrSteps);
		}
		ClearEntryInfo(dip);
	}
	else
		location += 5;

	return location;
}

ULONG ParseContinuation(struct DebugInfoParserSRC6 *dip, ULONG *ptrDepth, ULONG location,
 ULONG *sizeMultiplier)
{
	struct VariableBrowserInfo *vbi = &dip->dip.vbi;
	UBYTE *buf = dip->locationZero + location;
	ULONG flags = *buf;

	location++;
	
	if (flags & 0x1) // function
	{
		if (*ptrDepth >= vbi->skipPtrSteps)
			;
		if (!dip->handledFirstTypedef)
		{
			SNPrintf(vbi->funcPtrString, MAXTYPELEN, "(%s)()", vbi->ptrString);
			*vbi->ptrString = 0;
		}
		Strncat(vbi->encodedTypeString, "F", MAXTYPELEN);
	}
	
	if (flags & 0x4) // array
	{
		if (*ptrDepth >= vbi->skipPtrSteps)
		{
			ULONG len = *(ULONG *)(buf + 1);
			if (len == ~0)
			{			
				Strncat(vbi->encodedTypeString, "[]", MAXTYPELEN);
			}
			else
			{
				SNPrintf(vbi->encodedTypeString, MAXTYPELEN, "%s[%ld]", vbi->encodedTypeString, len);

				if (!dip->handledFirstTypedef)
				{
					if (*vbi->ptrString == '*')
					{
						strins(vbi->ptrString, "(");
						SNPrintf(vbi->ptrString, MAXTYPELEN, "%s)[%ld]", vbi->ptrString, len);
					}
					else
						SNPrintf(vbi->ptrString, MAXTYPELEN, "%s[%ld]", vbi->ptrString,len);
				}
				
				if (*sizeMultiplier)
					*sizeMultiplier *= len;
				else
					*sizeMultiplier = 1;
			}
		}
		location += 4;
	}
	
	if (flags & 0x02) //pointer
	{
		if (*ptrDepth >= vbi->skipPtrSteps)
		{
			Strncat(vbi->encodedTypeString, "*", MAXTYPELEN);
		
			// Insert * at beginning of vbi->ptrString
			if (!dip->handledFirstTypedef)
	 			strins(vbi->ptrString, "*");
		}			
		
	}
	
	(*ptrDepth)++;
	if (flags & 0x08) //there is more
		location = ParseContinuation(dip, ptrDepth, location, sizeMultiplier);
		
	return location;
}

ULONG ParseMember(struct DebugInfoParserSRC6 *dip, ULONG location, ULONG level,
	enum DecendMode decl, UBYTE *memPtr)
{
	struct VariableBrowserInfo *vbi = &dip->dip.vbi;
	UBYTE *buf = dip->locationZero + location;
	ULONG flags = *buf;
	STRPTR name = dip->stringTable + *(ULONG *)(buf + 1);
	ULONG offset;
	ULONG ptrDepth = 0;
		
	SNPrintf(vbi->encodedTypeString, MAXTYPELEN, "M%08lx", location);

	location += 13;
	
	if (flags & (SRC6_MEM_TDEF | SRC6_MEM_TAG)) // ref to another typdef or enum
		location += 4;// for the extra ref
	if (flags & SRC6_MEM_BITFIELD)
		location += 2;

	offset = *(ULONG *)(dip->locationZero + (location - 4));

	if (! *vbi->nameString)
	{
		// we are populating the members as opposed to
		// expanding a member that is an array
		Strncat(vbi->nameString, name, MAXTYPELEN);
		memPtr += offset;
	}

	// Time to handle ptr and arrays	
	if (flags & SRC6_MEM_POINTER)
	{
		if (vbi->skipPtrSteps == 0)
		{
			Strncat(vbi->encodedTypeString, "*", MAXTYPELEN);
			strins(vbi->ptrString, "*");
		}
			
		ptrDepth++;
	}

	if (vbi->encodedTypeString[9] == '*')
		memPtr = *(UBYTE **)memPtr; // was a pointer
		
	vbi->elementSize = 0;
	if (flags & SRC6_MEM_CONTINUATION)
		location = ParseContinuation(dip, &ptrDepth, location, &vbi->elementSize);

    // And now for the actual types
	if (flags & SRC6_MEM_TDEF) // ref to another typdef
	{
		ULONG tdeflocation = *(ULONG *)(buf + 5);
		if (decl != ONLYPARSE)
			ParseID(dip, tdeflocation, level, POSTPONEDDECL,  memPtr);
	}
	else if (flags & SRC6_MEM_TAG)
	{
		ULONG taglocation = *(ULONG *)(buf + 5);
		if (decl != ONLYPARSE)
			ParseTag(dip, taglocation,  level,  decl,  memPtr);
	}
	else if (flags & SRC6_MEM_SUBTAG)
	{
		location = ParseTag(dip, location, level, decl, memPtr);
	}
	else
	{
		ULONG expand = FALSE;
		ULONG attribs = *(ULONG *)(buf + 5);
		UBYTE bitFieldPos = 0;
		UBYTE bitFieldWidth = 0;
		ULONG hasChildren = (vbi->encodedTypeString[9] != '*'
		 || vbi->encodedTypeString[10] != 'F' ) &&
				!isValueOrPtrTo(vbi->encodedTypeString);
		printBasicType(dip, attribs);

		if (flags & SRC6_MEM_BITFIELD) // bitfield
		{
			bitFieldPos = *(UBYTE *)(buf + 9);
			bitFieldWidth = *(UBYTE *)(buf + 10);
			SNPrintf(vbi->typeString, MAXTYPELEN, "%s:%ld", vbi->typeString, bitFieldWidth);
		}
			
		Strncat(vbi->typeString, vbi->ptrString, MAXTYPELEN);
		Strncat(vbi->typeString, vbi->funcPtrString, MAXTYPELEN);
		
		printBasicValue(dip, attribs, memPtr, bitFieldPos, bitFieldWidth);

		if (*vbi->ptrString = '*')
			vbi->elementSize *= 4;
		else
			vbi->elementSize *= sizeOfBasicType(attribs);

		if (decl != ONLYPARSE)
			expand = createOrUpdateEntry(level, &dip->dip.vbi, hasChildren, memPtr);
		
		if (expand)
		{
			TEXT encodedType[sizeof(vbi->encodedTypeString)];
			
			Strncpy(encodedType, vbi->encodedTypeString, sizeof(encodedType));
			ClearEntryInfo(dip);
			ExpandArray(dip, encodedType, level+1, memPtr,
				 vbi->elementSize, vbi->skipPtrSteps);
		}
	}
	
	ClearEntryInfo(dip);

	return location;
}

UBYTE *calcActualMemPtr(struct DebugInfoParserSRC6 *dip, ULONG level,
		ULONG attrib, LONG offset, UBYTE *memPtr)
{
	struct VariableBrowserInfo *vbi = &dip->dip.vbi;
	STRPTR name = vbi->nameString;

	if (SRC6_ATTR_TYPEDEF & attrib)
		return memPtr;

	dip->IsAReg = FALSE;

	if (!level)
	{
		if (SRC6_ATTR_SCOPE_GLOBAL & attrib)
		{
			// global or global static
			// memory can be near or far;
			if (SRC6_ATTR_STORAGE_NEAR & attrib)
			{
				memPtr = (UBYTE *)brokenRegs->a4;
				if (SRC6_ATTR_UNINITIALIZED & attrib)
					memPtr += dip->uninitializedNearOffset + offset;
				else
					memPtr +=  dip->initializedNearOffset + offset;
			}
			else if (SRC6_ATTR_STORAGE_CHIP & attrib)
			{
				memPtr = dip->dataMem[dip->chipHunk];
				memPtr += dip->chipOffset + offset;
			}
			else
			{
				if (SRC6_ATTR_UNINITIALIZED & attrib) 
					memPtr = dip->dataMem[dip->uninitializedFarHunk]
						 + dip->uninitializedFarOffset + offset;
				else
					memPtr = dip->dataMem[dip->initializedFarHunk]
						 + dip->initializedFarOffset + offset;
			}				
		} else if (SRC6_ATTR_SCOPE_EXTERN & attrib)
		{
			UBYTE actualName[256];
			struct SymbolNode *symNode;
			struct List *symbolList = (struct List *)memPtr;
	
			SNPrintf(actualName, sizeof(actualName), "_%s", name);
			symNode = (struct SymbolNode *)FindName(symbolList, actualName);
	
			if (symNode)
				memPtr = symNode->memPtr;
			else
				memPtr = 0;

		}
		else if (SRC6_ATTR_REG & attrib)
		{
			if (offset < 8)
				SNPrintf(name, MAXTYPELEN, "%s [D%ld]", name, offset);
			else if (offset < 16)
				SNPrintf(name, MAXTYPELEN, "%s [A%ld]", name, offset-8);
			else
				SNPrintf(name, MAXTYPELEN, "%s [FP%ld]", name, offset-16);
			
			offset *= sizeof(ULONG);
	
			// Variables smaller than 4 bytes are offset further
			dip->IsAReg = TRUE;

			memPtr =  ((UBYTE*)brokenRegs) + offset;
		}
		else if (offset >= 0)
		{
			memPtr = (void *)~0; // optimized away
		}
		else
			memPtr =  memPtr + offset;
	}
	
	if (vbi->encodedTypeString[9] == '*')
	{
		dip->IsAReg = FALSE;
		memPtr = *(UBYTE **)memPtr; // was a pointer
	}
	
	return memPtr;
}

ULONG ParseID(struct DebugInfoParserSRC6 *dip, ULONG location,
	ULONG level,
	enum DecendMode decl, UBYTE *memPtr)
{
	struct VariableBrowserInfo *vbi = &dip->dip.vbi;
	UBYTE *buf = dip->locationZero + location;
	ULONG flags = *buf;
	ULONG ptrDepth = 0;
	
	//kprintf("%4ld [%lx] ID\n",location, *buf);
	
	if (! *vbi->encodedTypeString)
		SNPrintf(vbi->encodedTypeString, MAXTYPELEN, "I%08lx", location);
	location += 13;
	if (flags & (SRC6_ID_TAG | SRC6_ID_TDEF))
		location += 4;

	if (decl == DECL)
	{
		if (! *vbi->nameString)
			Strncpy(vbi->nameString, dip->stringTable + *(ULONG *)(buf + 1), MAXTYPELEN);
	}
	else
		if (!dip->handledFirstTypedef)
		{
			SNPrintf(vbi->typeString, MAXTYPELEN, "%s ", dip->stringTable + *(ULONG *)(buf + 1));
			Strncat(vbi->typeString, vbi->ptrString, MAXTYPELEN);
			Strncat(vbi->typeString, vbi->funcPtrString, MAXTYPELEN);
			dip->handledFirstTypedef = TRUE;
		}
	
	if (flags & SRC6_ID_POINTER)
	{
		if (vbi->skipPtrSteps == 0)
		{
			if( !dip->handledFirstTypedef)
				strins(vbi->ptrString, "*");
			Strncat(vbi->encodedTypeString, "*", MAXTYPELEN);
		}
		ptrDepth++;
	}
	
	if (decl == DECL)
		vbi->elementSize = 0;
	if (flags & SRC6_ID_CONTINUATION) //continuation
		location = ParseContinuation(dip, &ptrDepth, location,
		 &vbi->elementSize);

	if (flags & SRC6_ID_TDEF) // ref to another typdef
	{
		ULONG tdeflocation = *(ULONG *)(buf + 5);
		ULONG attrib = *(ULONG *)(buf + 9);
		LONG offset = *(LONG *)(buf + 13);
		memPtr = calcActualMemPtr(dip, level, attrib, offset, memPtr);
		if (vbi->skipPtrSteps >= ptrDepth)
			vbi->skipPtrSteps -= ptrDepth;
		ParseID(dip, tdeflocation, level, POSTPONEDDECL, memPtr);
	}
	else if (flags & SRC6_ID_TAG) // ref to another tag
	{
		ULONG taglocation = *(ULONG *)(buf + 5);
		ULONG attrib = *(ULONG *)(buf + 9);
		LONG offset = *(LONG *)(buf + 13);
		memPtr = calcActualMemPtr(dip, level, attrib, offset, memPtr);
			
		ParseTag(dip, taglocation,  level,  decl,  memPtr);
	}
	else if (flags & SRC6_ID_AGGREGATE)
	{
		ULONG attrib = *(ULONG *)(buf + 5);
		LONG offset = *(LONG *)(buf + 9);
		memPtr = calcActualMemPtr(dip, level, attrib, offset, memPtr);
			
		location = ParseTag(dip, location, level, decl, memPtr);
	}
	else
	{
		ULONG expand = FALSE;
		ULONG attrib = *(ULONG *)(buf + 5);
		LONG offset = *(LONG *)(buf + 9);
		ULONG hasChildren = (vbi->encodedTypeString[9] != '*'
		 || vbi->encodedTypeString[10] != 'F' ) &&
				!isValueOrPtrTo(vbi->encodedTypeString);
			
		if (!dip->handledFirstTypedef)
		{
			printBasicType(dip, attrib);
			Strncat(vbi->typeString, vbi->ptrString, MAXTYPELEN);
			Strncat(vbi->typeString, vbi->funcPtrString, MAXTYPELEN);
		}
		memPtr = calcActualMemPtr(dip, level, attrib, offset, memPtr);

		printBasicValue(dip, attrib, memPtr, 0, 0);
		
		if (*vbi->ptrString == '*')
			vbi->elementSize *= 4;
		else
			vbi->elementSize *= sizeOfBasicType(attrib);
		
		if (decl != ONLYPARSE && ! (SRC6_ATTR_AUTOGEN & attrib))
			expand = createOrUpdateEntry(level, &dip->dip.vbi, hasChildren, memPtr);
		
		if (expand)
		{
			TEXT encodedType[sizeof(vbi->encodedTypeString)];
			
			Strncpy(encodedType, vbi->encodedTypeString, sizeof(encodedType));
			ClearEntryInfo(dip);
			ExpandArray(dip, encodedType, level+1, memPtr,
				 vbi->elementSize, vbi->totalSkipPtrSteps);
		}
	}

	ClearEntryInfo(dip);

	return location;
}

ULONG ParseFUNC(struct DebugInfoParserSRC6 *dip, ULONG location, UBYTE *memPtr)
{
	UBYTE *buf = dip->locationZero + location;
	ULONG flags = *buf;
	ULONG seriesPresent = 0xF8 & *((UBYTE*)(buf+1));
	ULONG seriesAreDecl = 0xC8;
	
	location += 26;
	
	if (0x01 & *((UBYTE*)(buf+1)))  // returns a struct
		location += 4;		// location takes up 4 bytes
	else
		if (flags & 0x02)  // returns a typedef
			location += 4;		// takes up 4 bytes

	while (seriesPresent)
	{
		if (seriesPresent & 0x01)
		{
			ULONG done = FALSE;
			while (! done)
			{
				done = (dip->locationZero[location] & 0x02) == 0;
				location = ParseID(dip, location, 0, seriesAreDecl & 1 ? DECL : ONLYPARSE, memPtr);
			}
		}
		seriesPresent >>= 1;
		seriesAreDecl >>= 1;
	}
	return location;
}

void ParseBLK(struct DebugInfoParserSRC6 *dip, ULONG location
	, ULONG findThisBlock, UBYTE *memPtr)
{
	struct VariableBrowserInfo *vbi = &dip->dip.vbi;
	UBYTE *buf = dip->locationZero + location;
	ULONG flags = *buf;
	ULONG done;
	ULONG autoLocation;
	ULONG siblingLocation;
	ULONG finalChildLocation;
	ULONG firstBlockNum;
	ULONG lastBlockNum;
	ULONG lookingForThis;

	//kprintf("%4ld [%lx] BLK\n",location, *buf);
	
	location += 1;
	if (flags & 0x17)
	{
		autoLocation = *(ULONG *)(dip->locationZero +location);
		location += 4;
	}
	if (flags & 0x40)
	{
		finalChildLocation = *(ULONG *)(dip->locationZero +location);
		location += 4;
	}
	if (flags & 0x20)
	{
		// This is the sibling link
		siblingLocation = *(ULONG *)(dip->locationZero + location);
		location += 4;
	}
	firstBlockNum = *(UWORD *)(dip->locationZero + location);
	location += 2;	
	lastBlockNum = *(UWORD *)(dip->locationZero + location);
	location += 2;	
		
	lookingForThis = (findThisBlock <= lastBlockNum)
				&& (findThisBlock >= firstBlockNum);

	vbi->referenceBlockNum = firstBlockNum;

	if (lookingForThis && (flags & 0x10))
	{
		done = FALSE;
		if (flags & 0x01)
			while (! done)
			{
				done = (dip->locationZero[autoLocation] & 0x02) == 0;
				autoLocation = ParseTag(dip, autoLocation, 0, ONLYPARSE, NULL);
			}
		done = FALSE;
		if (flags & 0x02)
			while (! done)
			{
				done = (dip->locationZero[autoLocation] & 0x02) == 0;
				autoLocation = ParseID(dip, autoLocation, 0, ONLYPARSE, NULL);
			}
		done = FALSE;
		if (flags & 0x04)
			while (! done)
			{
			//FIXME how about statics in blocks
				done = (dip->locationZero[autoLocation] & 0x02) == 0;
				Strncpy(vbi->typeString, "static ", MAXTYPELEN);
				autoLocation = ParseID(dip, autoLocation, 0, DECL, memPtr);
			}
		done = FALSE;
		while (! done)
		{
			done = (dip->locationZero[autoLocation] & 0x02) == 0;
			autoLocation = ParseID(dip, autoLocation, 0, DECL, memPtr);
		}
	}

	if ((findThisBlock < firstBlockNum) && (flags & 0x20))
		ParseBLK(dip, siblingLocation, findThisBlock, memPtr);

	if (lookingForThis && (flags & 0x40))
		ParseBLK(dip, finalChildLocation, findThisBlock, memPtr);
}

ULONG ParseFMAP(struct DebugInfoParserSRC6 *dip, ULONG location
	, ULONG findThisBlock, UBYTE *memPtr)
{
	struct VariableBrowserInfo *vbi = &dip->dip.vbi;
	UBYTE *buf = dip->locationZero + location;
	ULONG flags = *buf;
	ULONG finalBLKLocation;
	ULONG firstBlockNum;
	ULONG lastBlockNum;
	//kprintf("%4ld [%lx] FMAP\n",location, *buf);

	location += 5;
	
	finalBLKLocation = *(ULONG *)(dip->locationZero + location);
	
	if (flags & 0x80)
		location += 4;

	firstBlockNum = *(UWORD *)(dip->locationZero + location);
	location += 2;	
	if (flags & 0x40)
	{
		lastBlockNum = *(UWORD *)(dip->locationZero + location);
		location += 2;
	}
	else
		lastBlockNum = firstBlockNum + (flags & 0x3F);
	
	if (findThisBlock < firstBlockNum)
		return location;
	if (findThisBlock > lastBlockNum)
		return location;
		
	vbi->referenceBlockNum = firstBlockNum;
	
	ParseFUNC(dip, *(ULONG *)(buf + 1), memPtr);

	if (flags & 0x80)
		ParseBLK(dip, finalBLKLocation, findThisBlock, memPtr);

	return location;
}
	
void Free(struct DebugInfoParserSRC6 *dip)
{
	FreeVec(dip->buffer);
	FreeVec(dip);
}

void ExpandSRC6Node(struct DebugInfoParserSRC6 *dip, struct Debugger *dbgr, ULONG blockNum,
		 STRPTR encodedType, ULONG level, ULONG elementSize,
		 ULONG skipPtrSteps, ULONG refBlockNum, 
		 UBYTE *memPtr)
{
	struct VariableBrowserInfo *vbi = &dip->dip.vbi;
	
	// we are called with a zerobased blocknum
	// but our data is 1 based
	blockNum++;

	ClearEntryInfo(dip);
	vbi->referenceBlockNum = refBlockNum;
	
	//encodedString is: 1 char + ULONG in hex + continuation
	if (encodedType[9])
	{
		// expandArray expects level of elements and not the array
		ExpandArray(dip, encodedType, level+1,
		 memPtr, elementSize, skipPtrSteps);
	}
	else
	{
		ULONG location = (ULONG)strtoul(encodedType+1, NULL, 16);
		vbi->skipPtrSteps = 0;
		vbi->totalSkipPtrSteps = 0;

		Strncpy(vbi->encodedTypeString, encodedType, MAXTYPELEN);
		ParseTag(dip, location, level, DECLBUTNOTTAGITSELF, memPtr);
	}
}

void ParseData(struct DebugInfoParserSRC6 *dip, struct Debugger *dbgr, ULONG blockNum, UBYTE *sp)
{
	struct VariableBrowserInfo *vbi = &dip->dip.vbi;
	struct Codecraft *ads = dbgr->ads;
	ULONG done = FALSE;
	ULONG location;
	ULONG i;

	// we are called with a zerobased blocknum
	// but our data is 1 based
	blockNum++;
					
	if (dip)
	{
	   	ads->nextVariableNode = ads->variableBrowserList.lh_Head;
	
		// a change of file means the global variables
		// are no longer the same
		if (ads->debugger->pathChanged)
			clearTrailingVariableNodes(ads);

		ClearEntryInfo(dip);
		vbi->skipPtrSteps = 0;
		vbi->totalSkipPtrSteps = 0;
		vbi->referenceBlockNum = 0;

		if (dip->ExtLocation)
		{
			location = dip->ExtLocation;
			done = FALSE;
			while (! done)
			{
				done = (dip->locationZero[location] & 0x02) == 0;
				location = ParseID(dip, location, 0, DECL, (UBYTE *)&dbgr->symbolList);
			}
		}
	
		if (dip->StaticLocation)
		{
			location = dip->StaticLocation;
			done = FALSE;
			while (! done)
			{
				done = (dip->locationZero[location] & 0x02) == 0;
				Strncpy(vbi->typeString, "static ", MAXTYPELEN);
				location = ParseID(dip, location, 0, DECL, NULL);
			}
		}
		
		i = dip->NumFuncs;
		location = dip->FMAPLocation;
		while (i--)
			location = ParseFMAP(dip, location, blockNum, sp);
	}
}

STRPTR LookupFunctionName(struct DebugInfoParserSRC6 *dip, ULONG *location, ULONG findThisBlock)
{
	UBYTE *buf = dip->locationZero + *location;
	ULONG flags = *buf;
	ULONG firstBlockNum;
	ULONG lastBlockNum;
	ULONG locationFUNC =  *(ULONG *)(dip->locationZero + *location + 1);
	
	*location += 5;
	
	if (flags & 0x80)
		*location += 4;

	firstBlockNum = *(UWORD *)(dip->locationZero + *location);
	*location += 2;
	if (flags & 0x40)
	{
		lastBlockNum = *(UWORD *)(dip->locationZero + *location);
		*location += 2;
	}
	else
		lastBlockNum = firstBlockNum + (flags & 0x3F);

	if (findThisBlock < firstBlockNum)
		return NULL;
	if (findThisBlock > lastBlockNum)
		return NULL;
		
	return dip->stringTable + *(ULONG *)(dip->locationZero + locationFUNC + 2);
}

ULONG LookupFunctionStackUsage(struct DebugInfoParserSRC6 *dip, ULONG findThisBlock, ULONG *argSize)
{
	LONG i = dip->NumFuncs;
	ULONG location = dip->FMAPLocation;

	while (i-- > 0)
	{
		UBYTE *buf = dip->locationZero + location;
		ULONG flags = *buf;
		ULONG firstBlockNum;
		ULONG lastBlockNum;
		ULONG locationFUNC =  *(ULONG *)(dip->locationZero + location + 1);
		ULONG extra = 0;
		
		location += 5;
		
		if (flags & 0x80)
			location += 4;
	
		firstBlockNum = *(UWORD *)(dip->locationZero + location);
		location += 2;
		if (flags & 0x40)
		{
			lastBlockNum = *(UWORD *)(dip->locationZero + location);
			location += 2;
		}
		else
			lastBlockNum = firstBlockNum + (flags & 0x3F);
	
		if (findThisBlock < firstBlockNum)
			continue;
		if (findThisBlock > lastBlockNum)
			continue;
	
		// if FUNC has  extra field set the locations change
		if (*(dip->locationZero + locationFUNC) & 0x02)
			extra = 4;
		else
			if (*(dip->locationZero + locationFUNC+1) & 0x01)
				extra = 4;
			 
		*argSize = *(ULONG *)(dip->locationZero + locationFUNC + 18 + extra);
		return *(ULONG *)(dip->locationZero + locationFUNC + 14 + extra);
	}

	return ~0;	
}

STRPTR LookupSRC6FunctionName(struct DebugInfoParserSRC6 *dip, struct Debugger *dbgr, ULONG blockNum)
{
	STRPTR funcName=NULL;
	ULONG i;
	ULONG location;
	blockNum++;

	i = dip->NumFuncs;
	location = dip->FMAPLocation;

	while (i-- && (funcName == NULL))
		funcName = LookupFunctionName(dip, &location, blockNum);
		
	return funcName;

}

ULONG ParseSRC6(struct Debugger *dbgr, BPTR file, ULONG totalLen)
{
	struct DebugInfoParserSRC6 *dip;
	
	dip = AllocVec(sizeof(struct DebugInfoParserSRC6), MEMF_ANY);
	if (! dip)
		return totalLen; // we havn't read anything
	
	dip->buffer = AllocVec(totalLen*4, MEMF_ANY);
	if (! dip->buffer)
	{
		FreeVec(dip);
		return totalLen; // we havn't read anything
	}
	
	dip->dip.Free = Free;
	dip->dip.ClearEntryInfo = ClearEntryInfo;
	dip->dip.RegisterData = RegisterData;
	dip->dip.RegisterBSS = RegisterBSS;
	dip->dip.ExpandNode = ExpandSRC6Node;
	dip->dip.ParseData = ParseData;
	dip->dip.LookupFunctionName = LookupSRC6FunctionName;
	dip->dip.LookupFunctionStackUsage = LookupFunctionStackUsage;

	FRead(file, dip->buffer, totalLen*4, 1);
	
	ParseHeader(dip);
	
	ParseGIH(dip);
	
	dip->dip.node.ln_Name = dip->dip.filename;

	AddTail(&dbgr->dipList, (struct Node *)dip);

	return 0;
}

