/* This file is part of Codecraft, an IDE running under AmigaOS 3.2.1 and later
 *
 * Copyright 2022 Camilla Boemann
 *
 * Codecraft is free software: you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2 as published by the Free Software Foundation.
 *
 * Codecrafr is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not,
 * see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>

#include <dos/dostags.h>
#include <dos/doshunks.h>

#include <tools/textedit/extension.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/utility.h>
#include <proto/intuition.h>
#include <proto/alib.h>

#include "codecraft.h"
#include "debugger.h"

extern struct DebuggerRegisters *brokenRegs;

void UpdateVariableBrowser(struct Debugger *dbgr, STRPTR fullPath, ULONG blockNum, UBYTE *sp)
{
	struct Codecraft *ads = dbgr->ads;
	struct DebugInfoParser *dip;

	if (fullPath)
	{
		dip = (struct DebugInfoParser *)FindName(&dbgr->dipList, fullPath);
	
		if (dip)
		{
		   	ads->nextVariableNode = ads->variableBrowserList.lh_Head;
		
			// a change of file means the global variables
			// are no longer the same
			if (ads->debugger->pathChanged)
				clearTrailingVariableNodes(ads);

			dip->ClearEntryInfo(dip);
			dip->vbi.skipPtrSteps = 0;
			dip->vbi.totalSkipPtrSteps = 0;
			dip->vbi.referenceBlockNum = 0;

			dip->ParseData(dip, dbgr, blockNum, sp);
		}
	}
		
	clearTrailingVariableNodes(ads);
}

void ExpandVariableBrowser(struct Debugger *dbgr, STRPTR fullPath, ULONG blockNum,
		 STRPTR encodedType, ULONG level, ULONG elementSize,
		 ULONG skipPtrSteps, ULONG refBlockNum, 
		 UBYTE *memPtr)
{
	struct DebugInfoParser *dip;
				
	dip = (struct DebugInfoParser *)FindName(&dbgr->dipList, fullPath);

	// The dip is known to be good as we have looked it up before
	// ads->nextVariableNode is already setup;

	dip->ClearEntryInfo(dip);
	dip->vbi.referenceBlockNum = refBlockNum;

	dip->ExpandNode(dip, dbgr, blockNum,
		 encodedType, level, elementSize,
		 skipPtrSteps, refBlockNum, 
		 memPtr);
}

UBYTE *advanceStackPointer(struct Debugger *dbgr, UBYTE *sp, UWORD *refPC, ULONG *argSize)
{
	STRPTR filename;
	ULONG lineNum=0;
	ULONG stackUsage = ~0;
 
	filename = LookupSourceLine(dbgr, refPC, &lineNum, NULL);

	if (lineNum)
	{
		struct DebugInfoParser *dip;
		
		dip = (struct DebugInfoParser *)FindName(&dbgr->dipList, filename);
		
		if (dip && dip->LookupFunctionStackUsage)
		{
			stackUsage = dip->LookupFunctionStackUsage(dip, lineNum + 1, argSize);

			if (stackUsage != ~0)
				sp += stackUsage;
		}
	}

	// Always add 4 bytes as we just saw a return address
	// on the stack
	return sp + 4;
}

void parseStackFrame(struct Debugger *dbgr, UBYTE *sp, UWORD *refPC)
{
	STRPTR funcName=NULL;
	STRPTR filename;
	ULONG blockNum=0;
	ULONG spOffset=0;
 
	filename = LookupSourceLine(dbgr, refPC, &blockNum, &spOffset);
	
	if (blockNum)
	{
		struct DebugInfoParser *dip;
		
		dip = (struct DebugInfoParser *)FindName(&dbgr->dipList, filename);
		
		if (dip)
			funcName = dip->LookupFunctionName(dip, dbgr, blockNum);
		
		sp += spOffset;
		
		if (dbgr->immediateReturnAddress == 0)
			dbgr->immediateReturnAddress = refPC;
	}
	
		
	createCallStackEntry(sp, funcName, filename, blockNum);
}

UBYTE *UpdateCallStackBrowser(struct Codecraft *ads, UWORD *brokenAddress)
{
	struct Debugger *dbgr = ads->debugger;
	struct Process *process = dbgr->process;
	UBYTE *sp = -4 + (UBYTE *)(brokenRegs+1); // stack is after regs
										// -4 is due how sp works
	UBYTE *firstSp = 0;
	UWORD *tmpPC = brokenAddress;
	ULONG argSize = 0;
		
	clearCallStackBrowser(ads);

	sp = advanceStackPointer(dbgr, sp, tmpPC, &argSize);
	// Subtract 4 as that is added always and that
	// is not correct the first time around
	sp -= 4; 
	
	// clear it here so we know we don't have any leftovers
	dbgr->immediateReturnAddress = 0;

	while (sp <= (UBYTE *)process->pr_Task.tc_SPUpper)
	{
		UWORD *returnAddress = (UWORD *)(*(ULONG *)sp);
		UWORD *opcodeAddress;
		ULONG found = FALSE;
		
		sp++; // we need to increment for next time around
			  // Do it here so we can just do continue
		
		opcodeAddress = returnAddress - 1;
		if ((opcodeAddress < (UWORD*)0xf80000 || opcodeAddress > (UWORD*)0xfc0000)
		  && !TypeOfMem(opcodeAddress))
			continue;
			
		if (((0xFFF8 & *opcodeAddress) == 0x4E90) // JSR reg
			|| (((0xFF00 & *opcodeAddress) == 0x6100)
			 && ((0xFF & *opcodeAddress) != 0x00)
			 && ((0xFF & *opcodeAddress) != 0xFF))) //BSR UBYTE
			found = TRUE;

		if (!found)
		{
			opcodeAddress--;
			
			if (((0xFFF8 & *opcodeAddress) == 0x4EA8) // JSR UWORD(reg)
				|| ((0xFFF8 & *opcodeAddress) == 0x4EE0) // JSR UBYTE(reg,reg)
				|| (*opcodeAddress == 0x4EB8) // JSR UWORD
				|| (*opcodeAddress == 0x4EBA) // JSR UWORD(pc)
				|| (*opcodeAddress == 0x4EBB) // JSR UBYTE(pc,reg)
				|| (*opcodeAddress == 0x6100)) // BSR UWORD
				found = TRUE;
		}
		
		if (!found)
		{
			opcodeAddress--;
				
			if ((*opcodeAddress == 0x4EB9) // JSR ULONG
				|| (*opcodeAddress == 0x61FF)) // BSR ULONG
				found = TRUE;
		}
		
		if (found)
		{
			sp--; // undo what we did at the top and then..
			parseStackFrame(dbgr, sp, tmpPC);
			if (! firstSp)
			{
				firstSp = sp;
				// clear this so it is set next parseStackFrame
				dbgr->immediateReturnAddress = 0;
			}
		

			sp += argSize;
			argSize = 0;
			tmpPC = returnAddress;
			found = FALSE;
			sp = advanceStackPointer(dbgr, sp, tmpPC, &argSize);
		}
	}
	return firstSp;
}
