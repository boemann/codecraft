/* This file is part of Codecraft, an IDE running under AmigaOS 3.2.1 and later
 *
 * Copyright 2022 Camilla Boemann
 *
 * Codecraft is free software: you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2 as published by the Free Software Foundation.
 *
 * Codecrafr is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not,
 * see <https://www.gnu.org/licenses/>.
 */
#include <string.h>
#include <intuition/classusr.h>
#include <gadgets/listbrowser.h>
#include <gadgets/layout.h>
#include <gadgets/clicktab.h>

#include <proto/exec.h>

#define __CLIB_PRAGMA_LIBCALL
#include <clib/alib_protos.h>

#include <proto/intuition.h>
#include <proto/graphics.h>
#include <proto/utility.h>
#include <proto/locale.h>
#include <proto/listbrowser.h>
#include <proto/layout.h>
#include <proto/clicktab.h>

#include "breakpoints.h"
#include "codecraft.h"
#include "debugger.h"
#include "stackanalyser.h"

extern struct Library *UtilityBase;
extern struct Locale *Locale;
extern struct Catalog *Catalog;

extern struct Codecraft myCodecraft;

void clearTrailingVariableNodes(struct Codecraft *ads)
{
	struct Node *tmp;
	struct Node *node = ads->nextVariableNode;
	
	if (node && node->ln_Succ)
	{
		while (tmp = RemTail(&ads->variableBrowserList))
		{
			FreeListBrowserNode(tmp);
			if (tmp == node)
				break;
		}
	}
		
	ads->nextVariableNode = ads->variableBrowserList.lh_TailPred->ln_Succ;
}

void clearSubGenNodes(struct Codecraft *ads, struct Node *node, int gen)
{
	struct Node *next;
	ULONG thisGen;
	
	node = node->ln_Succ;
	
	while (node->ln_Succ)
	{
		next = node->ln_Succ;
		
		GetListBrowserNodeAttrs(node,
			LBNA_Generation, &thisGen,
			NULL
		);

		if (thisGen <= gen)
			break;

		Remove(node);
		FreeListBrowserNode(node);
		node = next;
	}
}
ULONG bgpen = 0;

ULONG createOrUpdateEntry(ULONG level, struct VariableBrowserInfo *vbi,
   ULONG hasChildren, UBYTE *memPtr)
{
	struct Node *node;
	struct Codecraft *ads = &myCodecraft;
	
	node = ads->nextVariableNode;
	
	if (node->ln_Succ && !ads->expandVariableMode)
	{
		ULONG nodeBlockNum;
		ULONG flags;
		STRPTR oldStr;
		
		GetListBrowserNodeAttrs(node,
			LBNA_UserData, &nodeBlockNum,
			LBNA_Flags, &flags,
			LBNA_Column, 1,
			LBNCA_Text, &oldStr,
			TAG_DONE
		);

		if (nodeBlockNum != vbi->referenceBlockNum)
		{
			clearTrailingVariableNodes(ads);
		}
		else
		{
			ULONG showchildren = flags & LBFLG_SHOWCHILDREN;

			if (!showchildren)
				// To avoid any interference we delete subodes
				clearSubGenNodes(ads, node, level+1);
			
			// update value of node
			SetListBrowserNodeAttrs(node,
					LBNA_Column, 1,
					LBNCA_CopyText, TRUE,
			        LBNCA_FGPen, strcmp(oldStr,vbi->valueString) ? 2 : 1,
					LBNCA_Text, vbi->valueString,
					LBNA_Column, 4,
					LBNCA_CopyInteger, TRUE,
					LBNCA_Integer, &memPtr,
					TAG_DONE
			);
			
			ads->nextVariableNode = node->ln_Succ;
			
			// return with check for expanded subnodes
			return showchildren;
		}		
	}
	
	if (level == 0 && !ads->expandVariableMode)
	{
		if (bgpen == 0)
			bgpen = ads->variableBrowserAltPen;
		else
			bgpen = 0;
		if (node == ads->variableBrowserList.lh_Head)
			bgpen = 0;		
	}
		
	node = AllocListBrowserNode(
		7,
		LBNA_UserData, vbi->referenceBlockNum,
		LBNA_Generation, level+1,
		LBNA_Flags, LBFLG_CUSTOMPENS | (hasChildren ? LBFLG_HASCHILDREN : 0),
		LBNA_Column, 0,
        LBNCA_BGPen, bgpen,
        LBNCA_FGPen, 1,
		LBNCA_CopyText, TRUE,
		LBNCA_Text, vbi->nameString,
		LBNA_Column, 1,
        LBNCA_BGPen, bgpen,
        LBNCA_FGPen, 1,
		LBNCA_CopyText, TRUE,
		LBNCA_Text, vbi->valueString,
		LBNA_Column, 2,
        LBNCA_BGPen, bgpen,
        LBNCA_FGPen, 1,
		LBNCA_CopyText, TRUE,
		LBNCA_Text, vbi->typeString,
		LBNA_Column, 3,
		LBNCA_CopyText, TRUE,
		LBNCA_Text, vbi->encodedTypeString,
		LBNA_Column, 4,
		LBNCA_CopyInteger, TRUE,
		LBNCA_Integer, &memPtr,
		LBNA_Column, 5,
		LBNCA_CopyInteger, TRUE,
		LBNCA_Integer, &vbi->elementSize,
		LBNA_Column, 6,
		LBNCA_CopyInteger, TRUE,
		LBNCA_Integer, &vbi->totalSkipPtrSteps,
		TAG_DONE);

	// This will work both when building up a new list
	// where nextVariableNode is the end.
	// And when expanding a struct where nextVariableNode points
	// to the node after the struct we are expanding.
	Insert(&ads->variableBrowserList, node, ads->nextVariableNode->ln_Pred);

	return FALSE;
}

void createVariableBrowser(struct Codecraft *ads)
{
	struct ColumnInfo *ci;

	NewList(&ads->variableBrowserList);
	
	ci = AllocLBColumnInfo(3,
				LBCIA_Column, 0,
					LBCIA_Flags, CIF_DRAGGABLE,
					LBCIA_Width, 80,
				LBCIA_Column, 1,
					LBCIA_Flags, CIF_DRAGGABLE,
					LBCIA_Width, 280,
				LBCIA_Column, 2,
				TAG_DONE);

	ads->variableBrowser = NewObject(LISTBROWSER_GetClass(), NULL,
				GA_ID, GLOBALGID_VARIABLEBROWSER,
				GA_RelVerify, TRUE,
				LISTBROWSER_MultiSelect, FALSE,
				LISTBROWSER_Separators, TRUE,
				LISTBROWSER_ColumnInfo, (ULONG)ci,
				LISTBROWSER_Hierarchical, TRUE,
				LISTBROWSER_Labels, &ads->variableBrowserList,
				TAG_DONE);

	SetGadgetAttrs((struct Gadget *)ads->utilityPageGroup, ads->window, NULL,
				PAGE_Add, ads->variableBrowser,
				TAG_END);				
}

void clearVariableBrowser(struct Codecraft *ads)
{
	SetGadgetAttrs(	(struct Gadget *) ads->variableBrowser, ads->window, NULL,
		LISTBROWSER_Labels, (ULONG) NULL,
		TAG_DONE
		);

	FreeListBrowserList(&ads->variableBrowserList);

	SetGadgetAttrs(	(struct Gadget *) ads->variableBrowser, ads->window, NULL,
		LISTBROWSER_Labels, (ULONG) &ads->variableBrowserList,
		TAG_DONE
		);
}

void onVariableBrowserGadgetUp(struct Codecraft *ads)
{
	ULONG relevent;
	struct Node *node;
	struct Debugger *dbgr = ads->debugger;
	
	GetAttr(LISTBROWSER_SelectedNode, ads->variableBrowser, (ULONG *)&node);
	GetAttr(LISTBROWSER_RelEvent, ads->variableBrowser, &relevent);
	
	if (node && relevent == LBRE_DOUBLECLICK)
	{
		UBYTE **memPtr;

		GetListBrowserNodeAttrs(node,
			LBNA_Column, 4,
			LBNCA_Integer, (ULONG *)&memPtr,		
			NULL
		);
		browseMemoryFromAddress(ads, *memPtr);

		SetGadgetAttrs(	(struct Gadget *) dbgr->ads->utilityClickTab, dbgr->ads->window, NULL,
			CLICKTAB_Current, 5,
			TAG_DONE
			);
		
	}

	if (node && relevent == LBRE_SHOWCHILDREN)
	{
		struct Node *nextNode = node->ln_Succ;
		ULONG thisGeneration;
		ULONG nextGeneration;
		ULONG refBlockNum;
		STRPTR encodedLocation;
		UBYTE **memPtr;
		ULONG *elementSize;
		ULONG *skipPtrSteps;
		
		GetListBrowserNodeAttrs(node,
			LBNA_Generation, &thisGeneration,
			LBNA_UserData, &refBlockNum,
			LBNA_Column, 0,
	        LBNCA_BGPen, &bgpen,
			LBNA_Column, 3,
			LBNCA_Text, (ULONG *)&encodedLocation,
			LBNA_Column, 4,
			LBNCA_Integer, (ULONG *)&memPtr,
			LBNA_Column, 5,
			LBNCA_Integer, (ULONG *)&elementSize,
			LBNA_Column, 6,
			LBNCA_Integer, (ULONG *)&skipPtrSteps,
			NULL
		);

		if (nextNode->ln_Succ != NULL)
		{
			GetListBrowserNodeAttrs(nextNode,
				LBNA_Generation, &nextGeneration,
				NULL
			);
			if (nextGeneration == thisGeneration + 1)
				nextNode = NULL;
		}

		if (nextNode)
		{
			ads->nextVariableNode = node->ln_Succ;
			ads->expandVariableMode = TRUE;
			
			SetGadgetAttrs(	(struct Gadget *) ads->variableBrowser, ads->window, NULL,
				LISTBROWSER_Labels, NULL,
				TAG_DONE
				);
				
			// note that generation is always 1 more than the level
			// which the debugger is working with
			// see above where we always add 1
			// So subtract 1 here
			ExpandVariableBrowser(dbgr, dbgr->contextFullPath, dbgr->contextBlockNum, encodedLocation,
				thisGeneration-1, *elementSize, *skipPtrSteps, refBlockNum, *memPtr);
				
			ads->expandVariableMode = FALSE;
			
			SetGadgetAttrs(	(struct Gadget *) ads->variableBrowser, ads->window, NULL,
				LISTBROWSER_Labels, &ads->variableBrowserList,
				TAG_DONE
				);
		}
	}
}
