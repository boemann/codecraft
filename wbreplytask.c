#include <exec/ports.h>

#include <proto/exec.h>

#include "wbreplytask.h"

long __asm __saveds Main (void)
{
	struct MsgPort *mp;
	SysBase = (struct ExecBase *)*((struct Library **)(4L));

	if (mp = CreateMsgPort())
	{
		mp->mp_Node.ln_Name = WBREPLYPORTNAME;
		AddPort(mp);
		
		while (TRUE) // we never stop
		{
			struct WbRun *wbrun;
			WaitPort(mp);
			while (wbrun = (struct WbRun *)GetMsg(mp))
				FreeMem(wbrun, wbrun->Size);
		}
	}
	return 1;
}
