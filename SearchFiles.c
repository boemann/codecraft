/* This file is part of Codecraft, and IDE running under AmigaOS 3.2.1 and later
 *
 * Copyright 2023 Camilla Boemann
 *
 * Codecraft is free software: you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2 as published by the Free Software Foundation.
 *
 * Codecraft is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not,
 * see <https://www.gnu.org/licenses/>.
 */

/* includes */
#include <strings.h>
#include <ctype.h>

#include <exec/memory.h>
#include <intuition/intuition.h>
#include <intuition/gadgetclass.h>
#include <intuition/icclass.h>

#include <clib/alib_protos.h>

#include <proto/intuition.h>
#include <proto/gadtools.h>
#include <proto/intuition.h>
#include <proto/graphics.h>
#include <proto/utility.h>
#include <proto/locale.h>
#include <proto/dos.h>
#include <proto/icon.h>
#include <proto/exec.h>
#include <proto/window.h>
#include <proto/layout.h>
#include <proto/getfile.h>
#include <proto/listbrowser.h>
#include <proto/button.h>
#include <proto/checkbox.h>
#include <proto/clicktab.h>
#include <proto/label.h>
#include <proto/string.h>

#include "codecraft.h"
#include <tools/textedit/extension.h>

#include "iccallback.h"

enum gadids
{
  GID_OK = 1
 ,GID_CANCEL
 ,GID_SEARCHSTRING
 ,GID_CASESENSITIVE
 ,GID_WHOLEWORD
 ,GID_SEARCHPATH
 ,GID_RECURSE
 ,GID_PATTERN
 , MAXGADGETS
};

extern struct Codecraft myCodecraft;

static Object *gadgets[MAXGADGETS];

static struct Window *searchFilesWindow = NULL;
static Object *searchFilesWindowObj = NULL;
static Object *mainlayout;
static BOOL	firstActivation;

static ULONG matchCase=0,wholeWord=0, recurse=1;
static TEXT searchPattern[100];
static TEXT searchPath[MAXPATHLEN];
static TEXT searchText[256];

static struct Requester BlockingReq;

extern struct Library *UtilityBase;
extern struct Catalog *Catalog;
extern struct Locale *Locale;

#define CATCOMP_NUMBERS;
#include "strings.h"
const UBYTE *GetStr(LONG stringNum);

void closeSearchFiles(struct Codecraft *ads);
void createSearchFilesBrowser(struct Codecraft *ads);
void StartSearch(struct Codecraft *ads, STRPTR str);

__inline int charCompare(char a, char b, BOOL caseSensitive)
{
	if (!caseSensitive) b = tolower(b);
	return a==b;
}

void clearSearchInFilesPath()
{
	searchPath[0] = '\0';
}

int isWordMatch(STRPTR lineBuffer, ULONG endPos, LONG lineLen, ULONG matchLen)
{
	ULONG pos = endPos-(matchLen-1);
	endPos++;
	return (!pos || isspace(lineBuffer[pos-1])) && (((endPos) == lineLen) || isspace(lineBuffer[endPos]));
}

int isAllWhiteSpace(STRPTR s)
{
	int i;
	for (i=0; i<strlen(s); i++) if (!isspace(s[i])) return FALSE;
	return TRUE;
}

void openSearchFilesRequester(struct Codecraft *ads)
{
	ULONG sigmask;
	STRPTR text = NULL;
	struct CodecraftDoc *adsDoc = ads->currentAdsDocument;
	
	if (!strlen(searchPath))
	{
		NameFromLock(ads->projectDirLock, searchPath, MAXPATHLEN);
	}

	if (ads->currentAdsDocument)
	{
		text = ads->tei->executeRexxCommand(ads->currentAdsDocument->document, "GETSELECTEDTEXT");
		if (! text)
		{
			ads->tei->executeRexxCommand(ads->currentAdsDocument->document, "MARK Off");
			ads->tei->executeRexxCommand(ads->currentAdsDocument->document, "POSITION SOW");
			ads->tei->executeRexxCommand(ads->currentAdsDocument->document, "MARK On");
			ads->tei->executeRexxCommand(ads->currentAdsDocument->document, "POSITION EOW");
			text = ads->tei->executeRexxCommand(ads->currentAdsDocument->document, "GETSELECTEDTEXT");
		}
		
		if (text)
		{
			Strncpy(searchText, text, sizeof(searchText));
			FreeVec(text);
		}
	}
	
	searchFilesWindowObj = NewObject(WINDOW_GetClass(), NULL,
		WA_Activate, TRUE,
		WA_DragBar, TRUE,
		WA_DepthGadget, TRUE,
		WA_SizeGadget, TRUE,
		WA_Title, GetStr(MSG_SEARCHFILES_TITLE),
		WA_Left, ads->window->LeftEdge + 50,
		WA_Top, ads->window->TopEdge + 30,
		WA_Width, 350,
		WA_AutoAdjust, TRUE,
		WINDOW_GadgetHelp, TRUE,
		WA_IDCMP, IDCMP_CLOSEWINDOW|IDCMP_GADGETUP,
		WINDOW_Layout, mainlayout = NewObject(LAYOUT_GetClass(), NULL,
			LAYOUT_DeferLayout, TRUE,
			LAYOUT_Orientation, LAYOUT_ORIENT_VERT,
			LAYOUT_SpaceInner, TRUE,
			LAYOUT_SpaceOuter, TRUE,
			LAYOUT_AddChild, gadgets[GID_SEARCHSTRING] = NewObject(STRING_GetClass(), NULL,
				GA_ID, GID_SEARCHSTRING,
				GA_RelVerify, TRUE,
				GA_TabCycle, TRUE,
				STRINGA_TextVal, searchText,
				TAG_END),
			CHILD_WeightedHeight, 0,
			CHILD_Label, NewObject(LABEL_GetClass(), NULL, LABEL_Text, GetStr(MSG_SEARCHFILES_STR_LBL), TAG_END),
			LAYOUT_AddChild, NewObject(LAYOUT_GetClass(), NULL,
				TAG_DONE),
			CHILD_WeightedHeight, 0,
			LAYOUT_AddChild, gadgets[GID_CASESENSITIVE] = NewObject(CHECKBOX_GetClass(), NULL,
				GA_ID, GID_CASESENSITIVE,
				GA_RelVerify, TRUE,
				GA_Text, GetStr(MSG_SEARCHFILES_CASE_LBL),
				GA_TabCycle, TRUE,
				CHECKBOX_Checked, matchCase,
				TAG_END),
			CHILD_WeightedHeight, 0,
			CHILD_Label, NewObject(LABEL_GetClass(), NULL, LABEL_Text, "", TAG_END),
			LAYOUT_AddChild, gadgets[GID_WHOLEWORD] = NewObject(CHECKBOX_GetClass(), NULL,
				GA_ID, GID_WHOLEWORD,
				GA_RelVerify, TRUE,
				GA_Text, GetStr(MSG_SEARCHFILES_WHOLE_LBL),
				GA_TabCycle, TRUE,
				CHECKBOX_Checked, wholeWord,
				TAG_END),
			CHILD_WeightedHeight, 0,
			CHILD_Label, NewObject(LABEL_GetClass(), NULL, LABEL_Text, "", TAG_END),
			LAYOUT_AddChild, NewObject(LAYOUT_GetClass(), NULL,
				TAG_DONE),
			CHILD_WeightedHeight, 0,
			LAYOUT_AddChild, gadgets[GID_SEARCHPATH] = NewObject(GETFILE_GetClass(), NULL,
				GA_ID, GID_SEARCHPATH,
				GA_RelVerify, TRUE,
				GA_TabCycle, TRUE,
				GETFILE_RejectIcons, TRUE,
				GETFILE_DrawersOnly, TRUE,
				GETFILE_TitleText, GetStr(MSG_NEWTREE_SELECTDRAWER),
				GETFILE_Drawer, searchPath,
				TAG_END),
			CHILD_Label, NewObject(LABEL_GetClass(), NULL, LABEL_Text, GetStr(MSG_SEARCHFILES_WHERE_LBL), TAG_END),
			CHILD_WeightedHeight, 0,			
			LAYOUT_AddChild, gadgets[GID_RECURSE] = NewObject(CHECKBOX_GetClass(), NULL,
				GA_ID, GID_RECURSE,
				GA_RelVerify, TRUE,
				GA_Text, GetStr(MSG_SEARCHFILES_RECURSE),
				GA_TabCycle, TRUE,
				CHECKBOX_Checked, recurse,
				TAG_END),
			CHILD_Label, NewObject(LABEL_GetClass(), NULL, LABEL_Text, "", TAG_END),
			CHILD_WeightedHeight, 0,

			LAYOUT_AddChild, gadgets[GID_PATTERN] = NewObject(STRING_GetClass(), NULL,
				GA_ID, GID_PATTERN,
				GA_RelVerify, TRUE,
				GA_TabCycle, TRUE,
				STRINGA_TextVal, searchPattern,
				STRINGA_MaxChars, 100,
				TAG_END),
			CHILD_WeightedHeight, 0,
			CHILD_Label, NewObject(LABEL_GetClass(), NULL, LABEL_Text, GetStr(MSG_SEARCHFILES_PTRN_LBL), TAG_END),
			LAYOUT_AddChild, NewObject(LAYOUT_GetClass(), NULL,
				TAG_DONE),
			CHILD_WeightedHeight, 100,
			LAYOUT_AddChild, NewObject(LAYOUT_GetClass(), NULL,
				LAYOUT_EvenSize, TRUE,
				LAYOUT_AddChild, NewObject(BUTTON_GetClass(), NULL,
					GA_ID, GID_OK,
					GA_RelVerify, TRUE,
					GA_Text, GetStr(MSG_SEARCHFILES_SEARCH_GAD),
					BUTTON_TextPadding, TRUE,
					GA_TabCycle, TRUE,
					TAG_END),
				CHILD_WeightedWidth, 1,
				LAYOUT_AddChild, NewObject(LABEL_GetClass(), NULL, LABEL_Text, "",
					TAG_END),
				CHILD_WeightedWidth, 100,
				LAYOUT_AddChild, NewObject(BUTTON_GetClass(), NULL,
					GA_ID, GID_CANCEL,
					GA_RelVerify, TRUE,
					GA_Text, GetStr(MSG_CANCEL_GAD),
					BUTTON_TextPadding, TRUE,
					GA_TabCycle, TRUE,
					TAG_END),
				CHILD_WeightedWidth, 1,
				TAG_DONE),
			CHILD_WeightedHeight, 0,
			TAG_DONE),
		TAG_DONE);

	InitRequester(&BlockingReq);
	Request(&BlockingReq, ads->window);
	SetWindowPointer(ads->window, WA_BusyPointer, TRUE, TAG_DONE);

	firstActivation = TRUE;
	searchFilesWindow = (struct Window *)DoMethod(searchFilesWindowObj, WM_OPEN, NULL);

	if (!searchFilesWindowObj)
		closeSearchFiles(ads);

	GetAttr(WINDOW_SigMask, searchFilesWindowObj, &sigmask);

	ads->ext->sigMask |= sigmask;
}

void closeSearchFiles(struct Codecraft *ads)
{
	if (searchFilesWindowObj)
	{
		ULONG sigmask;

		GetAttr(WINDOW_SigMask, searchFilesWindowObj, &sigmask);
		ads->ext->sigMask &= ~sigmask;
		DisposeObject(searchFilesWindowObj);
		searchFilesWindowObj  = NULL;
		searchFilesWindow = NULL;

		SetWindowPointer(ads->window, TAG_DONE);
		EndRequest(&BlockingReq, ads->window);
	}
}

void searchFilesEventHandler(struct Codecraft *ads)
{
	ULONG result;
	ULONG code;

	if (!searchFilesWindowObj)
		return;

	while (result = DoMethod(searchFilesWindowObj, WM_HANDLEINPUT, &code))
	{
		switch (result & WMHI_CLASSMASK)
		{
		case WMHI_ACTIVE:
			if (firstActivation)	
				ActivateLayoutGadget((struct Gadget *)mainlayout, searchFilesWindow, NULL, (ULONG)gadgets[GID_SEARCHSTRING]);
			firstActivation = FALSE;
			break;
			
		case WMHI_GADGETUP:
			switch(result & WMHI_GADGETMASK)
			{
				case GID_SEARCHPATH:		
					DoMethod(gadgets[GID_SEARCHPATH], GFILE_REQUEST, searchFilesWindow);
					break;

				case GID_SEARCHSTRING:
					// INTENTIONAL FALL THROUGH					
				case GID_OK:
				{
				    SetGadgetAttrs((struct Gadget *)mainlayout, searchFilesWindow, NULL,
				    	GA_Disabled, TRUE,
				    	TAG_END);
				    RefreshGList((struct Gadget *)mainlayout, searchFilesWindow, NULL, 1);

					if (! ads->searchFilesBrowser)
						createSearchFilesBrowser(ads);
					
					if (ads->searchFilesBrowser)
					{
						STRPTR str;
						
					    SetGadgetAttrs((struct Gadget *)ads->searchFilesBrowser, ads->window, NULL,
					    	LISTBROWSER_Labels, (ULONG) NULL,
					    	TAG_END);
						
						GetAttr(STRINGA_TextVal, gadgets[GID_SEARCHSTRING], (ULONG *)&str);
						Strncpy(searchText, str, sizeof(searchText));

						StartSearch(ads, str);
					}
				}
				/* intentional fall through */
				case GID_CANCEL:
					closeSearchFiles(ads);
					break;
			}
			break;
		}
	}
}

ULONG searchHandler(struct TagItem *taglist)
{
	struct Codecraft *ads = &myCodecraft;
	struct Node *node;
	ULONG relevent;
	struct TagItem *tstate, *ti; /* grab some temp variables off of the stack. */
	ti = taglist;
	tstate = ti;

	while (ti = NextTagItem(&tstate)) /* Step through all of the attribute/value */
	{
		switch (ti->ti_Tag)
		{
			case LISTBROWSER_SelectedNode:
				node = (struct Node *)ti->ti_Data;
				GetAttr(LISTBROWSER_RelEvent, ads->searchFilesBrowser, &relevent);
				if (relevent == LBRE_DOUBLECLICK)
				{
					ULONG *blockNum = 0;
					
					GetListBrowserNodeAttrs(node,
						LBNA_Column, 1,
						LBNCA_Integer, (ULONG *)&blockNum,
						TAG_DONE);
					
					if (node->ln_Name)
					{
						LONG len = strlen(node->ln_Name);
						struct RevealLocationMsg *rlm;
						rlm = (struct RevealLocationMsg *)AllocVec(sizeof(struct RevealLocationMsg) + len+1, MEMF_CLEAR);
						
						if (rlm)
						{
							strcpy(rlm->filename, node->ln_Name);
							rlm->blockNum = *blockNum;
							rlm->type = 0;
							PutMsg(ads->revealPort, (struct Message *)rlm);
						}
					}
				}
				break;
		}
	}
	return TRUE;
}

Object *icobject;

void createSearchFilesBrowser(struct Codecraft *ads)
{
	struct Node *node;
	
    NewList(&ads->searchFilesBrowserList);
    
	icobject = NewObject(ICCallbackClass, NULL,
			TAG_USER, searchHandler,
			TAG_DONE);

    ads->searchFilesBrowser = NewObject(LISTBROWSER_GetClass(), NULL,
    	GA_ID, GLOBALGID_SEARCHBROWSER,
    	GA_RelVerify, TRUE,
    	LISTBROWSER_ShowSelected, TRUE,
    	LISTBROWSER_MultiSelect, FALSE,
    	LISTBROWSER_Separators, FALSE,
    	LISTBROWSER_Hierarchical, TRUE,
    	LISTBROWSER_StayActive, TRUE,
    	ICA_TARGET, icobject,
    	TAG_DONE);

	node = AllocClickTabNode(
		TNA_Text, GetStr(MSG_UTILTAB_SEARCHRESULTS),
		TNA_CloseGadget, TRUE,
		TAG_END);
	
    SetGadgetAttrs((struct Gadget *)ads->utilityClickTab, ads->window, NULL,
    	CLICKTAB_Labels, ~0,
    	TAG_END);

	AddTail(&ads->utilityTabs, node);
    
    ads->SearchTab = node;
    
    SetGadgetAttrs((struct Gadget *)ads->utilityPageGroup, ads->window, NULL,
    	PAGE_Add, ads->searchFilesBrowser,
    	TAG_END);
    	
    SetGadgetAttrs((struct Gadget *)ads->utilityClickTab, ads->window, NULL,
    	CLICKTAB_Labels, &ads->utilityTabs,
    	TAG_END);
}

STRPTR prevPath;

void addSearchResult(struct Codecraft *ads, STRPTR path, STRPTR line, LONG blockNum)
{
	struct Node *node;
	
    SetGadgetAttrs((struct Gadget *)ads->searchFilesBrowser, ads->window, NULL,
    	LISTBROWSER_Labels, (ULONG) ~0,
    	TAG_END);

	if (ads->searchFilesBrowserList.lh_Head->ln_Succ == NULL)
	{
		SetGadgetAttrs(	(struct Gadget *) ads->utilityClickTab, ads->window, NULL,
			CLICKTAB_CurrentNode, ads->SearchTab,
			TAG_DONE);
	}
	
	if (Stricmp(path, prevPath) != 0)
	{
		node = AllocListBrowserNode(1,
			LBNA_Generation, 1,
			LBNA_Flags, LBFLG_HASCHILDREN|LBFLG_SHOWCHILDREN,
			LBNA_Column, 0,
			LBNCA_CopyText, TRUE,
			LBNCA_Text, path,
			TAG_DONE);

		AddTail(&ads->searchFilesBrowserList, node);

		GetListBrowserNodeAttrs(node,
			LBNCA_Text, (ULONG *)&prevPath,
			NULL
		);
	}

	node = AllocListBrowserNode(2,
		LBNA_Generation, 2,
		LBNA_Column, 0,
		LBNCA_CopyText, TRUE,
		LBNCA_Text, line,
		LBNA_Column, 1,
		LBNCA_CopyInteger, TRUE,
		LBNCA_Integer, &blockNum,		
		TAG_DONE);

	node->ln_Name = prevPath;
	
	AddTail(&ads->searchFilesBrowserList, node);
	
    SetGadgetAttrs((struct Gadget *)ads->searchFilesBrowser, ads->window, NULL,
    	LISTBROWSER_Labels, &ads->searchFilesBrowserList,
    	TAG_END);
}

LONG detectBinary(STRPTR buffer)
{
	while (*buffer)
	{
		if ((*buffer < 32) && (*buffer != 13) && (*buffer != 10))
			return TRUE;
		
		buffer++;
	}
	return FALSE;
}

LONG *generateKmpTable(STRPTR pattern)
{
	ULONG pattern_len = strlen(pattern);
    LONG i = 0, j = -1;
    LONG *table = (LONG *)AllocVec((pattern_len + 1) * sizeof(LONG), MEMF_ANY);
    
    if (! table)
	    return NULL;
	    
    table[0] = -1;
    
    while (i < pattern_len) {
        while (j >= 0 && pattern[i] != pattern[j]) {
            j = table[j];
        }
        i++;
        j++;
        table[i] = j;
    }
    
    return table;
}

void searchInFile(struct Codecraft *ads, STRPTR str, LONG *table, STRPTR path)
{
	BPTR file;
	TEXT buffer[1024];
	STRPTR lineBuffer = buffer + 7;
	LONG blockNumber = 0;
	LONG lineLen;
	ULONG stringLen = strlen(str);
	
	GetAttr(CHECKBOX_Checked, gadgets[GID_CASESENSITIVE], &matchCase);
	GetAttr(CHECKBOX_Checked, gadgets[GID_WHOLEWORD], &wholeWord);
	
	if (!matchCase) strlwr(str);
	
	file = Open(FilePart(path), MODE_OLDFILE);
	
	if (!file)
		return;
	
	while (FGets(file, lineBuffer, 1024-7))
	{
		LONG next_match_pos = 0;
		ULONG pos;
		
		blockNumber++;
		
		lineLen = strlen(lineBuffer);
		if (lineBuffer[lineLen-1]==10)
		{
			lineLen--;
			lineBuffer[lineLen] = 0;
		}
		if (lineLen >1000)
			break;

		if (blockNumber < 6 && detectBinary(lineBuffer))
			break;
			
		
		for (pos = 0; pos < lineLen; pos++)
		{
			while (next_match_pos >= 0 && !charCompare(str[next_match_pos],lineBuffer[pos],matchCase))
			{
				next_match_pos = table[next_match_pos];
			}

			next_match_pos++;

			if (next_match_pos == stringLen)
			{
				if (!wholeWord || isWordMatch(lineBuffer,pos,lineLen,stringLen))
				{
					LONG leadBlanks = 0;
					while (IsSpace(Locale, lineBuffer[leadBlanks]))
						leadBlanks++;
					SNPrintf(buffer, 1024, "%ld: %s", blockNumber, lineBuffer+leadBlanks);
					addSearchResult(ads, path, buffer, blockNumber-1);
					break;
				}
			}
		}    	
	}
	
	
	Close(file);
}

void searchRecurse(struct Codecraft *ads, STRPTR str, LONG *table, BPTR dirLock, STRPTR path, LONG pathLen, UBYTE *searchPattern)
{
	struct FileInfoBlock *fib;	
	
	fib = (struct FileInfoBlock *)AllocDosObject(DOS_FIB, TAG_DONE);
	
	Examine(dirLock, fib);
	
	if (pathLen)
	{
		path[pathLen] = '/';
		pathLen++;
	}
	
	while (ExNext(dirLock, fib))
	{
		if (fib->fib_FileName[0] == '.')
			continue;
			
		strcpy(path + pathLen, fib->fib_FileName);

		if (fib->fib_DirEntryType < 0)
		{
			BPTR oldDir;
				
			if (!searchPattern || MatchPatternNoCase(searchPattern,path))
			{
				oldDir = CurrentDir(dirLock);
				searchInFile(ads, str, table, path);
				CurrentDir(oldDir);
			}
		}
		else if (recurse)
		{
			BPTR subLock;
			BPTR oldDir;
			
			oldDir = CurrentDir(dirLock);
			subLock = Lock(fib->fib_FileName, SHARED_LOCK);
			CurrentDir(oldDir);
			
			if (subLock)
				searchRecurse(ads, str, table, subLock, path, pathLen+strlen(fib->fib_FileName),searchPattern);
			
			UnLock(subLock);
		}	
	}
	
	FreeDosObject(DOS_FIB, fib);
}

void StartSearch(struct Codecraft *ads, STRPTR str)
{
	TEXT path[2048];
	LONG *table;
	struct Node *node;
	STRPTR gadgetText;
	UBYTE *parsedPattern;
	ULONG bufsize;
	
	prevPath = "";
	path[0] = 0;


	// First clear out old results
    SetGadgetAttrs((struct Gadget *)ads->searchFilesBrowser, ads->window, NULL,
    	LISTBROWSER_Labels, NULL,
    	TAG_END);
	while (node = RemTail(&ads->searchFilesBrowserList))
		FreeListBrowserNode(node);
    SetGadgetAttrs((struct Gadget *)ads->searchFilesBrowser, ads->window, NULL,
    	LISTBROWSER_Labels, &ads->searchFilesBrowserList,
    	TAG_END);


	table = generateKmpTable(str);

	GetAttr(STRINGA_TextVal, gadgets[GID_PATTERN], (ULONG *)&gadgetText);
	strncpy(searchPattern,gadgetText,100);

	GetAttr(GETFILE_Drawer, gadgets[GID_SEARCHPATH], (ULONG *)&gadgetText);
	strncpy(path,gadgetText,2048);

	GetAttr(CHECKBOX_Checked, gadgets[GID_RECURSE], &recurse);

	NameFromLock(ads->projectDirLock, searchPath, MAXPATHLEN);
	if (!stricmp(searchPath,gadgetText))
	{
		strcpy(searchPath,"");
	}
	else
	{
		strncpy(searchPath,gadgetText,MAXPATHLEN);
	}
	
	if (isAllWhiteSpace(searchPattern))
	{
			parsedPattern = NULL;
	}
	else
	{
		bufsize = strlen(searchPattern)*2 + 2;
		parsedPattern = AllocVec(bufsize, MEMF_ANY);
		if (parsedPattern)
			ParsePatternNoCase(searchPattern, parsedPattern, bufsize);
	};
	

	if (table)
		searchRecurse(ads, str, table, ads->projectDirLock, path, 0, parsedPattern);
		
	FreeVec(parsedPattern);
	FreeVec(table);
}

void CloseSearch(struct Codecraft *ads)
{
	struct Node *node;

    SetGadgetAttrs((struct Gadget *)ads->utilityClickTab, ads->window, NULL,
    	CLICKTAB_Labels, ~0,
    	TAG_END);

	Remove(ads->SearchTab);
    
    SetGadgetAttrs((struct Gadget *)ads->utilityPageGroup, ads->window, NULL,
    	PAGE_Remove, ads->searchFilesBrowser,
    	TAG_END);
    	
    SetGadgetAttrs((struct Gadget *)ads->utilityClickTab, ads->window, NULL,
    	CLICKTAB_Labels, &ads->utilityTabs,
    	TAG_END);
		
	ads->searchFilesBrowser = NULL;

	FreeClickTabNode(ads->SearchTab);
	ads->SearchTab = NULL;

	DisposeObject(icobject);
	icobject = NULL;
	
	while (node = RemTail(&ads->searchFilesBrowserList))
		FreeListBrowserNode(node);
}
