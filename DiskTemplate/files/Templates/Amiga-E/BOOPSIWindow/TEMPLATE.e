OPT OSVERSION=47
  
  MODULE 'intuition/intuition','intuition/classes','intuition/classusr',
          'gadgets/layout','classes/window','window','layout'

PROC startUI()
  DEF intuiwin=NIL:PTR TO window
  DEF windowObject=NIL:PTR TO object
  DEF mainLayout=NIL:PTR TO object

   mainLayout := NewObjectA(Layout_GetClass(), NIL,[
         LAYOUT_ORIENTATION, LAYOUT_ORIENT_VERT,
         LAYOUT_DEFERLAYOUT, TRUE,
         LAYOUT_SPACEINNER, TRUE,
         LAYOUT_SPACEOUTER, TRUE,
         TAG_DONE])

   windowObject := NewObjectA(Window_GetClass(), NIL,[
         WINDOW_POSITION, WPOS_CENTERSCREEN,
         WA_ACTIVATE, TRUE,
         WA_TITLE, '%TEMPLATE%',
         WA_DRAGBAR, TRUE,
         WA_CLOSEGADGET, TRUE,
         WA_DEPTHGADGET, TRUE,
         WA_SIZEGADGET, TRUE,
         WA_INNERWIDTH, 300,
         WA_INNERHEIGHT, 150,
         WA_IDCMP, IDCMP_CLOSEWINDOW,
         WINDOW_LAYOUT, mainLayout,
         TAG_DONE])

   IF (windowObject=FALSE) THEN cleanexit(NIL)

   IF ((intuiwin := DoMethod(windowObject,WM_OPEN, NIL))=FALSE) THEN cleanexit(windowObject)

   processEvents(windowObject)

   DoMethod(windowObject,WM_CLOSE)
   cleanexit(NIL)
ENDPROC 0

PROC main()
   IF ((windowbase:=OpenLibrary('window.class', 47))=FALSE) THEN cleanexit(NIL)

   IF ((layoutbase:=OpenLibrary('gadgets/layout.gadget', 47))=FALSE) THEN cleanexit(NIL)

   startUI()   
ENDPROC

PROC processEvents(windowObject:PTR TO object)
   DEF windowsignal
   DEF receivedsignal
   DEF result
   DEF code
   DEF end = FALSE

   GetAttr(WINDOW_SIGMASK, windowObject, {windowsignal})

   WHILE (end=FALSE)
      receivedsignal := Wait(windowsignal)
      WHILE ((result := DoMethod(windowObject, WM_HANDLEINPUT, {code})) <> WMHI_LASTMSG)
         SELECT result AND WMHI_CLASSMASK
            CASE WMHI_CLOSEWINDOW
               end:=TRUE
         ENDSELECT
      ENDWHILE
   ENDWHILE
ENDPROC

PROC cleanexit(windowObject:PTR TO object)
  IF (windowObject) THEN DisposeObject(windowObject)

  CloseLibrary(windowbase)
  CloseLibrary(layoutbase)
ENDPROC

CHAR '$VER: %TEMPLATE% (dd.mm.yyyy)',0
