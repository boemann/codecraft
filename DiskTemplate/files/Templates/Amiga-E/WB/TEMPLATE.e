OPT OSVERSION=37

  MODULE 'dos/rdargs','dos/dos'

#define TEMPLATE       'FANCY/K,FILE/M'
#define OPT_FANCY 0
#define OPT_FILENAME 1
#define OPT_COUNT 2

PROC main()
  DEF rdargs=NIL:PTR TO rdargs
  DEF opts[OPT_COUNT]:ARRAY OF LONG
  DEF filenames:PTR TO LONG
  DEF i

	PrintF('%TEMPLATE%\n')
	FOR i:=0 TO OPT_COUNT-1 DO opts[i]:=NIL

	IF (rdargs := ReadArgs(TEMPLATE, opts, NIL))
		IF (opts[OPT_FANCY]) THEN
			 PrintF('you selected fancy: \s\n', opts[OPT_FANCY])

		IF (opts[OPT_FILENAME])
			filenames := opts[OPT_FILENAME]
			WHILE (filenames[])
				PrintF('on file: \s\n', filenames[]++)
			ENDWHILE
		ENDIF
	ENDIF
ENDPROC 0

CHAR '$VER: %TEMPLATE% (dd.mm.yyyy)',0
