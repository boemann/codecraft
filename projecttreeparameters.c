/* This file is part of Codecraft, and IDE running under AmigaOS 3.2.1 and later
 *
 * Copyright 2022 Camilla Boemann
 *
 * Codecraft is free software: you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2 as published by the Free Software Foundation.
 *
 * Codecrafr is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not,
 * see <https://www.gnu.org/licenses/>.
 */

/* includes */
#include <strings.h>

#include <exec/memory.h>
#include <intuition/intuition.h>
#include <intuition/gadgetclass.h>
#include <intuition/icclass.h>
#include <gadgets/layout.h>
#include <gadgets/button.h>
#include <gadgets/getfile.h>
#include <gadgets/string.h>
#include <gadgets/listbrowser.h>
#include <images/label.h>
#include <classes/window.h>
#include <classes/requester.h>
/* prototypes */

#include <clib/alib_protos.h>

#include <clib/macros.h>
#include <proto/intuition.h>
#include <proto/gadtools.h>
#include <proto/intuition.h>
#include <proto/graphics.h>
#include <proto/utility.h>
#include <proto/dos.h>
#include <proto/icon.h>
#include <proto/exec.h>
#include <proto/window.h>
#include <proto/layout.h>
#include <proto/label.h>
#include <proto/string.h>
#include <proto/listbrowser.h>
#include <proto/chooser.h>
#include <proto/radiobutton.h>
#include <proto/getfile.h>
#include <proto/requester.h>

#include "codecraft.h"
#include <tools/textedit/extension.h>

#define CATCOMP_NUMBERS;
#include "strings.h"
const UBYTE *GetStr(LONG stringNum);

static struct Requester BlockingReq;
static struct Window *projectParametersWindow = NULL;
static Object *projectParametersWindowObj = NULL;
struct Library *RequesterBase = NULL;

void closeProjectParameters(struct Codecraft *ads);
void onChangeConfig(struct Codecraft *ads, LONG profileIndex);

extern struct Library *LocaleBase;
extern struct Library *UtilityBase;
extern struct Catalog *Catalog;

static STRPTR radiolabels[] = {"Existing", "Empty", "Shell", "Workbench", "GadTools", 0};

struct List catList;
struct List fileList;
struct List cfgChooserList;

ULONG numFilepatterns=0;
struct Node *selectedFilePatternNode = NULL;
struct Configuration *curCfg;

struct List tempConfigList;
LONG indexChange;

#define MAXPATH 256

void addCategory(CONST_STRPTR text)
{
	struct Node *node = AllocListBrowserNode(
		1,
		LBNA_Column, 0,
		LBNCA_CopyText, TRUE,
		LBNCA_Text, text,
		TAG_DONE);
	AddTail(&catList, node);
}

struct Node *addFilePattern(STRPTR text)
{
	struct Node *node = AllocListBrowserNode(
		1,
		LBNA_Column, 0,
		LBNCA_CopyText, TRUE,
		LBNCA_Text, text,
		LBNCA_Editable, TRUE,
		LBNCA_MaxChars, 32,
		TAG_DONE);
	AddTail(&fileList, node);

	return node;
}

void buildConfigChooserList(struct Codecraft *ads)
{
	struct Configuration *cfg = (struct Configuration *)tempConfigList.lh_Head;
	
	NewList(&cfgChooserList);
	
	while (cfg->node.ln_Succ)
	{
		struct Node *node = AllocChooserNode(CNA_Text, cfg->name, TAG_DONE);
		
		if (node)
			AddTail(&cfgChooserList, node);
			
		cfg = (struct Configuration *)cfg->node.ln_Succ;
	}
}

void buildFilePatternList(STRPTR str)
{
	STRPTR start = str+1, end;
	LONG depth = 0;

	numFilepatterns = 0;
	while (*start == '(')
	{
		start++;
		end = start;
		depth++;
		while (depth && *end)
		{
			if (*end == '(')
				depth++;
			else if (*end == ')')
				depth--;
			end++;
		}
		end--;
		*end = 0;
		addFilePattern(start);
		numFilepatterns++;
		*end = ')';
		start = end + 1;
		if (*start)
			start++;
	}
}

enum gadids
{
  GID_OK = 1
 ,GID_CANCEL
 ,GID_CONFIGCHOOSER
 ,GID_DUPLICATECONFIG
 ,GID_RENAMECONFIG
 ,GID_DELETECONFIG
 ,GID_LIST
 ,GID_PROJDIR
 ,GID_SHOWLIST
 ,GID_NEWSHOWRULE
 ,GID_DELETESHOWRULE
 ,GID_BUILDDRAWER
 ,GID_BUILDCOMMAND
 ,GID_CLEANCOMMAND
 ,GID_DEBUGCOMMAND
 ,GID_DEBUGARGUMENTS
 ,GID_WORKDRAWER
 ,GID_STARTTYPE
 ,GID_UPTODATECOMMAND
 , MAXGADGETS
};

static Object *gadgets[MAXGADGETS];
static Object *layout;
static CONST_STRPTR startupTypes[3];

struct TagItem listToPageMap[] =
{
	{LISTBROWSER_Selected, PAGE_Current},
	{TAG_END, }
};

void BuildTempConfigList(struct Codecraft *ads)
{
	struct Configuration *cfg;
	
	NewList(&tempConfigList);
	indexChange = 0;
	
	cfg = (struct Configuration *)ads->project.cfgList.lh_Head;
	while (cfg->node.ln_Succ)
	{
		struct Configuration *newCfg;
		
		newCfg = (struct Configuration *)AllocVec(sizeof(struct Configuration), MEMF_ANY);
		
		if (newCfg)
		{
			CopyMem(cfg, newCfg, sizeof(struct Configuration));
			AddTail(&tempConfigList, newCfg);
		}
				
		cfg = (struct Configuration *)cfg->node.ln_Succ;
	}
}

void EmptyConfigList(struct List *list)
{
	struct Configuration *cfg;
	
	while (cfg = (struct Configuration *)RemTail(list))
	{
		FreeMem(cfg, sizeof(struct Configuration));
	}
}

void ApplyTempConfigList(struct Codecraft *ads)
{
	struct Configuration *cfg;
	LONG idx;
	
	EmptyConfigList(&ads->project.cfgList);
	
	ads->project.cfg = NULL;

	idx = ads->project.currentCfgIndex += indexChange;

	while (cfg = (struct Configuration *)RemHead(&tempConfigList))
	{
		AddTail(&ads->project.cfgList, cfg);

		if (idx-- == 0)
			ads->project.cfg = cfg;
	}
	
	if (ads->project.cfg == NULL)
	{
		ads->project.currentCfgIndex = 0;
		ads->project.cfg = (struct Configuration *)ads->project.cfgList.lh_TailPred;
	}
}

void editProjectTreeParameters(struct Codecraft *ads)
{
	ULONG sigmask;
	Object *genProperties;
	Object *buildProperties;
	Object *debuggingProperties;
	Object *propertiesArea;
	struct gpDomain gpd;
	struct RastPort rp;
	TEXT tmpbuf[MAXPATH];

	NewList(&fileList);
	
	BuildTempConfigList(ads);
	
	buildConfigChooserList(ads);

	NameFromLock(ads->projectDirLock, tmpbuf, MAXPATH);
	
	/* Now the gadget for the general properties */
	genProperties = NewObject(LAYOUT_GetClass(), NULL,
					LAYOUT_Orientation, LAYOUT_ORIENT_VERT,
					LAYOUT_SpaceInner, TRUE,
					LAYOUT_SpaceOuter, TRUE,
					LAYOUT_AddImage, NewObject(LABEL_GetClass(), NULL, LABEL_SoftStyle, FSF_BOLD, LABEL_Text, GetStr(MSG_PRJPARAM_GENERAL), TAG_END),
					LAYOUT_AddChild, gadgets[GID_PROJDIR] = NewObject(STRING_GetClass(), NULL,
						STRINGA_TextVal, tmpbuf,
						GA_ReadOnly, TRUE,
						TAG_DONE),
					CHILD_Label, NewObject(LABEL_GetClass(), NULL, LABEL_Text, GetStr(MSG_PRJPARAM_PRJDIR), TAG_END),
					CHILD_WeightedHeight, 1,
					LAYOUT_AddChild, gadgets[GID_SHOWLIST] = NewObject(LISTBROWSER_GetClass(), NULL,
						GA_ID, GID_SHOWLIST,
						GA_RelVerify, TRUE,
						LISTBROWSER_Labels, &fileList,
						LISTBROWSER_ShowSelected, TRUE,
						LISTBROWSER_MultiSelect, FALSE,
						LISTBROWSER_Editable, TRUE,
						TAG_DONE),
					CHILD_Label, NewObject(LABEL_GetClass(), NULL, LABEL_Text, GetStr(MSG_PRJPARAM_FILES), TAG_END),
					CHILD_WeightedHeight, 100,
					LAYOUT_AddChild, NewObject(LAYOUT_GetClass(), NULL,
						LAYOUT_AddChild, gadgets[GID_NEWSHOWRULE] = NewObject(NULL, "button.gadget",
							GA_ID, GID_NEWSHOWRULE,
							GA_RelVerify, TRUE,
							GA_Text, GetStr(MSG_NEW_GAD),
							GA_TabCycle, TRUE,
							TAG_END),
						CHILD_WeightedWidth, 0,
						LAYOUT_AddChild, gadgets[GID_DELETESHOWRULE] = NewObject(NULL, "button.gadget",
							GA_ID, GID_DELETESHOWRULE,
							GA_RelVerify, TRUE,
							GA_Text, GetStr(MSG_DELETE_GAD),
							GA_Disabled, TRUE,
							GA_TabCycle, TRUE,
							TAG_END),
						CHILD_WeightedWidth, 0,
						LAYOUT_AddChild, NewObject(LABEL_GetClass(), NULL, LABEL_Text, "",
							TAG_END),
						CHILD_WeightedWidth, 100,
						TAG_DONE),
					CHILD_Label, NewObject(LABEL_GetClass(), NULL, TAG_END),
					CHILD_WeightedHeight, 0,
					TAG_DONE);

	/* Now the gadget for the general properties */
	buildProperties = NewObject(LAYOUT_GetClass(), NULL,
					LAYOUT_Orientation, LAYOUT_ORIENT_VERT,
					LAYOUT_SpaceInner, TRUE,
					LAYOUT_SpaceOuter, TRUE,
					LAYOUT_AddImage, NewObject(LABEL_GetClass(), NULL, LABEL_SoftStyle, FSF_BOLD, LABEL_Text, GetStr(MSG_PRJPARAM_BUILD), TAG_END),
					LAYOUT_AddChild, gadgets[GID_BUILDDRAWER] = NewObject(GETFILE_GetClass(), NULL,
						GA_ID, GID_BUILDDRAWER,
						GA_RelVerify, TRUE,
						GA_TabCycle, TRUE,
						GETFILE_RejectIcons, TRUE,
						GETFILE_DrawersOnly, TRUE,
						GETFILE_TitleText, GetStr(MSG_PRJPARAM_SELBLDDRW),
						TAG_END),
					CHILD_Label, NewObject(LABEL_GetClass(), NULL, LABEL_Text, GetStr(MSG_PRJPARAM_BLDDIR), TAG_END),
					CHILD_WeightedHeight, 0,
					LAYOUT_AddChild, gadgets[GID_BUILDCOMMAND] = NewObject(STRING_GetClass(), NULL,
						GA_ID, GID_BUILDCOMMAND,
						GA_TabCycle, TRUE,
						TAG_END),
					CHILD_Label, NewObject(LABEL_GetClass(), NULL, LABEL_Text, GetStr(MSG_PRJPARAM_BLDCMD), TAG_END),
					CHILD_WeightedHeight, 0,
					LAYOUT_AddChild, gadgets[GID_CLEANCOMMAND] = NewObject(STRING_GetClass(), NULL,
						GA_ID, GID_CLEANCOMMAND,
						GA_TabCycle, TRUE,
						TAG_END),
					CHILD_Label, NewObject(LABEL_GetClass(), NULL, LABEL_Text, GetStr(MSG_PRJPARAM_CLEANCMD), TAG_END),
					CHILD_WeightedHeight, 0,
					LAYOUT_AddChild, gadgets[GID_UPTODATECOMMAND] = NewObject(STRING_GetClass(), NULL,
						GA_ID, GID_UPTODATECOMMAND,
						GA_TabCycle, TRUE,
						TAG_END),
					CHILD_Label, NewObject(LABEL_GetClass(), NULL, LABEL_Text, GetStr(MSG_PRJPARAM_UPTODATECMD), TAG_END),
					CHILD_WeightedHeight, 0,
					
					LAYOUT_AddChild, NewObject(LABEL_GetClass(), NULL, LABEL_Text, "",
						TAG_END),
					TAG_DONE);

	startupTypes[0] = GetStr(MSG_PRJPARAM_TYPECLI);
	startupTypes[1] = GetStr(MSG_PRJPARAM_TYPEWB);
	startupTypes[2] = NULL;
	
	/* Now the gadget for the general properties */
	debuggingProperties = NewObject(LAYOUT_GetClass(), NULL,
					LAYOUT_Orientation, LAYOUT_ORIENT_VERT,
					LAYOUT_SpaceInner, TRUE,
					LAYOUT_SpaceOuter, TRUE,
					LAYOUT_AddImage, NewObject(LABEL_GetClass(), NULL, LABEL_SoftStyle, FSF_BOLD, LABEL_Text, GetStr(MSG_PRJPARAM_DEBUGGING), TAG_END),
					LAYOUT_AddChild, gadgets[GID_DEBUGCOMMAND] = NewObject(STRING_GetClass(), NULL,
						GA_ID, GID_DEBUGCOMMAND,
						GA_TabCycle, TRUE,
						TAG_END),
					CHILD_Label, NewObject(LABEL_GetClass(), NULL, LABEL_Text, GetStr(MSG_PRJPARAM_CMD), TAG_END),
					CHILD_WeightedHeight, 0,
					LAYOUT_AddChild, gadgets[GID_DEBUGARGUMENTS] = NewObject(STRING_GetClass(), NULL,
						GA_ID, GID_DEBUGARGUMENTS,
						GA_TabCycle, TRUE,
						TAG_END),
					CHILD_Label, NewObject(LABEL_GetClass(), NULL, LABEL_Text, GetStr(MSG_PRJPARAM_CMDARGS), TAG_END),
					CHILD_WeightedHeight, 0,
					LAYOUT_AddChild, gadgets[GID_WORKDRAWER] = NewObject(GETFILE_GetClass(), NULL,
						GA_ID, GID_WORKDRAWER,
						GA_RelVerify, TRUE,
						GA_TabCycle, TRUE,
						GETFILE_RejectIcons, TRUE,
						GETFILE_DrawersOnly, TRUE,
						GETFILE_TitleText, GetStr(MSG_PRJPARAM_SELWRKDRW),
						TAG_END),
					CHILD_Label, NewObject(LABEL_GetClass(), NULL, LABEL_Text, GetStr(MSG_PRJPARAM_WRKDIR), TAG_END),
					CHILD_WeightedHeight, 0,
					LAYOUT_AddChild, gadgets[GID_STARTTYPE] = NewObject(RADIOBUTTON_GetClass(), NULL,
						GA_ID, GID_STARTTYPE,
						GA_RelVerify, TRUE,
						GA_TabCycle, TRUE,
						GA_Text, startupTypes,
						TAG_END),
					CHILD_Label, NewObject(LABEL_GetClass(), NULL, LABEL_Text, GetStr(MSG_PRJPARAM_STARTTYPE), TAG_END),
					CHILD_WeightedHeight, 0,
					LAYOUT_AddChild, NewObject(LABEL_GetClass(), NULL, LABEL_Text, "",
						TAG_END),
					TAG_DONE);

	propertiesArea = NewObject(PAGE_GetClass(), NULL,
						LAYOUT_SpaceInner, FALSE,
						LAYOUT_SpaceOuter, FALSE,
						PAGE_Add, genProperties,
						PAGE_Add, buildProperties,
						PAGE_Add, debuggingProperties,
					TAG_END);

	NewList(&catList);
	addCategory(GetStr(MSG_PRJPARAM_GENERAL));
	addCategory(GetStr(MSG_PRJPARAM_BUILD));
	addCategory(GetStr(MSG_PRJPARAM_DEBUGGING));

	gadgets[GID_LIST] = NewObject(LISTBROWSER_GetClass(), NULL,
				GA_ID, GID_LIST,
				GA_RelVerify, TRUE,
				ICA_MAP, listToPageMap,
				ICA_TARGET, propertiesArea,
				LISTBROWSER_ShowSelected, TRUE,
				LISTBROWSER_MultiSelect, FALSE,
				LISTBROWSER_Selected, 0,
				LISTBROWSER_Labels, &catList,
				TAG_DONE);

	/* Let's figure out how wide the list wants to be */
	InitRastPort(&rp);
	SetFont( &rp, ads->window->WScreen->RastPort.Font);
	gpd.MethodID = GM_DOMAIN;
	gpd.gpd_GInfo = NULL;
	gpd.gpd_RPort = &rp;
	gpd.gpd_Which = GDOMAIN_NOMINAL;
	gpd.gpd_Attrs = NULL;
	DoGadgetMethodA((struct Gadget *)gadgets[GID_LIST], ads->window, NULL, &gpd);

	projectParametersWindowObj = NewObject(WINDOW_GetClass(), NULL,
				WA_Activate, TRUE,
				WA_DragBar, TRUE,
				WA_DepthGadget, TRUE,
				WA_SizeGadget, TRUE,
				WA_Title, GetStr(MSG_PRJPARAM_TITLE),
				WA_Left, ads->window->LeftEdge + 50,
				WA_Top, ads->window->TopEdge + 30,
				WA_Width, 500,
				WA_Height, 180,
				WA_AutoAdjust, TRUE,
//				WINDOW_HintInfo, gb_HintInfo,
				WINDOW_GadgetHelp, TRUE,
				WA_IDCMP, IDCMP_CLOSEWINDOW|IDCMP_GADGETUP,
				WINDOW_Layout, layout = NewObject(LAYOUT_GetClass(), NULL,
					LAYOUT_DeferLayout, TRUE,
					LAYOUT_Orientation, LAYOUT_ORIENT_VERT,
					LAYOUT_SpaceInner, TRUE,
					LAYOUT_SpaceOuter, TRUE,
					
					LAYOUT_AddChild, NewObject(LAYOUT_GetClass(), NULL,
						LAYOUT_AddChild, gadgets[GID_CONFIGCHOOSER] = NewObject(CHOOSER_GetClass(), NULL, 
							GA_ID, GID_CONFIGCHOOSER,
							GA_RelVerify, TRUE,
							GA_TabCycle, TRUE,
							CHOOSER_PopUp, TRUE,
							CHOOSER_Selected, ads->project.currentCfgIndex,
							CHOOSER_Labels, &cfgChooserList,
							TAG_END),
						CHILD_Label, NewObject(LABEL_GetClass(), NULL, LABEL_Text, GetStr(MSG_PRJPARAM_WHICHCONFIG), TAG_END),
						CHILD_WeightedWidth, 100,
						LAYOUT_AddChild, gadgets[GID_DUPLICATECONFIG] = NewObject(NULL, "button.gadget",
							GA_ID, GID_DUPLICATECONFIG,
							GA_RelVerify, TRUE,
							GA_Text, GetStr(MSG_DUPLICATE_GAD),
							GA_TabCycle, TRUE,
							TAG_END),
						CHILD_WeightedWidth, 0,
						LAYOUT_AddChild, gadgets[GID_RENAMECONFIG] = NewObject(NULL, "button.gadget",
							GA_ID, GID_RENAMECONFIG,
							GA_RelVerify, TRUE,
							GA_Text, GetStr(MSG_RENAME_GAD),
							GA_TabCycle, TRUE,
							TAG_END),
						CHILD_WeightedWidth, 0,
						LAYOUT_AddChild, gadgets[GID_DELETECONFIG] = NewObject(NULL, "button.gadget",
							GA_ID, GID_DELETECONFIG,
							GA_RelVerify, TRUE,
							GA_Text, GetStr(MSG_DELETE_GAD),
							GA_TabCycle, TRUE,
							TAG_END),
						CHILD_WeightedWidth, 0,
						TAG_END),
					CHILD_WeightedHeight, 0,
					
					LAYOUT_AddChild, NewObject(LABEL_GetClass(), NULL, LABEL_Text, "",
						TAG_END),
					CHILD_WeightedHeight, 0,

					LAYOUT_AddChild, NewObject(LAYOUT_GetClass(), NULL,
						LAYOUT_AddChild, gadgets[GID_LIST],
						CHILD_WeightedWidth, 0,
						CHILD_MinWidth, gpd.gpd_Domain.Width,
						CHILD_MaxWidth, gpd.gpd_Domain.Width,
						LAYOUT_AddChild, propertiesArea,
						CHILD_WeightedWidth, 100,
						TAG_DONE),
					CHILD_WeightedHeight, 100,

					LAYOUT_AddChild, NewObject(LAYOUT_GetClass(), NULL,
						LAYOUT_EvenSize, TRUE,
						LAYOUT_AddChild, NewObject(NULL, "button.gadget",
							GA_ID, GID_OK,
							GA_RelVerify, TRUE,
							GA_Text, GetStr(MSG_OK_GAD),
							BUTTON_TextPadding, TRUE,
							GA_TabCycle, TRUE,
							TAG_END),
						CHILD_WeightedWidth, 1,
						LAYOUT_AddChild, NewObject(LABEL_GetClass(), NULL, LABEL_Text, "",
							TAG_END),
						CHILD_WeightedWidth, 100,
						LAYOUT_AddChild, NewObject(NULL, "button.gadget",
							GA_ID, GID_CANCEL,
							GA_RelVerify, TRUE,
							GA_Text, GetStr(MSG_CANCEL_GAD),
							BUTTON_TextPadding, TRUE,
							GA_TabCycle, TRUE,
							TAG_END),
						CHILD_WeightedWidth, 1,
						TAG_DONE),
					CHILD_WeightedHeight, 0,
					TAG_DONE),
				TAG_DONE);


	curCfg = NULL;
	onChangeConfig(ads, ads->project.currentCfgIndex);

	InitRequester(&BlockingReq);
	Request(&BlockingReq, ads->window);
	SetWindowPointer(ads->window, WA_BusyPointer, TRUE, TAG_DONE);

	projectParametersWindow = (struct Window *)DoMethod(projectParametersWindowObj, WM_OPEN, NULL);

	if (!projectParametersWindow)
		closeProjectParameters(ads);

	GetAttr(WINDOW_SigMask, projectParametersWindowObj, &sigmask);

	ads->ext->sigMask |= sigmask;
}

void FreeChooserLabels(struct List *list)
{
	struct Node *node;
	struct Node *nextnode;

	if (list)
	{
		node = list->lh_Head;

		while(nextnode = node->ln_Succ)
		{
			FreeChooserNode(node);
			node = nextnode;
		}
	}
}

void storeGadsToCurCfg(void)
{
	STRPTR s;
	ULONG type;
	struct Node *node;

	GetAttr(GETFILE_Drawer, gadgets[GID_BUILDDRAWER], (ULONG *)&s);
	Strncpy(curCfg->buildDir, s, sizeof(curCfg->buildDir));

	GetAttr(STRINGA_TextVal, gadgets[GID_BUILDCOMMAND], (ULONG *)&s);
	Strncpy(curCfg->buildCmd, s, sizeof(curCfg->buildCmd));

	GetAttr(STRINGA_TextVal, gadgets[GID_CLEANCOMMAND], (ULONG *)&s);
	Strncpy(curCfg->cleanCmd, s, sizeof(curCfg->cleanCmd));

	GetAttr(STRINGA_TextVal, gadgets[GID_DEBUGCOMMAND], (ULONG *)&s);
	Strncpy(curCfg->exeCmd, s, sizeof(curCfg->exeCmd));

	GetAttr(STRINGA_TextVal, gadgets[GID_DEBUGARGUMENTS], (ULONG *)&s);
	Strncpy(curCfg->arguments, s, sizeof(curCfg->arguments));
	Strncat(curCfg->arguments, "\n", sizeof(curCfg->arguments));

	GetAttr(STRINGA_TextVal, gadgets[GID_UPTODATECOMMAND], (ULONG *)&s);
	Strncpy(curCfg->testUpToDateCmd, s, sizeof(curCfg->testUpToDateCmd));

	GetAttr(GETFILE_Drawer, gadgets[GID_WORKDRAWER], (ULONG *)&s);
	Strncpy(curCfg->workDir, s, sizeof(curCfg->workDir));

	GetAttr(RADIOBUTTON_Selected, gadgets[GID_STARTTYPE], &type);
	if (type == 1)
		curCfg->flags |= CFG_FLAG_WB;
	else
		curCfg->flags &= ~CFG_FLAG_WB;

	curCfg->includePattern[0] = '(';
	curCfg->includePattern[1] = 0;
	for (node = fileList.lh_Head; node->ln_Succ; node = node->ln_Succ)
	{
		GetListBrowserNodeAttrs(node,
			LBNCA_Text, &s,
			TAG_DONE);
		Strncat(curCfg->includePattern, "(", sizeof(curCfg->includePattern));
		Strncat(curCfg->includePattern, s, sizeof(curCfg->includePattern));
		Strncat(curCfg->includePattern, ")|", sizeof(curCfg->includePattern));
	}
	if (curCfg->includePattern[1])
		curCfg->includePattern[strlen(curCfg->includePattern)-1] = 0;
	Strncat(curCfg->includePattern, ")", sizeof(curCfg->includePattern));
}

void onChangeConfig(struct Codecraft *ads, LONG profileIndex)
{
	TEXT tmpbuf[MAXPATH];
	
	if (curCfg)
		storeGadsToCurCfg();
	
	curCfg = (struct Configuration *)tempConfigList.lh_Head;
	while (profileIndex && curCfg->node.ln_Succ)
	{
		profileIndex--;
		curCfg = (struct Configuration *)curCfg->node.ln_Succ;
	}

	SetGadgetAttrs((struct Gadget *) gadgets[GID_DELETECONFIG], projectParametersWindow, NULL,
		GA_Disabled, (curCfg==(struct Configuration *)tempConfigList.lh_TailPred) && (curCfg==(struct Configuration *)tempConfigList.lh_Head),
	TAG_DONE);

	SetGadgetAttrs((struct Gadget *) gadgets[GID_BUILDDRAWER], projectParametersWindow, NULL,
		GETFILE_Drawer, curCfg->buildDir,
	TAG_DONE);
	SetGadgetAttrs((struct Gadget *) gadgets[GID_BUILDCOMMAND], projectParametersWindow, NULL,
		STRINGA_TextVal, curCfg->buildCmd,
	TAG_DONE);
	SetGadgetAttrs((struct Gadget *) gadgets[GID_CLEANCOMMAND], projectParametersWindow, NULL,
		STRINGA_TextVal, curCfg->cleanCmd,
	TAG_DONE);
	SetGadgetAttrs((struct Gadget *) gadgets[GID_UPTODATECOMMAND], projectParametersWindow, NULL,
		STRINGA_TextVal, curCfg->testUpToDateCmd,
	TAG_DONE);

	SetGadgetAttrs((struct Gadget *) gadgets[GID_DEBUGCOMMAND], projectParametersWindow, NULL,
		STRINGA_TextVal, curCfg->exeCmd,
	TAG_DONE);

	Strncpy(tmpbuf, curCfg->arguments, MAXPATH);
	if (strlen(tmpbuf))
		tmpbuf[strlen(tmpbuf)-1] = 0;			
	SetGadgetAttrs((struct Gadget *) gadgets[GID_DEBUGARGUMENTS], projectParametersWindow, NULL,
		STRINGA_TextVal, tmpbuf,
	TAG_DONE);
	SetGadgetAttrs((struct Gadget *) gadgets[GID_WORKDRAWER], projectParametersWindow, NULL,
		GETFILE_Drawer, curCfg->workDir,
	TAG_DONE);
	SetGadgetAttrs((struct Gadget *) gadgets[GID_STARTTYPE], projectParametersWindow, NULL,
		RADIOBUTTON_Selected, curCfg->flags & CFG_FLAG_WB ? 1: 0,
	TAG_DONE);
	
	SetGadgetAttrs((struct Gadget *) gadgets[GID_SHOWLIST], projectParametersWindow, NULL,
		LISTBROWSER_Labels, NULL,
		TAG_DONE);
	FreeListBrowserList(&fileList);
	buildFilePatternList(curCfg->includePattern);
	selectedFilePatternNode = NULL;
	SetGadgetAttrs((struct Gadget *) gadgets[GID_SHOWLIST], projectParametersWindow, NULL,
		LISTBROWSER_Labels, (ULONG) &fileList,
		TAG_DONE);
}


void closeProjectParameters(struct Codecraft *ads)
{
	if (projectParametersWindowObj)
	{
		ULONG sigmask;

		GetAttr(WINDOW_SigMask, projectParametersWindowObj, &sigmask);
		ads->ext->sigMask &= ~sigmask;
		DisposeObject(projectParametersWindowObj);
		projectParametersWindowObj  = NULL;
		projectParametersWindow = NULL;

		FreeListBrowserList(&catList);
		FreeListBrowserList(&fileList);
		FreeChooserLabels(&cfgChooserList);
		SetWindowPointer(ads->window, TAG_DONE);
		EndRequest(&BlockingReq, ads->window);
	}
}

void makepathrelative(struct Codecraft *ads, Object *strGad)
{
	TEXT tmpbuf[MAXPATH];
	STRPTR chosenDir;
	ULONG prjDirLen;

	NameFromLock(ads->projectDirLock, tmpbuf, MAXPATH);
	GetAttr(STRINGA_TextVal, strGad, (ULONG *)&chosenDir);
	prjDirLen = strlen(tmpbuf);
	if (Strnicmp(tmpbuf, chosenDir, prjDirLen) == 0)
	{
//		if (*(chosenDir + prjDirLen) != '/')
//			return;
		SNPrintf(tmpbuf, MAXPATH, chosenDir + prjDirLen + 1);
		SetGadgetAttrs(	(struct Gadget *) strGad, projectParametersWindow, NULL,
			STRINGA_TextVal, tmpbuf,
			TAG_DONE);
	}
}

void projectParametersEventHandler(struct Codecraft *ads)
{
	ULONG result;
	ULONG code;
	ULONG selCfgIndex, lastnode;
	BPTR olddir;
	Object *req;

	if (!projectParametersWindowObj)
		return;

	while (result = DoMethod(projectParametersWindowObj, WM_HANDLEINPUT, &code))
	{
		switch (result & WMHI_CLASSMASK)
		{
		case WMHI_CLOSEWINDOW:
			closeProjectParameters(ads);
			break;


		case WMHI_GADGETUP:
			switch(result & WMHI_GADGETMASK)
			{
				case GID_OK:
					storeGadsToCurCfg();					
					saveProjectTree(ads);

					ApplyTempConfigList(ads);

					updateConfigurationMenu(ads);
					fillGui(ads); // filtered files might have changed

				/* intentional fall through */
				case GID_CANCEL:
					EmptyConfigList(&tempConfigList);
					closeProjectParameters(ads);
					break;

				case GID_CONFIGCHOOSER:
					onChangeConfig(ads, code >> 16);
					break;

				case GID_RENAMECONFIG:
					req = NewObject(REQUESTER_GetClass(), NULL, 
						REQ_Type, REQTYPE_STRING,
						REQ_TitleText, GetStr(MSG_RENAME_CONFIG),
						REQ_GadgetText, GetStr(MSG_OKCANCEL_GADS),
						REQS_Buffer, curCfg->name,
						REQS_MaxChars, sizeof (curCfg->name)-1, 
						TAG_DONE);

					if (req)
					{
						if (DoMethod(req, RM_OPENREQ, NULL, projectParametersWindow, NULL))
						{
							SetGadgetAttrs((struct Gadget *) gadgets[GID_CONFIGCHOOSER], projectParametersWindow, NULL,
								CHOOSER_Labels, &cfgChooserList,
								TAG_END);
						}
						DisposeObject(req);
					}

					break;
				
				case GID_DELETECONFIG:
					GetAttr(CHOOSER_Selected, (struct Gadget *) gadgets[GID_CONFIGCHOOSER], &selCfgIndex);
	
					lastnode = curCfg==(struct Configuration *)tempConfigList.lh_TailPred;
					Remove(curCfg);
					FreeVec(curCfg);
					curCfg = NULL;
					FreeChooserLabels(&cfgChooserList);
					buildConfigChooserList(ads);

					if (selCfgIndex < ads->project.currentCfgIndex + indexChange)
						indexChange--;
					else if (selCfgIndex == ads->project.currentCfgIndex + indexChange)
					{
						if (! selCfgIndex)
							indexChange = 0;
					}
						
					if (!lastnode)
					{
						//this is not the last entry we are removing
						//so refresh with the same index
							SetGadgetAttrs((struct Gadget *) gadgets[GID_CONFIGCHOOSER], projectParametersWindow, NULL,
								CHOOSER_Labels, &cfgChooserList,
								TAG_END);
						onChangeConfig(ads, selCfgIndex);
					}
					else
					{
						//this is the last entry we are removing
						//so move to the previous entry
							SetGadgetAttrs((struct Gadget *) gadgets[GID_CONFIGCHOOSER], projectParametersWindow, NULL,
								CHOOSER_Labels, &cfgChooserList,
								CHOOSER_Selected, selCfgIndex-1,
								TAG_END);
						onChangeConfig(ads, selCfgIndex-1);
					}
					break;

				case GID_DUPLICATECONFIG:
					{
						struct Configuration *cfg;
						cfg = (struct Configuration *)AllocVec(sizeof(struct Configuration), MEMF_ANY);
						GetAttr(CHOOSER_Selected, (struct Gadget *) gadgets[GID_CONFIGCHOOSER], &selCfgIndex);
						
						if (cfg)
						{
							CopyMem(curCfg, cfg, sizeof(struct Configuration));
							Strncpy(cfg->name, "Build", 128);

							Insert(&tempConfigList, (struct Node *)cfg, (struct Node *)curCfg);

							if (selCfgIndex < ads->project.currentCfgIndex + indexChange)
								indexChange++;
								
							FreeChooserLabels(&cfgChooserList);
							buildConfigChooserList(ads);
							SetGadgetAttrs((struct Gadget *) gadgets[GID_CONFIGCHOOSER], projectParametersWindow, NULL,
								CHOOSER_Labels, &cfgChooserList,
								CHOOSER_Selected, selCfgIndex+1,
								TAG_END);
							onChangeConfig(ads, selCfgIndex+1);
						}
					}
					break;

				case GID_SHOWLIST:
					GetAttr(LISTBROWSER_SelectedNode, gadgets[GID_SHOWLIST], (ULONG *)&selectedFilePatternNode);
					SetGadgetAttrs((struct Gadget *) gadgets[GID_DELETESHOWRULE], projectParametersWindow, NULL,
						GA_Disabled, !selectedFilePatternNode,
						TAG_DONE);
					break;

				case GID_NEWSHOWRULE:
					SetGadgetAttrs((struct Gadget *) gadgets[GID_SHOWLIST], projectParametersWindow, NULL,
						LISTBROWSER_Labels, ~0,
						TAG_DONE);

					selectedFilePatternNode = addFilePattern("");
					SetGadgetAttrs((struct Gadget *) gadgets[GID_SHOWLIST], projectParametersWindow, NULL,
						LISTBROWSER_Labels, (ULONG) &fileList,
						LISTBROWSER_EditNode, numFilepatterns++,
						LISTBROWSER_EditColumn, 0,
						TAG_DONE);
					ActivateLayoutGadget((struct Gadget *)layout, projectParametersWindow, NULL, (ULONG)gadgets[GID_SHOWLIST]);
					break;

				case GID_DELETESHOWRULE:
					SetGadgetAttrs((struct Gadget *) gadgets[GID_SHOWLIST], projectParametersWindow, NULL,
						LISTBROWSER_Labels, ~0,
						TAG_DONE);

					Remove(selectedFilePatternNode);
					selectedFilePatternNode = NULL;
					numFilepatterns--,
					SetGadgetAttrs((struct Gadget *) gadgets[GID_SHOWLIST], projectParametersWindow, NULL,
						LISTBROWSER_Labels, (ULONG) &fileList,
						TAG_DONE);
					SetGadgetAttrs((struct Gadget *) gadgets[GID_DELETESHOWRULE], projectParametersWindow, NULL,
						GA_Disabled, TRUE,
						TAG_DONE);
					break;
				
				case GID_BUILDDRAWER:
					olddir = CurrentDir(ads->projectDirLock);
					DoMethod(gadgets[GID_BUILDDRAWER], GFILE_REQUEST,
							 projectParametersWindow, NULL);
					CurrentDir(olddir);
					makepathrelative(ads, gadgets[GID_BUILDDRAWER]);
					break;
					
				case GID_WORKDRAWER:
					olddir = CurrentDir(ads->projectDirLock);
					DoMethod(gadgets[GID_WORKDRAWER], GFILE_REQUEST,
							 projectParametersWindow, NULL);
					CurrentDir(olddir);
					break;
			}
			break;
		}
	}
}

