/* This file is part of Codecraft, and IDE running under AmigaOS 3.2.1 and later
 *
 * Copyright 2022 Camilla Boemann
 *
 * Codecraft is free software: you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2 as published by the Free Software Foundation.
 *
 * Codecraft is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not,
 * see <https://www.gnu.org/licenses/>.
 */

/* includes */
#include <strings.h>

#include <exec/memory.h>
#include <intuition/intuition.h>
#include <intuition/gadgetclass.h>

/* prototypes */

#include <clib/alib_protos.h>

#include <clib/macros.h>
#include <proto/intuition.h>
#include <proto/gadtools.h>
#include <proto/intuition.h>
#include <proto/graphics.h>
#include <proto/dos.h>
#include <proto/icon.h>
#include <proto/exec.h>
#include <proto/utility.h>
#include <proto/window.h>
#include <proto/layout.h>
#include <proto/button.h>
#include <proto/label.h>
#include <proto/string.h>
#include <proto/listbrowser.h>
#include <proto/radiobutton.h>
#include <proto/getfile.h>

#include <ctype.h>

#define CATCOMP_NUMBERS;
#include "strings.h"
UBYTE *GetStr(LONG stringNum);

#include "codecraft.h"
#include "debugger.h"

#include <tools/textedit/extension.h>

#define TEMPLATEDESCFILENAME "TEMPLATE.description"
static struct Requester BlockingReq;
struct Window *projectWizardWindow = NULL;
Object *projectWizardWindowObj = NULL;
Object *drawerGadget;
Object *nameGadget;
void closeProjectWizard(struct Codecraft *ads);

void closeProjectTree(struct Codecraft *ads);
void fillGui(struct Codecraft *ads);

extern struct Library *LocaleBase;
extern struct Library *UtilityBase;
extern struct Catalog *Catalog;

STRPTR radiolabels[4];
struct List templatesList;
struct Gadget *templateBrowser;
struct Gadget *radiobutton;

struct Node *addTemplate(int gen, STRPTR text)
{
	struct Node *node = AllocListBrowserNode(
		1,
		LBNA_Generation, gen,
		LBNA_Flags, gen==2 ? LBFLG_HIDDEN : LBFLG_HASCHILDREN,
		LBNA_Column, 0,
		LBNCA_CopyText, TRUE,
		LBNCA_WordWrap, TRUE,
		LBNCA_Text, text,
		TAG_DONE);
	AddTail(&templatesList, node);

	return node;
}

static const int bufsize = 1024;
static TEXT templateTextBuf[256] = "";

// returns pointer to last node inserted or NULL if nothing was inserted
// Algorithm inserts into list depth first, so the parents are
// inserted above their children on the way back up
static struct Node *fill(UWORD generation, BPTR dirLock, struct Node *latestDirNode)
{
    BOOL more;
    struct Node *doFirst = NULL;
    struct Node *lastTemplateNode = NULL;
    BPTR oldLock;
    BPTR file;
    struct ExAllData *buffer = (struct ExAllData *)AllocMem(bufsize, MEMF_CLEAR);
    struct ExAllControl *eac;
	UBYTE namebuf[256];
	

    if (!buffer)
        return NULL;

    eac = (struct ExAllControl *)AllocDosObject(DOS_EXALLCONTROL, NULL);

    if (!eac)
    {
        FreeMem(buffer, bufsize);
        return NULL;
    }

    eac->eac_LastKey = 0;

    oldLock = CurrentDir(dirLock);

	if (file = Open(TEMPLATEDESCFILENAME, MODE_OLDFILE))
	{
		doFirst = latestDirNode;

		templateTextBuf[Read(file, templateTextBuf,
					 sizeof(templateTextBuf)-1)] = 0;
		Close(file);
	}
	else
	    do {
	        struct ExAllData *ead;
	        int err;
	        more = ExAll(dirLock, buffer, bufsize, ED_DATE, eac);
	        err = IoErr();
	        if ((!more) && (err != ERROR_NO_MORE_ENTRIES)) {
	            break;
	        }
	        if (eac->eac_Entries == 0) {
	            /* ExAll failed normally with no entries */
	            continue;        /* ("more" is *usually* zero) */
	        }
	        
	        ead = buffer;
	        do {
	            BOOL isDrawer = (ead->ed_Type >= 0 && ead->ed_Type != ST_SOFTLINK);
	            if (isDrawer)
	            {
		            if (ead->ed_Name[0] != '.')
					{
		                BPTR subLock = Lock(ead->ed_Name, SHARED_LOCK);
		                struct Node *newPre = fill(generation+1, subLock, latestDirNode);
		
		                if (newPre)
		                {
		                    if (!doFirst)
		                        doFirst = latestDirNode;
		                    latestDirNode = newPre;
		                }
		
		                UnLock(subLock);
					}
				}
	
	            ead = ead->ed_Next;
	        } while (ead);
	
	    } while (more);

    if (doFirst && generation > 0)
    {
		struct Node *drawernode;
		STRPTR text;
		
		NameFromLock(dirLock, namebuf, sizeof(namebuf));
	
		if (templateTextBuf[0])
			text = templateTextBuf;
		else
			text = FilePart(namebuf);
		
		drawernode = AllocListBrowserNode(
				2,
				LBNA_Generation, generation,
				LBNA_Flags, templateTextBuf[0] ? 0 : LBFLG_HASCHILDREN|LBFLG_SHOWCHILDREN,
				LBNA_Column, 0,
				LBNCA_CopyText, TRUE,
				LBNCA_WordWrap, TRUE,
				LBNCA_Text, text,
				LBNA_Column, 1,
				LBNCA_CopyText, TRUE,
				LBNCA_Text, namebuf,
                TAG_DONE
        );

		templateTextBuf[0] = 0;
		
        if(drawernode)
            Insert(&templatesList, drawernode, doFirst);
    }

    CurrentDir(oldLock);
    FreeDosObject(DOS_EXALLCONTROL, eac);
    FreeMem(buffer, bufsize);

    return doFirst ? (lastTemplateNode ? lastTemplateNode : latestDirNode) : NULL;
}

static BPTR createDirRecursively(STRPTR name)
{
	STRPTR namestart;
	BPTR dirLock;
	struct Window *w;
	struct Process *pr = (struct Process *)FindTask(NULL);
	
	// let's first see if we can short track
	w = pr->pr_WindowPtr;
	pr->pr_WindowPtr = (APTR)-1;
	dirLock = Lock(name, SHARED_LOCK);
	pr->pr_WindowPtr = w;
	
	if (dirLock)
		return dirLock;
	
	// Okay so either the disk is missing or some dirs are not
	// created
	namestart = FilePart(name);
	if (namestart != name)
	{
		TEXT c;
		STRPTR cutPos = PathPart(name);
		
		c = *cutPos;
		*cutPos = 0;
		
		dirLock = createDirRecursively(name);

		*cutPos = c;
		
		if (dirLock)
		{
			BPTR subDirLock = NULL;
			BPTR oldLock = CurrentDir(dirLock);

			// let's try one more time in case user inserted
			// the disk and the dir is now available
			w = pr->pr_WindowPtr;
			pr->pr_WindowPtr = (APTR)-1;
			subDirLock = Lock(namestart, SHARED_LOCK);
			pr->pr_WindowPtr = w;
			
			if (!subDirLock)
			{
				subDirLock = CreateDir(namestart);
				if (subDirLock)
				{
					UnLock(subDirLock);
					subDirLock = Lock(namestart, SHARED_LOCK);
				}
			}
										
			CurrentDir(oldLock);
			UnLock(dirLock);
			
			return subDirLock;
		}
	}
	else
	{
		// here we are either at:
		// a) disk that isn't inserted
		// b) relative path that doesn't exist yet
		int len = strlen(name);
		
		if (len)
		{
			if (name[len-1] == ':')
				dirLock = Lock(name, SHARED_LOCK); // ask insert
			else
			{
				dirLock = CreateDir(namestart); // relative
			
				if (dirLock)
				{
					UnLock(dirLock);
					dirLock = Lock(namestart, SHARED_LOCK);
				}
			}
		}
	}
	

	return dirLock;
}

void newProjectTree(struct Codecraft *ads)
{
	ULONG sigmask;
	BPTR templatesLock;
	TEXT drawerPath[MAXPATHLEN];

	if (ads->debugger->debuggerMode != NOTACTIVE)
	{
		askUser(ads->window, MSG_OK_GAD, MSG_DBGRUNS_CANTDO, NULL);
		return;
	}

	radiolabels[0] = GetStr(MSG_NEWTREE_FROM_EXISTING);
	radiolabels[1] = GetStr(MSG_NEWTREE_FROM_SCRATCH);
	radiolabels[2] = GetStr(MSG_NEWTREE_FROM_TEMPLATE);
	radiolabels[3] = 0;
	
	NewList(&templatesList);
	templatesLock = Lock("PROGDIR:usertemplates", SHARED_LOCK);
	if (templatesLock)
		fill(0, templatesLock, templatesList.lh_TailPred);
	UnLock(templatesLock);
	templatesLock = Lock("PROGDIR:templates", SHARED_LOCK);
	if (templatesLock)
		fill(0, templatesLock, templatesList.lh_TailPred);
	UnLock(templatesLock);

	if (ads->projectDirLock)
		NameFromLock(ads->projectDirLock, drawerPath, sizeof(drawerPath));
	else
	{
		struct Process *pr = (struct Process *)FindTask(NULL);
		NameFromLock(pr->pr_CurrentDir, drawerPath, sizeof(drawerPath));
	}	
	projectWizardWindowObj = NewObject(WINDOW_GetClass(), NULL,
				WA_Activate, TRUE,
				WA_DragBar, TRUE,
				WA_SizeGadget, TRUE,
				WA_DepthGadget, TRUE,
				WA_Title, GetStr(MSG_NEWTREE_TITLE),
				WA_Left, ads->window->LeftEdge + 50,
				WA_Top, ads->window->TopEdge + 30,
				WA_Width, 320,
				WA_Height, 200,
				WA_AutoAdjust, TRUE,
//				WINDOW_HintInfo, gb_HintInfo,
				WINDOW_GadgetHelp, TRUE,
				WA_IDCMP, IDCMP_CLOSEWINDOW|IDCMP_GADGETUP,
				WINDOW_Layout, NewObject(LAYOUT_GetClass(), NULL,
					LAYOUT_DeferLayout, TRUE,
					LAYOUT_Orientation, LAYOUT_ORIENT_VERT,
					LAYOUT_SpaceInner, TRUE,
					LAYOUT_SpaceOuter, TRUE,
					LAYOUT_LabelWidth, 70,
					LAYOUT_AddChild, drawerGadget = NewObject(GETFILE_GetClass(), NULL,
						GA_ID, 1,
						GA_RelVerify, TRUE,
						GA_TabCycle, TRUE,
						GETFILE_RejectIcons, TRUE,
						GETFILE_DrawersOnly, TRUE,
						GETFILE_TitleText, GetStr(MSG_NEWTREE_SELECTDRAWER),
						GETFILE_Drawer, drawerPath,
						TAG_END),
					CHILD_Label, NewObject(LABEL_GetClass(), NULL, LABEL_Text, GetStr(MSG_NEWTREE_DRAWER), TAG_END),
					CHILD_WeightedHeight, 0,
					LAYOUT_AddChild, nameGadget = NewObject(STRING_GetClass(), NULL,
						GA_ID, 2,
						GA_TabCycle, TRUE,
						TAG_END),
					CHILD_Label, NewObject(LABEL_GetClass(), NULL, LABEL_Text, GetStr(MSG_NEWTREE_NAME), TAG_END),
					CHILD_WeightedHeight, 0,
					LAYOUT_AddChild, radiobutton = NewObject( RADIOBUTTON_GetClass(), NULL,
						GA_ID, 3,
						GA_RelVerify, TRUE,
						GA_Text, radiolabels,
						GA_TabCycle, TRUE,
						TAG_END),
					CHILD_WeightedWidth, 1,
					CHILD_WeightedHeight, 0,
					LAYOUT_AddChild, templateBrowser = NewObject(LISTBROWSER_GetClass(), NULL,
						GA_ID, 100,
						GA_RelVerify, TRUE,
						GA_Disabled, TRUE,
						LISTBROWSER_Labels, &templatesList,
						LISTBROWSER_ShowSelected, TRUE,
						LISTBROWSER_Striping, TRUE,
						LISTBROWSER_WrapText, TRUE,
						LISTBROWSER_Hierarchical, TRUE,
						TAG_DONE),
					CHILD_WeightedHeight, 100,
					LAYOUT_AddChild, NewObject(LABEL_GetClass(), NULL, LABEL_Text, "",
						TAG_END),
					CHILD_WeightedHeight, 0,
					LAYOUT_AddChild, NewObject(LAYOUT_GetClass(), NULL,
						LAYOUT_EvenSize, TRUE,
						LAYOUT_AddChild, NewObject( NULL, "button.gadget",
							GA_ID, 4,
							GA_RelVerify, TRUE,
							GA_Text, GetStr(MSG_OK_GAD),
							BUTTON_TextPadding, TRUE,
							GA_TabCycle, TRUE,
							TAG_END),
						CHILD_WeightedWidth, 1,
						LAYOUT_AddChild, NewObject( LABEL_GetClass(), NULL, LABEL_Text, "",
							TAG_END),
						CHILD_WeightedWidth, 100,
						LAYOUT_AddChild, NewObject( NULL, "button.gadget",
							GA_ID, 5,
							GA_RelVerify, TRUE,
							GA_Text, GetStr(MSG_CANCEL_GAD),
							BUTTON_TextPadding, TRUE,
							GA_TabCycle, TRUE,
							TAG_END),
						CHILD_WeightedWidth, 1,
						TAG_DONE),
					CHILD_WeightedHeight, 0,
					TAG_DONE),
				TAG_DONE);

	InitRequester(&BlockingReq);
	Request(&BlockingReq, ads->window);
	SetWindowPointer(ads->window, WA_BusyPointer, TRUE, TAG_DONE);

	projectWizardWindow = (struct Window *)DoMethod(projectWizardWindowObj, WM_OPEN, NULL);

	if (!projectWizardWindow)
		closeProjectWizard(ads);

	GetAttr(WINDOW_SigMask, projectWizardWindowObj, &sigmask);

	ads->ext->sigMask |= sigmask;
}

void closeProjectWizard(struct Codecraft *ads)
{
	if (projectWizardWindowObj)
	{
		ULONG sigmask;

		GetAttr(WINDOW_SigMask, projectWizardWindowObj, &sigmask);
		ads->ext->sigMask &= ~sigmask;
		DisposeObject(projectWizardWindowObj);
		projectWizardWindowObj  = NULL;
		projectWizardWindow = NULL;

		SetWindowPointer(ads->window, TAG_DONE);
		EndRequest(&BlockingReq, ads->window);
	}
}

static void replaceTemplateInAllFiles(BPTR dirLock, STRPTR projectName)
{
    BOOL more;
    BPTR oldLock;
    struct ExAllData *buffer = (struct ExAllData *)AllocMem(bufsize, MEMF_CLEAR);
    struct ExAllControl *eac;
	struct List fileList;
	struct Node *node;

    if (!buffer)
        return;

    eac = (struct ExAllControl *)AllocDosObject(DOS_EXALLCONTROL, NULL);

    if (!eac)
    {
        FreeMem(buffer, bufsize);
        return;
    }

	NewList(&fileList);
	
    eac->eac_LastKey = 0;

    oldLock = CurrentDir(dirLock);
    
    do {
        struct ExAllData *ead;
        int err;
        more = ExAll(dirLock, buffer, bufsize, ED_DATE, eac);
        err = IoErr();
        if ((!more) && (err != ERROR_NO_MORE_ENTRIES)) {
            break;
        }
        if (eac->eac_Entries == 0) {
            /* ExAll failed normally with no entries */
            continue;        /* ("more" is *usually* zero) */
        }
        
        ead = buffer;

        do {
            BOOL isDrawer = (ead->ed_Type >= 0 && ead->ed_Type != ST_SOFTLINK);
            if (isDrawer)
            {
	            if (ead->ed_Name[0] != '.')
				{
	                BPTR subLock = Lock(ead->ed_Name, SHARED_LOCK);

	                replaceTemplateInAllFiles(subLock, projectName);
		
	                UnLock(subLock);
				}
			}
			else
			{
				node = (struct Node *)AllocVec(sizeof(struct Node) + 108, MEMF_ANY);
				
				if (node)
				{
					node->ln_Name = ((STRPTR)node) + sizeof(struct Node);
					Strncpy(node->ln_Name, ead->ed_Name, 108);
					AddTail(&fileList, node);
				}
			}

            ead = ead->ed_Next;
        } while (ead);

    } while (more);


	while (node = RemTail(&fileList))
	{
		TEXT tmpStr[256];
		BPTR editFile;
		editFile = Open(node->ln_Name, MODE_OLDFILE);
		if (editFile)
		{
			ULONG i=0;
			ULONG len = Read(editFile, tmpStr, 256);
			
			while (i < len)
			{
				TEXT c = tmpStr[i];
				if (iscntrl(c) && !isspace(c))
					break;
				
				i++;
			}
			Close(editFile);

			if (i == len)
			{
				// reuse filehandle for something else
				editFile = Open("RAM:ccedit", MODE_NEWFILE);
				if (editFile)
				{
					FPrintf(editFile, "GE /%%TEMPLATE%%/%s/\n", projectName);
					Close(editFile);
					SNPrintf(tmpStr, 256, "EDIT FROM \"%s\" WITH RAM:ccedit >NIL:", node->ln_Name);
					SystemTags(tmpStr, TAG_DONE);
				}
			}
		}

		if (Strnicmp(node->ln_Name, "TEMPLATE", 8) == 0)
		{
			SNPrintf(tmpStr, 256, "RENAME \"%s\" \"", node->ln_Name);
			Strncat(tmpStr, projectName, 256);
			Strncat(tmpStr, node->ln_Name+8, 256);
			Strncat(tmpStr, "\"", 256);
			SystemTags(tmpStr, TAG_DONE);
		}
		
		FreeVec(node);
	}
	
    CurrentDir(oldLock);
    FreeDosObject(DOS_EXALLCONTROL, eac);
    FreeMem(buffer, bufsize);
}

static ULONG createProjectTree(struct Codecraft *ads)
{
	STRPTR s;
	STRPTR dirPath;
	BPTR dirLock = NULL;
	ULONG mode;
	BPTR lock;
	BPTR oldDir;
	TEXT fileName[MAXPATHLEN];
	struct Node *templateNode;

	GetAttr(STRINGA_TextVal, nameGadget, (ULONG *)&s);
	if (strlen(s) == 0)
	{
		askUser(projectWizardWindow, MSG_OK_GAD, MSG_NEWTREE_NONAME, NULL);
		return FALSE;
	}
	
	GetAttr(RADIOBUTTON_Selected, radiobutton, &mode);
	GetAttr(LISTBROWSER_SelectedNode, templateBrowser, (ULONG *)&templateNode);
	GetAttr(GETFILE_FullFile, drawerGadget, (ULONG *)&dirPath);
	
	if (dirPath[strlen(dirPath)] == '/')
		dirPath[strlen(dirPath)] = 0;
		
	switch (mode)
	{
		case 0:
			dirLock = Lock(dirPath, SHARED_LOCK);
			if (dirLock == NULL)
				askUser(projectWizardWindow, MSG_OK_GAD, MSG_NEWTREE_NODIR, NULL);

			break;
		
		case 2:
			if (templateNode == NULL)
			{
				askUser(projectWizardWindow, MSG_OK_GAD, MSG_NEWTREE_NOTEMPLATE, NULL);
				break;
			}
			// intentional fallthrough
		case 1:
			dirLock = Lock(dirPath, SHARED_LOCK);
			if (dirLock)
			{
				askUser(projectWizardWindow, MSG_OK_GAD, MSG_NEWTREE_ALREADYDIR, NULL);
				UnLock(dirLock);
				dirLock = NULL;
				break;
			}
			
			dirLock = createDirRecursively(dirPath);
			if (dirLock == NULL)
				askUser(projectWizardWindow, MSG_OK_GAD, MSG_NEWTREE_DIRCREATEFAIL, dirPath);
			break;
	}
	
	if (! dirLock)
		return FALSE;
		
	SNPrintf(fileName, MAXPATHLEN, "%s.projecttree", s);

	oldDir = CurrentDir(dirLock);
	lock = Lock(fileName, SHARED_LOCK);
	CurrentDir(oldDir);
	if (lock)
	{
		UnLock(lock);
		UnLock(dirLock);
		askUser(projectWizardWindow, MSG_OK_GAD, MSG_NEWTREE_ALREADY, NULL);
		return FALSE;
	}

	closeProjectTree(ads);
	memset(&ads->project, 0, sizeof(ads->project));
	ads->project.flags = CCPROJ_FLAG_NEWFMT;
	NewList((struct List *)&ads->project.cfgList);
	ads->project.cfg = (struct Configuration *)AllocVec(sizeof(struct Configuration), MEMF_ANY|MEMF_CLEAR);
	Strncpy(ads->project.cfg->name, "Default", 128);
	Insert(&ads->project.cfgList, (struct Node *)ads->project.cfg, 0);
	ads->project.currentCfgIndex = 0;

	if (mode == 2)
	{
		STRPTR templatePath;
		TEXT tmpStr[MAXPATHLEN];
		
		GetListBrowserNodeAttrs(templateNode,
			LBNA_Column, 1,
			LBNCA_Text, (ULONG *)&templatePath,
			TAG_DONE);
			
		SNPrintf(tmpStr, MAXPATHLEN, "COPY \"%s\" \"%s\" ALL", templatePath, dirPath);
		if (SystemTags(tmpStr, TAG_DONE))
			goto templatefail;
									
		SNPrintf(tmpStr, MAXPATHLEN, "DELETE \"%s", dirPath);
		AddPart(tmpStr+8, TEMPLATEDESCFILENAME, MAXPATHLEN-8);
		strcat(tmpStr, "\"");
		if (SystemTags(tmpStr, TAG_DONE))
			goto templatefail;
			
		replaceTemplateInAllFiles(dirLock, s);
		UnLock(dirLock);
		
		Strncpy(tmpStr, dirPath, MAXPATHLEN);
		AddPart(tmpStr, fileName, MAXPATHLEN);
		openProjectTree(ads, tmpStr);
		
		if (0) // we don't want this if things work:
		{
templatefail:
			Fault(IoErr(), "", tmpStr, MAXPATHLEN);
			askUser(projectWizardWindow, MSG_OK_GAD, MSG_NEWTREE_TEMPLATEFAIL, tmpStr);
			UnLock(dirLock);
			return FALSE;
		}
	}
	else
	{
		Strncpy(ads->project.cfg->includePattern, "((#?.(c|h))|(#?.(s|asm))|(#?.e)|(#?akefile))", sizeof(ads->project.cfg->includePattern));
	
		ads->projectDirLock = dirLock;
	}

	Strncpy(ads->project.name, s, sizeof(ads->project.name));
	Strncpy(ads->project.cfg->exeCmd, s, sizeof(ads->project.cfg->exeCmd));
		
	fillGui(ads);
	saveProjectTree(ads);
	editProjectTreeParameters(ads);
	
	return TRUE;
}

void projectWizardEventHandler(struct Codecraft *ads)
{
	ULONG result;
	ULONG code;

	if (!projectWizardWindowObj)
		return;

	while (result = DoMethod(projectWizardWindowObj, WM_HANDLEINPUT, &code))
	{
		switch (result & WMHI_CLASSMASK)
		{
		case WMHI_CLOSEWINDOW:
			closeProjectWizard(ads);
			break;


		case WMHI_GADGETUP:
			switch(result & WMHI_GADGETMASK)
			{
				case 1:
				{
					DoMethod(drawerGadget, GFILE_REQUEST, projectWizardWindow, NULL);
					break;
				}
				case 3:
				{
					ULONG mode;
					GetAttr(RADIOBUTTON_Selected, radiobutton, &mode);
					
					SetGadgetAttrs(templateBrowser, projectWizardWindow, NULL,
						GA_Disabled, mode != 2,
						TAG_DONE
						);
					break;
				}
				case 4:
				if (! createProjectTree(ads))
					break;
					
				ads->tei->installWelcomeGadget((Object *)1, "Get started");
				/* intentional fall through */
				case 5:
					closeProjectWizard(ads);
					break;
			}
			break;
		}
	}
}

