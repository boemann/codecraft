/* This file is part of Codecraft, an IDE running under AmigaOS 3.2.1 and later
 *
 * Copyright 2022 Camilla Boemann
 *
 * Codecraft is free software: you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2 as published by the Free Software Foundation.
 *
 * Codecrafr is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not,
 * see <https://www.gnu.org/licenses/>.
 */
#ifndef CODECRAFT_H
#define CODECRAFT_H

#include <math.h>

#include <exec/types.h>
#include <intuition/classusr.h>
#include <gadgets/texteditor.h>
#include <dos/dos.h>
#include <dos/notify.h>

#define MAXPATHLEN 256

#define BPIMG_COUNT 7
#define BPIMG_BREAK 0
#define BPIMG_CONTINUE 1
#define BPIMG_DISABLED 2
#define BPIMG_EXEC 4
#define BPIMG_CONTEXT 5
#define BPIMG_CLOSETAB 6

struct CodecraftDoc
{
    struct MinNode node;
    struct Codecraft *ads;
    APTR document;
	struct ChangeListenerCodecraft *changeListener;
    struct MinList breakPointList;
    struct Hook leftBarHook;
    BOOL changed;
	UBYTE fullPath[MAXPATHLEN];
};

struct Configuration
{
	struct Node node;
	UBYTE name[128];
	ULONG flags;
	UBYTE includePattern[128];
	UBYTE testUpToDateCmd[128];
	UBYTE exeCmd[128];
	UBYTE arguments[256];
	UBYTE workDir[MAXPATHLEN];
	UBYTE buildDir[MAXPATHLEN];
	UBYTE buildCmd[128];
	UBYTE cleanCmd[128];
};

#define CFG_FLAG_WB 0x00000001 // start as if from workbench


struct CodecraftProject
{
	ULONG flags;
	UBYTE name[128];
	UBYTE currentCfgIndex;
	
	//---- rest is not saved/loaded
	struct Configuration *cfg;
	struct List cfgList;
};
#define PROJECTHEADSIZE 132

#define CCPROJ_FLAG_NEWFMT 0x00000002 // New format

struct LinkedNotifyRequest
{
	struct MinNode node;
	struct NotifyRequest nr;
};

struct Codecraft
{
    struct TextEditInterface *tei;
	struct Extension *ext;
    struct Window *window;
	APTR visualInfo;
	struct ColorMap *CMap;
	struct Menu *menu;
	struct MenuItem *recentMenuItems;
	struct MenuItem *configMenuItems;
	struct Menu *settingsmenu;
	struct Menu *editmenu;
	UBYTE currentRecentBufferOffset;
	UBYTE currentRecentBuffer[199];
	UBYTE recentBuffer[1024];
	ULONG doRecentMenu;

    struct List msgNodeList;
	Object *buildBrowser;
	struct List breakpointBrowserList;
	Object *breakpointBrowser;
	struct List registerBrowserList;
	Object *registerBrowser;
	struct List memoryBrowserList;
	Object *memoryBrowserLayout;
	Object *memoryBrowser;
	Object *memoryBrowserAddrStr;
	Object *memoryBrowserTypeChooser;

	struct List variableBrowserList;
	Object *variableBrowser;
	struct Node *nextVariableNode;
	ULONG expandVariableMode;
	ULONG variableBrowserAltPen;

	struct List callStackBrowserList;
	Object *callStackBrowser;

	struct List searchFilesBrowserList;
	Object *searchFilesBrowser;
	struct Node *SearchTab;
	
	struct List utilityTabs;
	Object *utilityClickTab;
	Object *utilityPageGroup;
	Object *utilityICObject;
	
	struct MsgPort *asyncFilePort;
	struct MsgPort *revealPort;

	Object *welcomeGadget;

	struct List projectTreeList;
	ULONG notifySignal;
	struct MinList notifyRequestList;
    
	Object *makefileExpGadget;
    Object *makefileListGadget;
    Object *makefileGadget;
    ULONG makefileGadWeight;

	struct Image *Images[BPIMG_COUNT];
    struct FileRequester *FileRequester;
	struct MinList documentList;
	struct MinList breakpointDocList;
	struct CodecraftDoc *currentAdsDocument;
    UBYTE matchingPat[256];
    UBYTE pipeBuffer[128];
    UBYTE messageBuffer[256];
    BPTR pipeFh;
	BPTR projectDirLock;
	UBYTE windowtitle[128];
	struct CodecraftProject project;
	ULONG FollowupAfterBuild;
	BOOL buildInProgress;
	
	struct Debugger *debugger;
};

#define MAXTYPELEN 256

#define GLOBALGID_MAKEFILEBROWSER	1000
#define GLOBALGID_BUILDBROWSER		1001
#define GLOBALGID_BREAKPOINTBROWSER	1002
#define GLOBALGID_VARIABLEBROWSER	1003
#define GLOBALGID_CALLSTACKBROWSER	1004
#define GLOBALGID_SEARCHBROWSER		1005
#define GLOBALGID_SIDEBARRESIZER	1009
#define GLOBALGID_REGISTERBROWSER	1010
#define GLOBALGID_MEMORYBROWSER		1011
#define GLOBALGID_MEMORYBROWSERUP	1012
#define GLOBALGID_MEMORYBROWSERDOWN	1013
#define GLOBALGID_MEMORYBROWSERADDR	1014
#define GLOBALGID_MEMORYBROWSERGOTO	1015
#define GLOBALGID_MEMORYBROWSERTYPE 1016

struct RevealLocationMsg
{
	struct Message msg;
	ULONG type;
	ULONG blockNum;
	TEXT filename[0];
};


struct VariableBrowserInfo
{
	ULONG elementSize;
	ULONG referenceBlockNum;
	TEXT nameString[MAXTYPELEN];
	TEXT valueString[MAXTYPELEN];
	TEXT ptrString[MAXTYPELEN];
	// When showing a subelement we need to skip its parents
	// The following two allows us to keep track of how many
	// (grand)parents we should ignore in order to show the
	// subelement.
	// They start out being the same but if there are typedefs
	// involved, then the skipping has to be decimated as
	// we decend towards the subelement.
	// the total is what is stored in the variablebrowser
	// the other is helping in the traversal.
	ULONG totalSkipPtrSteps; // from root to the subelement
	ULONG skipPtrSteps; // from nearest typedef to the subelement
	TEXT funcPtrString[MAXTYPELEN];
	TEXT typeString[MAXTYPELEN];
	TEXT encodedTypeString[MAXTYPELEN];
};


// from session.c
void loadSession(void);
void saveSession(void);
struct Debugger *CreateDebugger(struct Codecraft *ads);
void writeExpandedNodes(struct Codecraft *ads, BPTR file, STRPTR buffer);
void expandNodes(struct Codecraft *ads, BPTR file, STRPTR buffer);

// from codecraft.c
void updateRecentMenu(struct Codecraft *ads);
void disposeCodecraftDoc(struct CodecraftDoc *adsDoc);
LONG askUser(struct Window *win, ULONG gadsMsg, ULONG questionMsg, STRPTR opt);

// from actions.c
void saveAll(struct Codecraft *ads);
void UpdateMenuStates(struct Codecraft *ads, BOOL gr1, BOOL gr2, BOOL gr3);
void updateConfigurationMenu(struct Codecraft *ads);

// from projecttree.c
void parseBuildMessages(struct Codecraft *ads);
void buildProjectTree(struct Codecraft *ads, STRPTR cmd, ULONG restartOutput);
void newProjectTree(struct Codecraft *ads);
void editProjectTreeParameters(struct Codecraft *ads);
void editBuildProfiles(struct Codecraft *ads);
void openProjectTree(struct Codecraft *ads, STRPTR optionalFullPath);
void openRecentProjectTree(struct Codecraft *ads, UWORD index);
void gotoPrevMessage(struct Codecraft *ads);
void gotoNextMessage(struct Codecraft *ads);
void runProject(struct Codecraft *ads, BOOL withDebug);
void closeProjectTree(struct Codecraft *ads);
void saveProjectTree(struct Codecraft *ads);
void projectWizardEventHandler(struct Codecraft *ads);
void projectParametersEventHandler(struct Codecraft *ads);
void buildProfilesEventHandler(struct Codecraft *ads);
ULONG parseBuildLine(struct Codecraft *ads, STRPTR text, struct Node *outputNode);
void setWindowTitles(struct Codecraft *ads);
void fillGui(struct Codecraft *ads);
void expandNode(struct Node *node, STRPTR path, LONG visible);
struct CodecraftDoc *revealLocation(struct Codecraft *ads, STRPTR path, ULONG blockNum);
struct Process *runAsIfFromCLI(struct Codecraft *ads, BPTR homeLock, BPTR seglist, ...);
struct Process *runAsIfFromWb(struct Codecraft *ads, BPTR homeLock, BPTR seglist, ...);

// from variablebrowser.c
void createVariableBrowser(struct Codecraft *ads);
ULONG createOrUpdateEntry(ULONG level, struct VariableBrowserInfo *vbi,
   ULONG hasChildren, UBYTE *memPtr);
void clearVariableBrowser(struct Codecraft *ads);
void clearTrailingVariableNodes(struct Codecraft *ads);
void onVariableBrowserGadgetUp(struct Codecraft *ads);

// from registerbrowser.c
void clearRegisterBrowser(struct Codecraft *ads);
void createRegisterBrowser(struct Codecraft *ads);
void UpdateRegisterBrowser(struct Codecraft *ads);

// from memorybrowser.c
void clearMemoryBrowser(struct Codecraft *ads);
void createMemoryBrowser(struct Codecraft *ads);
void UpdateMemoryBrowser(struct Codecraft *ads, BOOL showUpdates);
void browseMemory(struct Codecraft *ads);
void browseMemoryFromAddress(struct Codecraft *ads, UBYTE *addr);
void browseMemoryDown(struct Codecraft *ads);
void browseMemoryUp(struct Codecraft *ads);
void freeMemoryBrowser(void);

// from callstackbrowser.c
void createCallStackBrowser(struct Codecraft *ads);
void createCallStackEntry(UBYTE *sp, STRPTR funcname, STRPTR filename, ULONG linenumber);
void clearCallStackBrowser(struct Codecraft *ads);
void onCallStackBrowserGadgetUp(struct Codecraft *ads);

// from searchfiles.c
void openSearchFilesRequester(struct Codecraft *ads);
void searchFilesEventHandler(struct Codecraft *ads);
void CloseSearch(struct Codecraft *ads);
void clearSearchInFilesPath();

//from projecttreeparameters.c
void refreshBuildProfiles(struct Codecraft *ads);

#endif /* CODECRAFT_H */
