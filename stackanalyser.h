/* This file is part of Codecraft, and IDE running under AmigaOS 3.2.1 and later
 *
 * Copyright 2022 Camilla Boemann
 *
 * Codecraft is free software: you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2 as published by the Free Software Foundation.
 *
 * Codecrafr is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not,
 * see <https://www.gnu.org/licenses/>.
 */

void UpdateVariableBrowser(struct Debugger *dbgr, STRPTR fullPath, ULONG blockNum, UBYTE *sp);
void UpdateRegisterBrowser(struct Codecraft *ads);
void UpdateMemoryBrowser(struct Codecraft *ads, BOOL showUpdates);

UBYTE *UpdateCallStackBrowser(struct Codecraft *ads, UWORD *brokenAddress);
void ExpandVariableBrowser(struct Debugger *dbgr, STRPTR fullPath,
	ULONG blockNum, STRPTR encodedType, ULONG level,
	ULONG elementSize, ULONG skipPtrSteps, ULONG refBlockNum, UBYTE *memPtr);


void clearEntryInfo(struct DebugInfoParser *sdl);


