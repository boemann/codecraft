/* This file is part of Codecraft, and IDE running under AmigaOS 3.2.1 and later
 *
 * Copyright 2022 Camilla Boemann
 *
 * Codecraft is free software: you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2 as published by the Free Software Foundation.
 *
 * Codecrafr is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not,
 * see <https://www.gnu.org/licenses/>.
 */

#include <intuition/classusr.h>
#include <gadgets/listbrowser.h>
#include <gadgets/layout.h>
#include <gadgets/clicktab.h>

#include <proto/exec.h>

#define __CLIB_PRAGMA_LIBCALL
#include <clib/alib_protos.h>

#include <proto/intuition.h>
#include <proto/utility.h>
#include <proto/dos.h>
#include <proto/locale.h>
#include <proto/listbrowser.h>
#include <proto/layout.h>
#include <proto/clicktab.h>

#include <tools/textedit/extension.h>

#include "breakpoints.h"
#include "codecraft.h"
#include "debugger.h"
#include "stackanalyser.h"

extern struct Library *UtilityBase;
extern struct Locale *Locale;
extern struct Catalog *Catalog;

extern struct Codecraft myCodecraft;


void createCallStackEntry(UBYTE *sp, STRPTR funcName, STRPTR filename, ULONG lineNumber)
{
	struct Node *node;
	struct Codecraft *ads = &myCodecraft;
	TEXT text[256];
	BOOL first = ads->callStackBrowserList.lh_Head->ln_Succ == NULL;
	
	if (funcName)
		SNPrintf(text, sizeof(text), "%s,   %s(),   line %ld",
		FilePart(filename), funcName, lineNumber + 1);
	else if (filename)
		SNPrintf(text, sizeof(text), "%s,   line %ld", FilePart(filename), lineNumber + 1);
	else
		SNPrintf(text, sizeof(text), "???");

	node = AllocListBrowserNode(
		5,
		LBNA_UserData, sp,
		LBNA_Column, 0,
		LBNCA_Image, first ? ads->Images[BPIMG_EXEC] : NULL,
		LBNA_Column, 1,
		LBNCA_CopyText, TRUE,
		LBNCA_Text, text,
		LBNA_Column, 2,
		LBNCA_CopyInteger, TRUE,
		LBNCA_Integer, &lineNumber,
		LBNA_Column, 3,
		LBNCA_CopyText, TRUE,
		LBNCA_Text, filename,
		TAG_DONE);

	AddTail(&ads->callStackBrowserList, node);
	SetGadgetAttrs(	(struct Gadget *) ads->callStackBrowser, ads->window, NULL,
		LISTBROWSER_Labels, (ULONG) &ads->callStackBrowserList,
		TAG_DONE
		);
}

void createCallStackBrowser(struct Codecraft *ads)
{
	struct ColumnInfo *ci;

	NewList(&ads->callStackBrowserList);
	
	ci = AllocLBColumnInfo(2,
				LBCIA_Column, 0,
				LBCIA_Width, 16,
				LBCIA_Column, 1,
				LBCIA_Weight, 100,
				TAG_DONE);

	ads->callStackBrowser = NewObject(LISTBROWSER_GetClass(), NULL,
				GA_ID, GLOBALGID_CALLSTACKBROWSER,
				GA_RelVerify, TRUE,
				LISTBROWSER_MultiSelect, FALSE,
				LISTBROWSER_Separators, FALSE,
				LISTBROWSER_ShowSelected, TRUE,
				LISTBROWSER_ColumnInfo, (ULONG)ci,
				LISTBROWSER_Labels, &ads->callStackBrowserList,
				TAG_DONE);

	SetGadgetAttrs((struct Gadget *)ads->utilityPageGroup, ads->window, NULL,
				PAGE_Add, ads->callStackBrowser,
				TAG_END);
}

void clearCallStackBrowser(struct Codecraft *ads)
{
	SetGadgetAttrs(	(struct Gadget *) ads->callStackBrowser, ads->window, NULL,
		LISTBROWSER_Labels, (ULONG) NULL,
		TAG_DONE
		);

	FreeListBrowserList(&ads->callStackBrowserList);

	SetGadgetAttrs(	(struct Gadget *) ads->callStackBrowser, ads->window, NULL,
		LISTBROWSER_Labels, (ULONG) &ads->callStackBrowserList,
		TAG_DONE
		);
}

void onCallStackBrowserGadgetUp(struct Codecraft *ads)
{
	ULONG relevent;
	struct Node *node;
	struct Debugger *dbgr = ads->debugger;
	
	GetAttr(LISTBROWSER_SelectedNode, ads->callStackBrowser, (ULONG *)&node);
	GetAttr(LISTBROWSER_RelEvent, ads->callStackBrowser, &relevent);
	
	if (node && relevent == LBRE_DOUBLECLICK)
	{
		STRPTR fullPath = NULL;
		ULONG *blockNum = 0;
		UBYTE *sp;
		
		GetListBrowserNodeAttrs(node,
			LBNA_UserData, (ULONG *)&sp,
			LBNA_Column, 2,
			LBNCA_Integer, (ULONG *)&blockNum,
			LBNA_Column, 3,
			LBNCA_Text, (ULONG *)&fullPath,
			TAG_DONE);


		if (fullPath)
		{
			dbgr->pathChanged = strcmp(dbgr->contextFullPath, fullPath);
			if  (dbgr->pathChanged)
				Strncpy(dbgr->contextFullPath, fullPath, sizeof(dbgr->contextFullPath));
		}
		else
		{
			dbgr->contextFullPath[0] = 0;
			dbgr->pathChanged = TRUE;
		}
		
		dbgr->contextBlockNum = *blockNum;
		
		if (dbgr->contextNode)
		{
			SetListBrowserNodeAttrs(dbgr->contextNode,
				LBNA_Column, 0,
				LBNCA_Image, NULL,
				TAG_DONE);
		}

		dbgr->contextNode = NULL;
		if (node->ln_Pred->ln_Pred) // we will not change the first
		{
			SetListBrowserNodeAttrs(node,
				LBNA_Column, 0,
				LBNCA_Image, ads->Images[BPIMG_CONTEXT],
				TAG_DONE);
			dbgr->contextNode = node;
		}

		if (fullPath)
			revealLocation(ads, fullPath, *blockNum);
		UpdateVariableBrowser(dbgr, fullPath, *blockNum, sp);
		ads->tei->redrawLeftBar(ads->currentAdsDocument->document);
		RefreshGList((struct Gadget *) ads->callStackBrowser, ads->window, NULL, 1);
	}
}
