/* This file is part of Codecraft, an IDE running under AmigaOS 3.2.1 and later
 *
 * Copyright 2022 Camilla Boemann
 *
 * Codecraft is free software: you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2 as published by the Free Software Foundation.
 *
 * Codecraft is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not,
 * see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/utility.h>
#include <ctype.h>

#include "evar.h"
#include "debugger.h"

#include <gadgets/listbrowser.h>
#include <proto/listbrowser.h>

extern struct DebuggerRegisters *brokenRegs;

static UBYTE selfdata1[] = { 0, 0, 0, 1, 0, 0 };
static UBYTE selfdata2[] = { 0, 0, 0, 1, 0, EVAR_VARTYPE_PTR_TO_OBJECT, 0, 0, 0, 0, 0, 6, 'S', 'e', 'l', 'f', 0, 0 };

struct DebugInfoParserEVAR
{
	struct DebugInfoParser dip;

	ULONG EVarDataLength;
	UBYTE *buffer;
	UBYTE *objDefs;
};

STRPTR LookupEObjectName(struct DebugInfoParserEVAR *dip, struct Debugger *dbgr, ULONG oid);
UWORD LookupEObjectSize(struct DebugInfoParserEVAR *dip, struct Debugger *dbgr, ULONG oid);
ULONG recurseEArray(struct DebugInfoParserEVAR *dip,  struct Debugger *dbgr, UWORD level, UWORD maxlevel, ULONG varobjectid, ULONG vararraylen, UWORD *vardims, ULONG vartype, ULONG varval, ULONG elementSize);
UBYTE *LookupEObjectDefinition(struct DebugInfoParserEVAR *dip, struct Debugger *dbgr, ULONG oid);
BOOL setETypeAndValue(struct DebugInfoParserEVAR *dip, struct Debugger *dbgr, BOOL member, ULONG vartype, UBYTE *varval, ULONG varobjectid, ULONG vararraylen, UWORD *vardims, BOOL baseType);
void recurseEObject(struct DebugInfoParserEVAR *dip,  struct Debugger *dbgr, UWORD level, ULONG oid, ULONG varval);
void ParseEvarData(struct DebugInfoParser *sdl, struct Debugger *dbgr, ULONG blockNum,
	UBYTE *processFrom);

LONG ReadMem(UBYTE *memPtr, ULONG len)
{
	LONG res = 0,i;
	
	if (TypeOfMem(memPtr))
	{
		for (i=0; i<len;i++)
		{
			res = (res << 8) | *memPtr++;
		}
	}
	return res;
}

static void ClearEntryInfo(struct DebugInfoParserEVAR *dip)
{
	struct VariableBrowserInfo *vbi = &dip->dip.vbi;
	
	*vbi->nameString = *vbi->typeString = *vbi->valueString
		= *vbi->ptrString = *vbi->funcPtrString
		= *vbi->encodedTypeString = 0;
}
	

void FreeEVAR(struct DebugInfoParserEVAR *dip)
{
	FreeVec(dip->buffer);
	FreeVec(dip);
}

void ExpandENode(struct DebugInfoParserEVAR *dip, struct Debugger *dbgr, ULONG blockNum,
		 STRPTR encodedType, ULONG level, ULONG elementSize,
		 ULONG skipPtrSteps, ULONG refBlockNum, 
		 UBYTE *memPtr)
{
	struct VariableBrowserInfo *vbi = &dip->dip.vbi;
	ULONG oid = (ULONG)strtoul(encodedType, NULL, 16);
	ULONG membtype = (ULONG)strtoul(encodedType+18, NULL, 16);
	
	level = (ULONG)strtoul(encodedType+9, NULL, 16);
	
	if (vbi->referenceBlockNum == -1)
	{
		recurseEObject(dip, dbgr, level+1 , oid,
			(membtype == EVAR_VARTYPE_OBJECT || membtype == EVAR_VARTYPE_ARRAY_OF_OBJECT) ? (ULONG)memPtr : (ULONG)ReadMem(memPtr,4));
	}
	else if (vbi->referenceBlockNum == -2)
	{
		ULONG objArrayLen = (ULONG)strtoul(encodedType+27, NULL, 16);
		UWORD *objDims = (UWORD *)strtoul(encodedType+36, NULL, 16);
		ULONG elementSize = (ULONG)strtoul(encodedType+45, NULL, 16);
	
		recurseEArray(dip, dbgr, level+1, objArrayLen + level , oid, objArrayLen,
			objDims, membtype, (ULONG)memPtr, elementSize);
	}
	else
	{
		ParseEvarData(dip, dbgr, blockNum, NULL);
	}
}

void recurseEObject(struct DebugInfoParserEVAR *dip,  struct Debugger *dbgr, UWORD level,
	ULONG oid, ULONG varval)
{
	struct VariableBrowserInfo *vbi = &dip->dip.vbi;
	UBYTE *buf;
	ULONG nameLen, objArrayLen, membtype, objOid, objSize, expand;
	ULONG elementSize, membOffset;
	UWORD *objDims;
	BOOL hasChildren;

	ClearEntryInfo(dip);

	buf = LookupEObjectDefinition(dip, dbgr, oid);
	if (buf)
	{
		objOid = *(WORD *)(buf); //object oid
		buf += 2; 
		
		buf += 2; //object flags + pad
		
		objSize = *(WORD *)(buf);	//object size
		buf += 2;
		
		nameLen = *(WORD *)(buf);	//object name length
		buf += 2;
		buf += nameLen;	//skip name
		
		membtype = *(WORD *)(buf);	//member type (0 to terminate)
		buf += 2; //type
		while (membtype)
		{
			objOid = *(WORD *)(buf);	//member object oid
			buf += 2; //oid
			membOffset = *(WORD *)(buf);	//member offset
			buf += 2;
			objArrayLen = *(WORD *)(buf);	//number of array dimensions
			buf += 2;
			objDims = (UWORD *)buf;
			buf += objArrayLen*2; //skip array dimensions info

			nameLen = *(WORD *)(buf);	//name length
			buf += 2;

			Strncpy(vbi->nameString, buf, sizeof(vbi->nameString));

			buf += nameLen;	//skip name
			hasChildren = setETypeAndValue(dip, dbgr, TRUE, membtype, (UBYTE *)(varval+membOffset), objOid,
				objArrayLen, objDims, FALSE);
			elementSize = vbi->elementSize;
			
			if (objArrayLen)
			{
				vbi->referenceBlockNum = -2;
				SNPrintf(vbi->encodedTypeString, MAXTYPELEN, "%08lx %08lx %08lx %08lx %08lx %08lx",
					objOid,level,membtype,objArrayLen,objDims, vbi->elementSize);
			}
			else
			{
				vbi->referenceBlockNum = -1;
				SNPrintf(vbi->encodedTypeString, MAXTYPELEN, "%08lx %08lx %08lx", objOid,level,membtype);
				//kprintf("name %s vbi->encodedTypeString %s\n",vbi->nameString,vbi->encodedTypeString);
			}
			expand = createOrUpdateEntry(level, &dip->dip.vbi,hasChildren,(UBYTE *)(varval+membOffset));
			if (expand)
			{
				if (objArrayLen)
					recurseEArray(dip, dbgr, level+1, objArrayLen + level , objOid, objArrayLen,
						objDims, membtype, (varval+membOffset), vbi->elementSize);
				else
					recurseEObject(dip, dbgr, level+1, objOid,
						(membtype == EVAR_VARTYPE_OBJECT || membtype == EVAR_VARTYPE_ARRAY_OF_OBJECT) ? (varval+membOffset) : (ULONG)ReadMem((UBYTE *)(varval+membOffset),4));
			}

			membtype = *(WORD *)(buf);	//next member type
			buf += 2; //type
		}
	}
}

char *getVarFormatStr(ULONG vartype, UBYTE *varval)
{
	char *formatStr;
	
	if (TypeOfMem(varval))
	{
		switch (vartype)
		{
			case EVAR_VARTYPE_PTR_TO_CHAR:
				if (strlen(varval)>30)
					formatStr = "<addr> 0x%08lx (%ld \"%.30s\"...)";
				else
					formatStr = "<addr> 0x%08lx (%ld \"%s\")";
				break;

			case EVAR_VARTYPE_PTR_TO_BYTE:
			case EVAR_VARTYPE_PTR_TO_INT:
			case EVAR_VARTYPE_PTR_TO_WORD:
			case EVAR_VARTYPE_PTR_TO_LONG:
					formatStr = "<addr> 0x%08lx (%ld 0x%lx)";
					break;
				
			case EVAR_VARTYPE_ARRAY_OF_CHAR:
				if (strlen(varval)>30)
					formatStr = "<addr> 0x%08lx (\"%.30s\"...)";
				else
					formatStr = "<addr> 0x%08lx (\"%s\")";
				break;

			default:
				formatStr = "<addr> 0x%08lx";
		}
	}
	else
	{
		if (vartype==EVAR_VARTYPE_PTR_TO_CHAR)
			formatStr = "<illegal addr> 0x%08lx (%ld)";
		else
			formatStr = "<illegal addr> 0x%08lx";
	}
	return formatStr;
}

BOOL setETypeAndValue(struct DebugInfoParserEVAR *dip, struct Debugger *dbgr, BOOL member,
	ULONG vartype, UBYTE *varval, ULONG varobjectid, ULONG vararraylen,
	UWORD *vardims, BOOL baseType)
{
	struct VariableBrowserInfo *vbi = &dip->dip.vbi;
	BOOL hasChildren = 0;
	ULONG j;
	LONG derefVal;
	char tempSize[8];

	switch(vartype)
	{
		case EVAR_VARTYPE_UNKNOWN:
			vbi->elementSize = 4;
			Strncpy(vbi->typeString, "unknown", MAXTYPELEN);
			SNPrintf(vbi->valueString, sizeof(vbi->valueString),"0x%08lx",
				varval);
			break;
		case EVAR_VARTYPE_CHAR:
			vbi->elementSize = 1;
			Strncpy(vbi->typeString, "CHAR", MAXTYPELEN);
			derefVal = (UBYTE)ReadMem(varval,1);
			if (isprint(derefVal))
				SNPrintf(vbi->valueString, sizeof(vbi->valueString),"%ld (0x%02lx \"%lc\")",
					derefVal,derefVal&0xff,derefVal);
			else
				SNPrintf(vbi->valueString, sizeof(vbi->valueString),"%ld (0x%02lx)",
					derefVal,derefVal&0xff);
			break;
		case EVAR_VARTYPE_BYTE:
			vbi->elementSize = 1;
			Strncpy(vbi->typeString, "BYTE", MAXTYPELEN);
			derefVal = (BYTE)ReadMem(varval,1);
			SNPrintf(vbi->valueString, sizeof(vbi->valueString),"%ld (0x%02lx)",
				derefVal,derefVal&0xff);
			break;
		case EVAR_VARTYPE_INT:
			vbi->elementSize = 2;
			Strncpy(vbi->typeString, "INT", MAXTYPELEN);
			derefVal = (WORD)ReadMem(varval,2);
			SNPrintf(vbi->valueString, sizeof(vbi->valueString),"%ld (0x%04lx)",
				derefVal,derefVal&0xffff);
			break;
		case EVAR_VARTYPE_WORD:
			vbi->elementSize = 2;
			Strncpy(vbi->typeString, "WORD", MAXTYPELEN);
			derefVal = (UWORD)ReadMem(varval,2);
			SNPrintf(vbi->valueString, sizeof(vbi->valueString),"%ld (0x%04lx)",
				derefVal,derefVal&0xffff);
			break;
		case EVAR_VARTYPE_LONG:
			vbi->elementSize = 4;
			Strncpy(vbi->typeString, "LONG", MAXTYPELEN);
			derefVal = ReadMem(varval,4);
			SNPrintf(vbi->valueString, sizeof(vbi->valueString),"%ld (0x%08lx)",
				derefVal,derefVal);
			break;
		case EVAR_VARTYPE_OBJECT:
			hasChildren = 1;
			vbi->elementSize = LookupEObjectSize(dip, dbgr, varobjectid);
			
			SNPrintf(vbi->typeString, sizeof(vbi->typeString),"%s",
				LookupEObjectName(dip, dbgr, varobjectid));
			SNPrintf(vbi->valueString, sizeof(vbi->valueString),"0x%08lx",
				varval);
			break;
		case EVAR_VARTYPE_PTR_TO_CHAR:
			vbi->elementSize = 4;
			Strncpy(vbi->typeString, "LONG/PTR TO CHAR", MAXTYPELEN);
			if (member) varval = (UBYTE *)ReadMem(varval,4);
			SNPrintf(vbi->valueString, sizeof(vbi->valueString),getVarFormatStr(vartype,varval),varval,varval,varval);
			break;
		case EVAR_VARTYPE_PTR_TO_BYTE:
			vbi->elementSize = 4;
			Strncpy(vbi->typeString, "PTR TO BYTE", MAXTYPELEN);
			if (member) varval = (UBYTE *)ReadMem(varval,4);
			derefVal = (BYTE)ReadMem(varval,1);
			SNPrintf(vbi->valueString, sizeof(vbi->valueString),getVarFormatStr(vartype,varval),varval, derefVal,derefVal&0xff);
			break;
		case EVAR_VARTYPE_PTR_TO_INT:
			vbi->elementSize = 4;
			Strncpy(vbi->typeString, "PTR TO INT", MAXTYPELEN);
			if (member) varval = (UBYTE *)ReadMem(varval,4);
			derefVal = (WORD)ReadMem(varval,2);
			SNPrintf(vbi->valueString, sizeof(vbi->valueString),getVarFormatStr(vartype,varval),varval,derefVal,derefVal&0xffff);
			break;
		case EVAR_VARTYPE_PTR_TO_WORD:
			vbi->elementSize = 4;
			Strncpy(vbi->typeString, "PTR TO WORD", MAXTYPELEN);
			if (member) varval = (UBYTE *)ReadMem(varval,4);
			derefVal = (UWORD)ReadMem(varval,2);
			SNPrintf(vbi->valueString, sizeof(vbi->valueString),getVarFormatStr(vartype,varval),varval,derefVal,derefVal&0xffff);
			break;
		case EVAR_VARTYPE_PTR_TO_LONG:
			vbi->elementSize = 4;
			Strncpy(vbi->typeString, "PTR TO LONG", MAXTYPELEN);
			if (member) varval = (UBYTE *)ReadMem(varval,4);
			derefVal = (LONG)ReadMem(varval,4);
			SNPrintf(vbi->valueString, sizeof(vbi->valueString),getVarFormatStr(vartype,varval),varval,derefVal,derefVal);
			break;
		case EVAR_VARTYPE_PTR_TO_OBJECT:
			vbi->elementSize = 4;
			hasChildren = 1;
			SNPrintf(vbi->typeString, sizeof(vbi->typeString),"PTR TO %s",
				LookupEObjectName(dip, dbgr, varobjectid));
			if (member) varval = (UBYTE *)ReadMem(varval,4);
			SNPrintf(vbi->valueString, sizeof(vbi->valueString),getVarFormatStr(vartype,varval),varval);
			break;
		case EVAR_VARTYPE_PTR_TO_PTR:
			vbi->elementSize = 4;
			Strncpy(vbi->typeString, "PTR TO PTR TO ...", MAXTYPELEN);
			if (member) varval = (UBYTE *)ReadMem(varval,4);
			SNPrintf(vbi->valueString, sizeof(vbi->valueString),getVarFormatStr(vartype,varval),varval);
			break;

		case EVAR_VARTYPE_ARRAY_OF_CHAR:
			vbi->elementSize = 1;
			if (baseType)
			{
				Strncpy(vbi->typeString, "CHAR",sizeof(vbi->typeString));
				derefVal = (UBYTE)ReadMem(varval,1);
				if (isprint(derefVal))
					SNPrintf(vbi->valueString, sizeof(vbi->valueString),"%ld (0x%02lx \"%lc\")",
						derefVal,derefVal&0xff,derefVal);
				else
					SNPrintf(vbi->valueString, sizeof(vbi->valueString),"%ld (0x%02lx)",
						derefVal,derefVal&0xff);			
			}
			else
			{
				Strncpy(vbi->typeString, "ARRAY OF CHAR", MAXTYPELEN);
				hasChildren = 1;
				for (j = 0; j < vararraylen; j++)
				{
					SNPrintf(tempSize, sizeof(tempSize),"[%ld]",vardims[j]);
					Strncat(vbi->typeString, tempSize, MAXTYPELEN);
				}
				SNPrintf(vbi->valueString, sizeof(vbi->valueString),getVarFormatStr(vartype,varval),varval,varval,varval);

			}
			break;
		case EVAR_VARTYPE_ARRAY_OF_BYTE:
			vbi->elementSize = 1;
			if (baseType)
			{
				Strncpy(vbi->typeString, "BYTE",sizeof(vbi->typeString));
				derefVal = (BYTE)ReadMem(varval,1);
				SNPrintf(vbi->valueString, sizeof(vbi->valueString),"%ld (0x%02lx)",
					derefVal,derefVal&0xff);
			}
			else 
			{
				Strncpy(vbi->typeString, "ARRAY OF BYTE", MAXTYPELEN);
				hasChildren = 1;
				for (j = 0; j < vararraylen; j++)
				{
					SNPrintf(tempSize, sizeof(tempSize),"[%ld]",vardims[j]);
					Strncat(vbi->typeString, tempSize, MAXTYPELEN);
				}
				SNPrintf(vbi->valueString, sizeof(vbi->valueString),getVarFormatStr(vartype,varval),varval,varval,varval);
			}
			break;
		case EVAR_VARTYPE_ARRAY_OF_INT:
			vbi->elementSize = 2;
			if (baseType)
			{
				Strncpy(vbi->typeString, "INT",sizeof(vbi->typeString));
				derefVal = (WORD)ReadMem(varval,2);
				SNPrintf(vbi->valueString, sizeof(vbi->valueString),"%ld (0x%04lx)",
					derefVal,derefVal&0xffff);
			}
			else 
			{
				Strncpy(vbi->typeString, "ARRAY OF INT", MAXTYPELEN);
				hasChildren = 1;
				for (j = 0; j < vararraylen; j++)
				{
					SNPrintf(tempSize, sizeof(tempSize),"[%ld]",vardims[j]);
					Strncat(vbi->typeString, tempSize, MAXTYPELEN);
				}
				SNPrintf(vbi->valueString, sizeof(vbi->valueString),getVarFormatStr(vartype,varval),varval,varval,varval);
			}
			break;
		case EVAR_VARTYPE_ARRAY_OF_WORD:
			vbi->elementSize = 2;
			if (baseType)
			{
				Strncpy(vbi->typeString, "WORD",sizeof(vbi->typeString));
				derefVal = (UWORD)ReadMem(varval,2);
				SNPrintf(vbi->valueString, sizeof(vbi->valueString),"%ld (0x%04lx)",
					derefVal,derefVal&0xffff);
			}
			else 
			{
				Strncpy(vbi->typeString, "ARRAY OF WORD", MAXTYPELEN);
				hasChildren = 1;
				for (j = 0; j < vararraylen; j++)
				{
					SNPrintf(tempSize, sizeof(tempSize),"[%ld]",vardims[j]);
					Strncat(vbi->typeString, tempSize, MAXTYPELEN);
				}
				SNPrintf(vbi->valueString, sizeof(vbi->valueString),getVarFormatStr(vartype,varval),varval,varval,varval);
			}
			break;
		case EVAR_VARTYPE_ARRAY_OF_LONG:
			vbi->elementSize = 4;
			if (baseType)
			{
				Strncpy(vbi->typeString, "LONG",sizeof(vbi->typeString));
				derefVal = (LONG)ReadMem(varval,4);
				SNPrintf(vbi->valueString, sizeof(vbi->valueString),"%ld (0x%08lx)",
					derefVal,derefVal);
			}
			else 
			{
				Strncpy(vbi->typeString, "ARRAY OF LONG", MAXTYPELEN);
				hasChildren = 1;
				for (j = 0; j < vararraylen; j++)
				{
					SNPrintf(tempSize, sizeof(tempSize),"[%ld]",vardims[j]);
					Strncat(vbi->typeString, tempSize, MAXTYPELEN);
				}
				SNPrintf(vbi->valueString, sizeof(vbi->valueString),getVarFormatStr(vartype,varval),varval,varval,varval);
			}
			break;
		case EVAR_VARTYPE_ARRAY_OF_OBJECT:
			hasChildren = 1;
			vbi->elementSize = LookupEObjectSize(dip, dbgr, varobjectid);

			if (baseType)
			{
				SNPrintf(vbi->typeString, sizeof(vbi->typeString),"%s",
					LookupEObjectName(dip, dbgr, varobjectid));
				SNPrintf(vbi->valueString, sizeof(vbi->valueString),getVarFormatStr(vartype,varval),
					varval);
			}
			else 
			{
				SNPrintf(vbi->typeString, sizeof(vbi->typeString),"ARRAY OF %s",
					LookupEObjectName(dip, dbgr, varobjectid));

				for (j = 0; j < vararraylen; j++)
				{
					SNPrintf(tempSize, sizeof(tempSize),"[%ld]",vardims[j]);
					Strncat(vbi->typeString, tempSize, MAXTYPELEN);
				}
				SNPrintf(vbi->valueString, sizeof(vbi->valueString),getVarFormatStr(vartype,varval),varval,varval,varval);
			}
			break;
		case EVAR_VARTYPE_ARRAY_OF_PTR:
				vbi->elementSize = 4;
			if (baseType)
			{
				Strncpy(vbi->typeString, "PTR TO ...",sizeof(vbi->typeString));
				SNPrintf(vbi->valueString, sizeof(vbi->valueString),"0x%08lx",
					ReadMem(varval,4));
			}
			else 
			{
				Strncpy(vbi->typeString, "ARRAY OF PTR TO ...", MAXTYPELEN);
				hasChildren = 1;
				for (j = 0; j < vararraylen; j++)
				{
					SNPrintf(tempSize, sizeof(tempSize),"[%ld]",vardims[j]);
					Strncat(vbi->typeString, tempSize, MAXTYPELEN);
				}
				SNPrintf(vbi->valueString, sizeof(vbi->valueString),getVarFormatStr(vartype,varval),varval,varval,varval);
			}
			break;

		case EVAR_VARTYPE_ARRAY_OF_PTR_TO_CHAR:
			vbi->elementSize = 4;
			if (baseType)
			{
				Strncpy(vbi->typeString, "PTR TO CHAR",sizeof(vbi->typeString));
				varval= (UBYTE *)ReadMem(varval,4);
				SNPrintf(vbi->valueString, sizeof(vbi->valueString),getVarFormatStr(EVAR_VARTYPE_PTR_TO_CHAR,varval),varval,varval,varval);
			}
			else
			{
				Strncpy(vbi->typeString, "ARRAY OF PTR TO CHAR", MAXTYPELEN);
				hasChildren = 1;
				for (j = 0; j < vararraylen; j++)
				{
					SNPrintf(tempSize, sizeof(tempSize),"[%ld]",vardims[j]);
					Strncat(vbi->typeString, tempSize, MAXTYPELEN);
				}
				SNPrintf(vbi->valueString, sizeof(vbi->valueString),getVarFormatStr(vartype,varval),varval,varval,varval);

			}
			break;
		case EVAR_VARTYPE_ARRAY_OF_PTR_TO_BYTE:
			vbi->elementSize = 4;
			if (baseType)
			{
				Strncpy(vbi->typeString, "PTR TO BYTE",sizeof(vbi->typeString));
				varval= (UBYTE *)ReadMem(varval,4);
				derefVal = (UBYTE)ReadMem(varval,1);
				SNPrintf(vbi->valueString, sizeof(vbi->valueString),getVarFormatStr(EVAR_VARTYPE_PTR_TO_BYTE,varval),
					varval,derefVal,derefVal&0xff);
			}
			else 
			{
				Strncpy(vbi->typeString, "ARRAY OF PTR TO BYTE", MAXTYPELEN);
				hasChildren = 1;
				for (j = 0; j < vararraylen; j++)
				{
					SNPrintf(tempSize, sizeof(tempSize),"[%ld]",vardims[j]);
					Strncat(vbi->typeString, tempSize, MAXTYPELEN);
				}
				SNPrintf(vbi->valueString, sizeof(vbi->valueString),getVarFormatStr(vartype,varval),varval,varval,varval);
			}
			break;
		case EVAR_VARTYPE_ARRAY_OF_PTR_TO_INT:
			vbi->elementSize = 4;
			if (baseType)
			{
				Strncpy(vbi->typeString, "PTR TO INT",sizeof(vbi->typeString));
				varval= (UBYTE *)ReadMem(varval,4);
				derefVal = (WORD)ReadMem(varval,2);
				SNPrintf(vbi->valueString, sizeof(vbi->valueString),getVarFormatStr(EVAR_VARTYPE_PTR_TO_INT, varval),
					varval, derefVal,derefVal&0xffff);
			}
			else 
			{
				Strncpy(vbi->typeString, "ARRAY OF PTR TO INT", MAXTYPELEN);
				hasChildren = 1;
				for (j = 0; j < vararraylen; j++)
				{
					SNPrintf(tempSize, sizeof(tempSize),"[%ld]",vardims[j]);
					Strncat(vbi->typeString, tempSize, MAXTYPELEN);
				}
				SNPrintf(vbi->valueString, sizeof(vbi->valueString),getVarFormatStr(vartype,varval),varval,varval,varval);
			}
			break;
		case EVAR_VARTYPE_ARRAY_OF_PTR_TO_WORD:
			vbi->elementSize = 4;
			if (baseType)
			{
				Strncpy(vbi->typeString, "PTR TO WORD",sizeof(vbi->typeString));
				varval= (UBYTE *)ReadMem(varval,4);
				derefVal = (UWORD)ReadMem(varval,2);
				SNPrintf(vbi->valueString, sizeof(vbi->valueString),getVarFormatStr(EVAR_VARTYPE_PTR_TO_WORD,varval),
					varval,derefVal,derefVal&0xffff);
			}
			else 
			{
				Strncpy(vbi->typeString, "ARRAY OF PTR TO WORD", MAXTYPELEN);
				hasChildren = 1;
				for (j = 0; j < vararraylen; j++)
				{
					SNPrintf(tempSize, sizeof(tempSize),"[%ld]",vardims[j]);
					Strncat(vbi->typeString, tempSize, MAXTYPELEN);
				}
				SNPrintf(vbi->valueString, sizeof(vbi->valueString),getVarFormatStr(vartype,varval),varval,varval,varval);
			}
			break;
		case EVAR_VARTYPE_ARRAY_OF_PTR_TO_LONG:
			vbi->elementSize = 4;
			if (baseType)
			{
				Strncpy(vbi->typeString, "PTR TO LONG",sizeof(vbi->typeString));
				varval= (UBYTE *)ReadMem(varval,4);
				derefVal = ReadMem(varval,4);
				SNPrintf(vbi->valueString, sizeof(vbi->valueString),getVarFormatStr(EVAR_VARTYPE_PTR_TO_LONG,varval),
					varval,derefVal,derefVal);
			}
			else 
			{
				Strncpy(vbi->typeString, "ARRAY OF PTR TO LONG", MAXTYPELEN);
				hasChildren = 1;
				for (j = 0; j < vararraylen; j++)
				{
					SNPrintf(tempSize, sizeof(tempSize),"[%ld]",vardims[j]);
					Strncat(vbi->typeString, tempSize, MAXTYPELEN);
				}
				SNPrintf(vbi->valueString, sizeof(vbi->valueString),getVarFormatStr(vartype,varval),varval,varval,varval);
			}
			break;
		case EVAR_VARTYPE_ARRAY_OF_PTR_TO_OBJECT:
			hasChildren = 1;
			vbi->elementSize = 4;

			if (baseType)
			{
				SNPrintf(vbi->typeString, sizeof(vbi->typeString),"PTR TO %s",
					LookupEObjectName(dip, dbgr, varobjectid));
				SNPrintf(vbi->valueString, sizeof(vbi->valueString),"0x%08lx",
					ReadMem(varval,4));
			}
			else 
			{
				SNPrintf(vbi->typeString, sizeof(vbi->typeString),"ARRAY OF PTR TO %s",
					LookupEObjectName(dip, dbgr, varobjectid));

				for (j = 0; j < vararraylen; j++)
				{
					SNPrintf(tempSize, sizeof(tempSize),"[%ld]",vardims[j]);
					Strncat(vbi->typeString, tempSize, MAXTYPELEN);
				}
				SNPrintf(vbi->valueString, sizeof(vbi->valueString),getVarFormatStr(vartype,varval),varval,varval,varval);
			}
			break;
		case EVAR_VARTYPE_ARRAY_OF_PTR_TO_PTR:
				vbi->elementSize = 4;
			if (baseType)
			{
				Strncpy(vbi->typeString, "PTR TO PTR TO ...",sizeof(vbi->typeString));
				SNPrintf(vbi->valueString, sizeof(vbi->valueString),"0x%08lx",
					ReadMem(varval,4));
			}
			else 
			{
				Strncpy(vbi->typeString, "ARRAY OF PTR TO PTR TO ...", MAXTYPELEN);
				hasChildren = 1;
				for (j = 0; j < vararraylen; j++)
				{
					SNPrintf(tempSize, sizeof(tempSize),"[%ld]",vardims[j]);
					Strncat(vbi->typeString, tempSize, MAXTYPELEN);
				}
				SNPrintf(vbi->valueString, sizeof(vbi->valueString),getVarFormatStr(vartype,varval),varval,varval,varval);
			}
			break;

		default:
			vbi->elementSize = 4;
			Strncpy(vbi->typeString, "unknown", MAXTYPELEN);
			SNPrintf(vbi->valueString, sizeof(vbi->valueString),"0x%08lx",varval);
			break;
	}
	
	return hasChildren;
}

ULONG recurseEArray(struct DebugInfoParserEVAR *dip,  struct Debugger *dbgr, UWORD level,
	UWORD maxlevel, ULONG varobjectid, ULONG vararraylen, UWORD *vardims, ULONG vartype,
	ULONG varval, ULONG elementSize)
{
	struct VariableBrowserInfo *vbi = &dip->dip.vbi;
	ULONG k, expand, flags;
	struct Node *node;

	for (k = 0; k < *vardims; k++)
	{
		ClearEntryInfo(dip);
		SNPrintf(vbi->nameString, sizeof(vbi->nameString),"[%ld]", k);

		setETypeAndValue(dip, dbgr, FALSE, vartype, (UBYTE *)varval, varobjectid, vararraylen,
			vardims, level == maxlevel);

		if (level < maxlevel)
		{
			vbi->referenceBlockNum = -2;
			node = dbgr->ads->nextVariableNode->ln_Pred;
			expand = createOrUpdateEntry(level, &dip->dip.vbi,TRUE,(UBYTE *)varval);
			node = node->ln_Succ; //get the new node

			varval = recurseEArray(dip, dbgr, level+1, maxlevel, varobjectid, vararraylen,
				vardims+1, vartype,
				varval, elementSize);

			//we force expand all child nodes as we can't handle
			//generating just one level of a multi-dimensional array
			GetListBrowserNodeAttrs(node,
				LBNA_Flags, &flags,
				TAG_DONE
			);

			SetListBrowserNodeAttrs(node,
				LBNA_Flags, flags | LBFLG_SHOWCHILDREN,
				TAG_DONE
			);
		}
		else
		{
			vbi->referenceBlockNum = -1;
			SNPrintf(vbi->encodedTypeString, MAXTYPELEN, "%08lx %08lx %08lx", varobjectid,level,vartype);
			expand = createOrUpdateEntry(level, &dip->dip.vbi,varobjectid!=0,(UBYTE *)varval);
			if (expand)
			{
				recurseEObject(dip, dbgr, level+1, varobjectid, (vartype == EVAR_VARTYPE_OBJECT || vartype == EVAR_VARTYPE_ARRAY_OF_OBJECT) ? varval : ReadMem((UBYTE *)varval,4));
			}
			varval += elementSize;
		}
	}
	return varval;
}

UBYTE* ProcessEvarObjects(UBYTE *buf,ULONG oid, UBYTE **objectName)
{
	ULONG nameLen, objArrayLen, membtype, objOid;

	objOid = *(WORD *)(buf); //object oid
	buf += 2; 
	buf += 2; //object flags + pad
	buf += 2; //object size
	nameLen = *(WORD *)(buf);	//object name length
	buf += 2;
	if ((objectName) && (oid == objOid))
	{
			*objectName = buf;
	}
		
	buf += nameLen;	//skip name
	
	membtype = *(WORD *)(buf);	//member type (0 to terminate)
	buf += 2; //type
	while (membtype)
	{
		buf += 2; //oid
		buf += 2; //ooff
		objArrayLen = *(WORD *)(buf);	//number of array dimensions
		buf += 2;
		buf += objArrayLen*2; //skip array dimensions info

		nameLen = *(WORD *)(buf);	//name length
		buf += 2;
		buf += nameLen;	//skip name
		membtype = *(WORD *)(buf);	//next member type
		buf += 2; //type
	}
	return buf;
}


UBYTE* ProcessEvarVars(struct DebugInfoParserEVAR *dip,  struct Debugger *dbgr, UBYTE *buf,
	UBYTE *vardetailsbuf, BOOL global, BOOL addvar, ULONG *procvarlinePtr, ULONG blockNum,
	UBYTE *processFrom)
{
	struct VariableBrowserInfo *vbi = &dip->dip.vbi;
	signed short regvar;
	ULONG i,line,num,regno,varoffs,varval, namelen, hasChildren, expand;
	UWORD vartype, varobjectid, vararraylen, *vardims;
	UBYTE *varname, *varbuf;
	BOOL process = 0;
	char unknownName[20];

	line = (*(UWORD *)buf) +1;

	if (procvarlinePtr)
	{
		*procvarlinePtr = line;
	}

	if (vardetailsbuf) {
		num = *(WORD *)(buf+2);
		num = num | 0x8000;
		*(WORD *)(buf +2) = num;
	}

	num = *(WORD *)(buf+2);
	if ((num & 0x8000) && (!vardetailsbuf)) addvar = 0;

	num = num & 0x7fff;

	buf = buf +4;
	if (vardetailsbuf) vardetailsbuf = vardetailsbuf + 4;

	for (i=1; i<=num; i++)
	{

		varbuf = buf;

		regvar = *(signed short *)buf;
		regno = regvar >=30000 ? (regvar-30000) : 0;
		varoffs =  regvar >=30000 ? 0 : regvar;
		buf = buf + 2;
		hasChildren = 0;

		if (vardetailsbuf) {
			vartype = *(UWORD *)vardetailsbuf;
			varobjectid = *(UWORD *)(vardetailsbuf+2);
			vararraylen = *(UWORD *)(vardetailsbuf+4);
			if (vararraylen)
				vardims  = (UWORD *)(vardetailsbuf+6);
			else
				vardims  = 0;

			vardetailsbuf = vardetailsbuf+(vararraylen*2)+6;
			namelen = *(UWORD *)(vardetailsbuf);
			varname = vardetailsbuf + 2;
			vardetailsbuf = vardetailsbuf + 2 + namelen;
		}
		else
		{
			if (global)
				SNPrintf(unknownName, sizeof(unknownName),"g%ld_%ld",i,line);
			else
				SNPrintf(unknownName, sizeof(unknownName),"l_%ld_%ld",i,line);

			varname = unknownName;
			varobjectid = 0;
			namelen = 8;
			vartype = 0xffff;
			vararraylen = 0;
			vardims  = 0;
		}

		if (!processFrom) process = TRUE;
		if (varbuf == processFrom) process = TRUE;
		if (!process) continue;

		vbi->elementSize = 4;

		if (addvar && (blockNum>line))
		{
			Strncpy(vbi->nameString, varname, namelen+1);

			if (regno && !global)
			{
				SNPrintf(vbi->nameString, sizeof(vbi->nameString),"%s [%s%ld]",varname,(regno < 8 ? "d" : "a"), regno & 7);
				switch (regno)
				{
					case 1:
						varval = brokenRegs->d1;
						break;
					case 2:
						varval = brokenRegs->d2;
						break;
					case 3:
						varval = brokenRegs->d3;
						break;
					case 4:
						varval = brokenRegs->d4;
						break;
					case 5:
						varval = brokenRegs->d5;
						break;
					case 6:
						varval = brokenRegs->d6;
						break;
					case 7:
						varval = brokenRegs->d7;
						break;
					case 8:
						varval = brokenRegs->a0;
						break;
					case 9:
						varval = brokenRegs->a1;
						break;
					case 10:
						varval = brokenRegs->a2;
						break;
					case 11:
						varval = brokenRegs->a3;
						break;
					case 12:
						varval = brokenRegs->a4;
						break;
					case 13:
						varval = brokenRegs->a5;
						break;
					case 14:
						varval = brokenRegs->a6;
						break;
					case 15:
						varval = brokenRegs->a7;
						break;
				}
			} else {
				if (global)
				{
					varval = *(ULONG *)(brokenRegs->a4+varoffs);
				}
				else
				{
					varval = *(ULONG *)(brokenRegs->a5+varoffs);
				}
			}

			hasChildren = setETypeAndValue(dip, dbgr, FALSE, vartype, (UBYTE *)varval, varobjectid, vararraylen,
				vardims, FALSE);

			if (varbuf == processFrom)
			{
				expand = TRUE;
			}
			else
			{
				vbi->referenceBlockNum = (ULONG)varbuf;
				SNPrintf(vbi->encodedTypeString, MAXTYPELEN, "%08lx %08lx %08lx",
					varobjectid, 0, vartype);
				expand = createOrUpdateEntry(0, &dip->dip.vbi,hasChildren,(UBYTE *)varval);
			}

			if (expand)
			{
				if (vararraylen)
					recurseEArray(dip, dbgr, 1,vararraylen,varobjectid, vararraylen,vardims,
						vartype,varval,vbi->elementSize);
				else
					recurseEObject(dip, dbgr, 1, varobjectid, varval);
			}
			ClearEntryInfo(dip);
		}
	}
	return vardetailsbuf ? vardetailsbuf : buf;
}

UBYTE *LookupEObjectDefinition(struct DebugInfoParserEVAR *dip, struct Debugger *dbgr, ULONG oid) 
{
	ULONG blockType,len,length;
	UBYTE *buf = dip->buffer, *oldbuf, *objectName = NULL, *prevvarbuf;

	prevvarbuf = NULL; //last processed variable block (parameter, local or global variable)

	//skip straight to the object defs if we already know where they are
	if (dip->objDefs) buf = dip->objDefs-2;

	length = dip->EVarDataLength;
	while (buf<(dip->buffer+length))
	{
		blockType = *(UWORD *)buf;
		buf = buf + 2;
		
		switch (blockType)
		{
			case EVAR_BLOCK_END:
				break;
			case EVAR_BLOCK_PARAM:
			case EVAR_BLOCK_LOCAL:
				//scan through the structure (without doing anyting)
				prevvarbuf = buf;
				buf = ProcessEvarVars(dip, dbgr, buf, NULL, 0,0, NULL, 0, NULL);
				break;
			case EVAR_BLOCK_GLOBAL:
				//handle global variable definitions
				//scan through the structure (without doing anyting)
				prevvarbuf = buf;
				buf = ProcessEvarVars(dip, dbgr, buf, NULL, 0, 0, NULL, 0, NULL);
				break;
			case EVAR_BLOCK_PROC:
				//skip procs
				len = *(UWORD *)buf;
				buf = buf + 2 + len;
				break;
			case EVAR_BLOCK_SELF:
				//skip self variable definition
				len = *(UWORD *)buf;
				buf = buf + 2 + len;
				break;
			case EVAR_BLOCK_VAREXTRA:
				//skip variable extra details
				buf = ProcessEvarVars(dip, dbgr, prevvarbuf, buf, 0, 0, NULL, 0, NULL);
				break;
			case EVAR_BLOCK_PROCEXTRA:
				//skip procedure extra details
				buf = buf + 2;
				break;
			case EVAR_BLOCK_OBJECT:
				oldbuf = buf;
				buf = ProcessEvarObjects(buf, oid, &objectName);
				if (objectName) return oldbuf;
				break;
		}
	}
	return NULL;
}

UWORD LookupEObjectSize(struct DebugInfoParserEVAR *dip, struct Debugger *dbgr, ULONG oid)
{
	UBYTE *buf = LookupEObjectDefinition(dip, dbgr, oid);
	if (buf)
		return *(UWORD *)(buf+4);
	else
		return 0;
}

STRPTR LookupEObjectName(struct DebugInfoParserEVAR *dip, struct Debugger *dbgr, ULONG oid) 
{
	UBYTE *buf = LookupEObjectDefinition(dip, dbgr, oid);
	if (buf)
		return (STRPTR)(buf+8);
	else
		return NULL;
}

STRPTR LookupEFunctionName(struct DebugInfoParserEVAR *dip, struct Debugger *dbgr,
	ULONG findThisBlock)
{
	ULONG blockType,len,length,procline, prevprocline, procvarline;
	UBYTE *buf = dip->buffer, *prevvarbuf, *procname, *prevprocname;

	findThisBlock++; // increment as we are given a 0 based block number
	
	length = dip->EVarDataLength;
	procline = -1;   //most recently encountered procedure definition line number
	prevprocline = -1; //second most recently encountered procedure definition line number
	procname = 0;  //name of most recently encountered procedure definition
	prevprocname = 0; //name of second most recently encountered procedure definition
	prevvarbuf = NULL; //last processed variable block (parameter, local or global variable)
	
	while (buf<(dip->buffer+length))
	{
		blockType = *(UWORD *)buf;
		buf = buf + 2;

		switch (blockType)
		{
			case EVAR_BLOCK_END:
				break;
			case EVAR_BLOCK_PARAM:
			case EVAR_BLOCK_LOCAL:
				//handle parameter and local variables definitions
				prevvarbuf = buf;
				//scan through the structure (without doing anyting)
				buf = ProcessEvarVars(dip, dbgr, buf, NULL, 0,0, &procvarline, findThisBlock, NULL);
				if (procline == -1) procline = procvarline-1;
				break;
			case EVAR_BLOCK_GLOBAL:
				//handle global variable definitions
				prevvarbuf = buf;
				//scan through the structure (without doing anyting)
				buf = ProcessEvarVars(dip, dbgr, buf, NULL, 0, 0, &procvarline, findThisBlock, NULL);
				break;
			case EVAR_BLOCK_PROC:
				//procs
				prevprocline = procline;
				prevprocname = procname;
				procline = -1;
				prevvarbuf = NULL;
				len = *(UWORD *)buf;
				buf = buf + 2;
				prevprocname = procname;
				procname = buf;
				buf = buf + len;
				break;
			case EVAR_BLOCK_SELF:
				//self variable definition
				len = *(UWORD *)buf;
				buf = buf + 2 + len;
				prevvarbuf = NULL;
				break;
			case EVAR_BLOCK_VAREXTRA:
				//process variable extra details
				buf = ProcessEvarVars(dip, dbgr, prevvarbuf, buf, 0, 0, &procvarline,
					findThisBlock, NULL);

				if (procline == -1) procline = procvarline-1;
				prevvarbuf = NULL;
				break;
			case EVAR_BLOCK_PROCEXTRA:
				//process procedure extra details
				procline =  *(UWORD *)buf;
				buf = buf + 2;
				prevvarbuf = NULL;
				break;
			case EVAR_BLOCK_OBJECT:
				buf = ProcessEvarObjects(buf, FALSE, NULL);
				break;
		}

		if ((prevprocline<=findThisBlock) && (procline>findThisBlock) && (prevprocline!=-1)
				&& (procline!=-1))
		{
			//the previous procedure was the one we need
			return prevprocname;
		}

		if (buf>=(dip->buffer+length))
		{
			//the last procedure was the one we need
			return procname;
		}
	}
	return procname;
}

void ParseEvarData(struct DebugInfoParserEVAR *dip, struct Debugger *dbgr, ULONG blockNum,
		UBYTE *sp)
{
	struct VariableBrowserInfo *vbi = &dip->dip.vbi;
	ULONG blockType,len,length,procline,prevprocline,rep,dovars,global,procvarline;
	UBYTE *buf = dip->buffer, *prevvarbuf, *procbuf, *prevprocbuf, *processFrom;

	if (dbgr->ads->expandVariableMode) processFrom = (UBYTE *)vbi->referenceBlockNum; else processFrom = NULL;

	blockNum++; // increment as we are given a 0 based block number
	
	length = dip->EVarDataLength;
	procline = 0;
	prevprocline = -1;
	procvarline = -1;
	procbuf = 0;
	rep = 0;
	dovars = 0;
	global = 0;
	prevprocbuf = 0;
	prevvarbuf = NULL;
	
	while (buf<(dip->buffer+length))
	{
		blockType = *(UWORD *)buf;
		buf = buf + 2;
		if (global && (blockType!=EVAR_BLOCK_VAREXTRA) && prevvarbuf) {
			//add global variables if no extra detail is present
			//only add the main global variables
			//globals only defined in modules do not have
			//sufficient enough debug information in older
			//versions of E
			if (!dbgr->firstdip || (dip == (struct DebugInfoParserEVAR *)dbgr->firstdip))
			{
				ProcessEvarVars(dip, dbgr, prevvarbuf, NULL, 1, 1, 0, 0xffffffff, processFrom);
			}
			prevvarbuf = NULL;
		}
		switch (blockType)
		{
			case EVAR_BLOCK_END:
				//handle end
				break;
			case EVAR_BLOCK_PARAM:
			case EVAR_BLOCK_LOCAL:
				//handle parameters and local variables
				global = 0;
				prevvarbuf = buf;
				//scan through the structure, only add the variables if
				//we found the correct block and we are still in that block
				buf = ProcessEvarVars(dip, dbgr, buf, NULL, 0,dovars, &procvarline,
					blockNum, processFrom);
				if (procline == -1) procline = procvarline-1;
				break;
			case EVAR_BLOCK_GLOBAL:
				//handle global variables definition
				global = 1;
				prevvarbuf = buf;
				//scan through the structure but dont add the variables yet
				buf = ProcessEvarVars(dip, dbgr, buf, NULL, 1, 0, 0, blockNum, processFrom);
				break;
			case EVAR_BLOCK_PROC:
				//handle procs definition
				prevprocbuf = procbuf;
				len = *(UWORD *)buf;
				buf = buf + 2 + len;
				prevprocline = procline;
				procline = -1; //we don't know the line number yet
				prevvarbuf = NULL; //we dont have a previous variable block
				//we are no longer in the same block
				dovars = 0;
				procbuf = buf;
				break;
			case EVAR_BLOCK_SELF:
				//handle self variable definition
				len = *(UWORD *)buf;
				buf = buf +2;
				if (len>=6)
				{
					selfdata2[6] = buf[4];	//object oid
					selfdata2[7] = buf[5];	//object oid
				}
				else
				{
					selfdata2[6] = 0;
					selfdata2[7] = 0;
				}
				selfdata1[0] = buf[0];	//line number
				selfdata1[1] = buf[1];  //line number
				selfdata1[4] = buf[2];  //regno
				selfdata1[5] = buf[3];	//regno
				selfdata2[0] = buf[0];	//line number
				selfdata2[1] = buf[1];	//line number
				ProcessEvarVars(dip, dbgr, selfdata1, selfdata2, 0, dovars, 0,
						blockNum, processFrom);
				buf = buf + len;
				prevvarbuf = NULL;
				break;
			case EVAR_BLOCK_VAREXTRA:
				//handle var extra detail (var names + types)
				//add in the variables now that we have the relevant info
				buf = ProcessEvarVars(dip, dbgr, prevvarbuf, buf, global, dovars | global,
					&procvarline, global ? 0xffffffff : blockNum, processFrom);
				prevvarbuf = NULL; //clear the previous variable buffer so we dont add them again
				break;
			case EVAR_BLOCK_PROCEXTRA:
				//handle proc extra detail (line number)
				procline =  *(UWORD *)buf;
				prevvarbuf = NULL;
				buf = buf + 2;  // skip lineno
				break;
			case EVAR_BLOCK_OBJECT:
				if (!dip->objDefs) dip->objDefs = buf;
				buf = ProcessEvarObjects(buf, FALSE, NULL);
				break;
		}

		if ((prevprocline<=blockNum) && (procline>blockNum) && (!rep) && (prevprocline!=-1)
				&& (procline!=-1))
		{
			//the previous procedure was the one we need
			//so loop back and add the variables
			rep = 1; dovars = 1; buf = prevprocbuf;
		}

		if (!rep && buf>=(dip->buffer+length) && procbuf)
		{
				//the last procedure was the one we need
				//so loop back and add the variables
				rep = 1; dovars = 1; buf = procbuf;
		}

	}
}


ULONG ParseEVAR(struct Debugger *dbgr, BPTR file, UBYTE *filename, ULONG totalLen)
{
	struct DebugInfoParserEVAR *dip;

	dip = AllocVec(sizeof(struct DebugInfoParserEVAR), MEMF_ANY|MEMF_CLEAR);
	if (! dip)
		return totalLen; // we havn't read anything

	dbgr->useExactSLL = TRUE;

	dip->buffer = AllocVec(totalLen*4, MEMF_ANY);
	if (! dip->buffer)
	{
		FreeVec(dip);
		return totalLen; // we havn't read anything
	}

	dip->dip.Free = FreeEVAR;
	dip->dip.ClearEntryInfo = ClearEntryInfo;
	dip->dip.RegisterData = NULL;
	dip->dip.RegisterBSS = NULL;
	dip->dip.ExpandNode = ExpandENode;
	dip->dip.ParseData = ParseEvarData;
	dip->dip.LookupFunctionName = LookupEFunctionName;
	dip->dip.LookupFunctionStackUsage = NULL;

	FRead(file, dip->buffer, totalLen*4, 1);

	dip->EVarDataLength = totalLen*4;

	Strncpy(dip->dip.filename,filename, sizeof(dip->dip.filename));

	dip->dip.node.ln_Name = dip->dip.filename;

	AddTail(&dbgr->dipList, (struct Node *)dip);

	return 0;
}
